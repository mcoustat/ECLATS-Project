#ifndef AREA_FILTER_HH
# define AREA_FILTER_HH

# include "compute_area_image.hh"


namespace mln
{

  namespace my
  {


    image2d<value::int_u8>
    area_filter(const tree& t, unsigned lambda)
    {
      image2d<unsigned> area = compute_area_image(t);

      image2d<value::int_u8> output(t.domain());
      for (unsigned i = 0; i < t.S.size(); ++i)
	{
	  point2d p = t.S[i];  // from root to leaves
	  if (t.is_representative(p) and area(p) > lambda)
	    output(p) = t.u(p);
	  else
	    output(p) = output(t.parent(p));
	}
    
      return output;
    }


  } // end of namespace mln::my

} // end of namespace mln


#endif // ndef AREA_FILTER_HH
