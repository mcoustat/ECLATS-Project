#ifndef BBOX_HH
# define BBOX_HH

# include "tree.hh"



namespace mln
{

  namespace my
  {

    
    struct bbox
    {
      typedef box2d value_type;
      
      box2d bb;
      
      void init(const point2d& p)
      {
	bb.pmin() = p;
	bb.pmax() = p;
      }
      
      void take(const point2d& p)
      {
	bb.merge(box2d(p, p));
      }

      void take(const bbox& other)
      {
	bb.merge(other.bb);
      }

      box2d value() const
      {
	return bb;
      }
    };


    
  } // mln::my

} // mln


#endif // ndef BBOX_HH
