#include <mln/arith/diff_abs.hh>
#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>

#include <iostream>

void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " ref1.pgm ref2.pgm output.pgm" << std::endl;
  std::abort();
}


// g++ -std=c++11 -DNDEBUG -O3 -I. abs_diff.cc -o abs_diff


int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 4)
    usage(argv);
  
  std::cout << "├── Read input image - ref1" << std::endl;
  image2d<int_u8> input1;
  io::pgm::load(input1, argv[1]); // /!\ fichier .pgm
  
  std::cout << "├─── Read input image - ref2" << std::endl;
  image2d<int_u8> input2;
  io::pgm::load(input2, argv[2]); // /!\ fichier .pgm

  std::cout << "├──── Making the absolute difference" << std::endl;
  image2d<int_u8> result = arith::diff_abs(input1, input2);

  std::cout << "├───── Saving result" << std::endl;
  io::pgm::save(result, argv[3]);

  std::cout << "└────── DONE" << std::endl;

}
