#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>

#include "min_tree.hh"
#include "max_tree.hh"

#include "compute_area_image.hh"
#include "area_filter.hh"

#include "compute_attribute.hh"
#include "bbox.hh"

#include "stdafx.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "dataanalysis.h"

#include <fstream>

using namespace alglib;



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm" << std::endl;
  std::abort();
}


// g++ -I./test/cpp/src testVec.cc ./test/cpp/src/*.cpp -o testVec2.out



int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 2)
    usage(argv);

  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm


  //---___---MIN-TREE---___---

  my::tree t = my::min_tree(input);
  std::ostringstream oss;
/*
  //___________DEBUG___________
  // Print des parents
  debug::println("parent", t.parent);
  // Print des niveaux de gris
  debug::println("t.u", t.u);
  // Print des pixels représentatifs
  if (!t.S.empty())
  {
    std::copy(t.S.begin(), t.S.end()-1,std::ostream_iterator< mln::point<mln::grid::square, short int> >(oss,","));
    oss << t.S.back();
  }
  std::cout << "t.S" << std::endl;
  std::cout << oss.str() << std::endl << std::endl;
*/


  //---___---AREA---___---
/*
  image2d<unsigned> image = my::compute_area_image(t);
  image2d<value::int_u8> imageint(t.domain());
  for (unsigned i = 0; i < t.S.size(); ++i)
  {
    point2d p = t.S[i];  // from root to leaves
    imageint(p) = (value::int_u8) image(p);
  }
  my::tree area = my::min_tree(imageint);
*/
/*
  //___________DEBUG___________
  debug::println("area", imageint);
  // Print des parents
  debug::println("parent", area.parent);
  // Print des niveaux de gris
  debug::println("t.u", area.u);
*/
/*
  oss.str("");
  oss << "[" ;
  for (unsigned i = 0; i < area.S.size(); ++i)
  {
    point2d p = area.S[i];  // from root to leaves
    oss << "[" << imageint(p) << "]";
    if(p != area.S.back())
	oss << ",";
  }
  oss << "]" ;
*/

  //Déclaration de Pi
  const double pi = std::atan(1.0)*4;

  image2d<unsigned> area = my::compute_area_image(t);
  image2d<box2d> bbox = my::compute_attribute< my::bbox >(t);

  oss.str("");
  oss << "[" ;
  std::string chaine = "";


  for (unsigned i = 0; i < area.nelements(); ++i)
  {
    //if(area.element(i) != 1 && area.element(i) != input.nrows()*input.ncols() ){
    //if(t.is_representative(area.point_at_offset(i)) && area.element(i) != 1 && area.element(i) != input.nrows()*input.ncols()){

      double excentricite = 0;
      if(bbox.element(i).nrows() != 0)
        excentricite = bbox.element(i).ncols()/bbox.element(i).nrows();

      double compacite = 0;
      if(bbox.element(i).nrows() != 0)
        compacite = sqrt((4/pi)*area.element(i))/bbox.element(i).nrows();

      chaine.append("[");
      chaine.append(std::to_string(bbox.element(i).nrows())); //hauteur
      chaine.append(",");
      chaine.append(std::to_string(bbox.element(i).ncols())); //largeur
      chaine.append(",");
      chaine.append(std::to_string(excentricite)); //excentricite
      chaine.append(",");
      chaine.append(std::to_string(area.element(i))); //aire
      chaine.append(",");
      chaine.append(std::to_string(compacite)); //compacite
      chaine.append("],");

    //}
  }
  chaine.pop_back();
  oss << chaine << "]" ;



  // Sauvegarde des données pour clusterisation
  const std::string str = oss.str();
  const char* cstr = str.c_str();


/*
  //___________DEBUG___________
  // Print des pixels représentatifs
  oss.str("");
  if (!area.S.empty())
  {
    
    std::copy(area.S.begin(), area.S.end()-1,std::ostream_iterator< mln::point<mln::grid::square, short int> >(oss,","));
    oss << area.S.back();
  }
  std::cout << "t.S" << std::endl;
  std::cout << oss.str() << std::endl << std::endl;

  std::cout << "Données entrée clustering" << std::endl;
  std::cout << cstr << std::endl << std::endl;
*/
  std::ofstream fichier("testVecDEBUG.txt", std::ios::out | std::ios::trunc);
  fichier << cstr << std::endl;
  fichier.close();




  //---___---K-MEANS---___---

  int nbclusters = 5;

  clusterizerstate s;
  kmeansreport rep;
  real_2d_array xy = real_2d_array(cstr);

  clusterizercreate(s);
  clusterizersetpoints(s, xy, 2); // Euclidean metric (2)
  clusterizersetkmeanslimits(s, 5, 0); // Set number of restarts from random positions (5)
  clusterizerrunkmeans(s, nbclusters, rep); // Number of clusters

//  std::cout << "Resultat Clustering" << std::endl;
//  std::cout << rep.cidx.tostring().c_str() << std::endl << std::endl;

/*
  //___________DEBUG___________
  std::ofstream fichier("testVecDEBUG.txt", std::ios::out | std::ios::trunc);
  fichier << rep.cidx.tostring() << std::endl;
  fichier.close();
*/


  image2d<value::int_u8> cluster0(t.domain());
  image2d<value::int_u8> cluster1(t.domain());
  image2d<value::int_u8> cluster2(t.domain());
  image2d<value::int_u8> cluster3(t.domain());
  image2d<value::int_u8> cluster4(t.domain());
  data::fill(cluster0, 255);
  data::fill(cluster1, 255);
  data::fill(cluster2, 255);
  data::fill(cluster3, 255);
  data::fill(cluster4, 255);


  for (unsigned i = 0; i < rep.cidx.length(); ++i)
  {
    int cluster = rep.cidx[i];
    point2d p = area.point_at_offset(i);
    switch(cluster){
    case 0 : // cas du cluster0
             cluster0(p) = t.u(p);
             /*if (t.is_representative(p)){
	       cluster0(p) = t.u(p);
	     }else
	       cluster0(p) = cluster0(t.parent(p));*/
             break;
    case 1 : // cas du cluster1
             cluster1(p) = t.u(p);
             /*if (t.is_representative(p)){
	       cluster1(p) = t.u(p);
	     }else
	       cluster1(p) = cluster1(t.parent(p));*/
             break;
    case 2 : // cas du cluster2
             cluster2(p) = t.u(p);
             /*if (t.is_representative(p)){
	       cluster2(p) = t.u(p);
	     }else
	       cluster2(p) = cluster2(t.parent(p));*/
             break;
    case 3 : // cas du cluster2
             cluster3(p) = t.u(p);
             /*if (t.is_representative(p)){
	       cluster3(p) = t.u(p);
	     }else
	       cluster3(p) = cluster3(t.parent(p));*/
             break;
    case 4 : // cas du cluster2
             cluster4(p) = t.u(p);
             /*if (t.is_representative(p)){
	       cluster4(p) = t.u(p);
	     }else
	       cluster4(p) = cluster4(t.parent(p));*/
             break;
    default: break;
    }
  }
  std::cout << std::endl;

  io::pgm::save(cluster0, "testVec_c0.pgm");
  io::pgm::save(cluster1, "testVec_c1.pgm");
  io::pgm::save(cluster2, "testVec_c2.pgm");
  io::pgm::save(cluster3, "testVec_c3.pgm");
  io::pgm::save(cluster4, "testVec_c4.pgm");

}
