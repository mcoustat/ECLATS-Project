#include <iostream>

#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <mln/arith/diff_abs.hh>
#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>

#include "max_tree.hh"
#include "min_tree.hh"

#include "compute_attribute.hh"
#include "compute_area_image.hh"
#include "area_filter.hh"
#include "bbox.hh"

#include <limits.h>
#include <stdlib.h>
#include <cmath>

void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm" << std::endl;
  std::abort();
}

// g++ -std=c++11 -DNDEBUG -O3 -I. detectType.cc -o detectType

int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;
  
  if (argc != 2)
    usage(argv);

  std::cout << "├── Read input image" << std::endl;
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm

  std::cout << "├─── Building tree" << std::endl;
  my::tree t = my::max_tree(input);

  std::cout << "├──── Computing images of attributes" << std::endl;
  image2d<unsigned> area = my::compute_area_image(t);
  image2d<box2d> bbs = my::compute_attribute< my::bbox >(t);

  std::cout << "├───── Detecting type of image" << std::endl;
  int type = 0;
  if(input.bbox().ncols() > input.bbox().nrows()){
    std::cout << "├ └──── Format : PAYSAGE" << std::endl;
    // Reste a determiner si c'est 4, 6 ou 7

    // Check : split au milieu (deux aires de moitié de la carte)
    // Result : OUI - ⑥ / Non - skip
    int countSplit = 0;
    for (unsigned i = 0; i < t.S.size(); ++i)
    {
      point2d p = t.S[i];  // from root to leaves
      if (
        t.is_representative(p)		// noeud représentatif
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmin().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y1)!=(X2,Y2) diagonales opposées
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmax().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y2)!=(X2,Y2) minimum 4 points distincts
        and area(p) > area(t.S[0])*0.35	// on ne veut que des aires supérieures a 35% de l'aire de l'image (aire d'une zone de split ~39/40%)
        and area(p) < area(t.S[0])*0.44	// // on ne veut que des aires inférieures a 45% de l'aire de l'image (aire d'une zone de split ~39/40%)
        and bbs(p).pmin().col() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmin().row() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmax().col() != bbs(t.S[0]).pmax().col()	// point d'origine différent du bas de l'image (contour)
        and bbs(p).pmax().row() != bbs(t.S[0]).pmax().row()	// point d'origine différent du bas de l'image (contour)
      ){
        countSplit++;
      }
    }
    if(countSplit==2){
      std::cout << "├ └──── Split : OUI" << std::endl;
      type = 6;
      std::cout << "├  └──── Type : ⑥" << std::endl;
    } else
     std::cout << "├ └──── Split : NON" << std::endl;
    
    
    
    double diag = sqrt( pow(bbs(t.S[0]).pmax().col(),2) + pow(bbs(t.S[0]).pmax().row(),2) );
    
    
    
    // Check : aire en haut à gauche
    // Result : OUI - ④ / Non - skip
    if(type==0){
    int countHG = 0;
    for (unsigned i = 0; i < t.S.size(); ++i)
    {
      point2d p = t.S[i];  // from root to leaves
      if (
        t.is_representative(p)		// noeud représentatif
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmin().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y1)!=(X2,Y2) diagonales opposées
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmax().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y2)!=(X2,Y2) minimum 4 points distincts
        and area(p) > area(t.S[0])*0.01	// on retire les très petites zones de la map
        and bbs(p).pmin().col() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmin().row() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmax().col() != bbs(t.S[0]).pmax().col()	// point d'origine différent du bas de l'image (contour)
        and bbs(p).pmax().row() != bbs(t.S[0]).pmax().row()	// point d'origine différent du bas de l'image (contour)
       ){
        double hyp =  sqrt( pow(bbs(p).pmax().col(),2) + pow(bbs(p).pmax().row(),2) );
        double port = (hyp*100.00)/diag;
        if(port > 40.00 and port < 48.00)
          countHG++;
        //std::cout << std::to_string(hyp) << " soit " << std::to_string(port) << "%" << std::endl;
      }
    }
    if(countHG==1){
      std::cout << "├ └──── Aire Haut-Gauche : OUI" << std::endl;
      type = 4;
      std::cout << "├  └──── Type : ④" << std::endl;
    } else
     std::cout << "├ └──── Aire Haut-Gauche : NON" << std::endl;
    }


    // Check : aire en bas à droite
    // Result : OUI - ⑦ / Non - indeterminé
    if(type==0){
    int countBD = 0;
    for (unsigned i = 0; i < t.S.size(); ++i)
    {
      point2d p = t.S[i];  // from root to leaves
      if (
        t.is_representative(p)		// noeud représentatif
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmin().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y1)!=(X2,Y2) diagonales opposées
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmax().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y2)!=(X2,Y2) minimum 4 points distincts
        and area(p) > area(t.S[0])*0.01	// on retire les très petites zones de la map
        and bbs(p).pmin().col() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmin().row() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmax().col() != bbs(t.S[0]).pmax().col()	// point d'origine différent du bas de l'image (contour)
        and bbs(p).pmax().row() != bbs(t.S[0]).pmax().row()	// point d'origine différent du bas de l'image (contour)
       ){
        double hyp =  sqrt( pow((bbs(p).pmax().col()-bbs(p).pmin().col()),2) + pow((bbs(p).pmax().row()-bbs(p).pmin().row()),2) );
        double port = (hyp*100.00)/diag;
        if(port > 61.00 and port < 69.00)
          countBD++;
        //std::cout << std::to_string(hyp) << " soit " << std::to_string(port) << "%" << std::endl;
      }
    }
    if(countBD==1){
      std::cout << "├ └──── Aire Bas-Droite : OUI" << std::endl;
      type = 7;
      std::cout << "├  └──── Type : ⑦" << std::endl;
    } else {
      std::cout << "├ └──── Aire Bas-Droite : NON" << std::endl;
      std::cout << "├  └──── Type : ⚠ INDETERMINE ⚠" << std::endl;
    }
    }
    // DONE



  } else {
    std::cout << "├ └──── Format : PORTRAIT" << std::endl;
    // Reste a determiner si c'est 1, 2, 3 ou 5
    
    // Check : split au milieu (deux aires de moitié de la carte)
    // Result : OUI - ⑤ / Non - skip
    //std::cout << "Type : ⑤" << std::endl;
    //std::cout << "Pas de split" << std::endl;
    int countSplit = 0;
    for (unsigned i = 0; i < t.S.size(); ++i)
    {
      point2d p = t.S[i];  // from root to leaves
      if (
        t.is_representative(p)		// noeud représentatif
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmin().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y1)!=(X2,Y2) diagonales opposées
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmax().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y2)!=(X2,Y2) minimum 4 points distincts
        and area(p) > area(t.S[0])*0.01	// on retire les très petites zones de la map
        //and area(p) > area(t.S[0])*0.29	// on ne veut que des aires supérieures a 35% de l'aire de l'image (aire d'une zone de split ~32/33%)
        //and area(p) < area(t.S[0])*0.36	// // on ne veut que des aires inférieures a 45% de l'aire de l'image (aire d'une zone de split ~32/33%)
        and bbs(p).pmin().col() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmin().row() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmax().col() != bbs(t.S[0]).pmax().col()	// point d'origine différent du bas de l'image (contour)
        and bbs(p).pmax().row() != bbs(t.S[0]).pmax().row()	// point d'origine différent du bas de l'image (contour)
      ){
        std::cout << std::to_string(area(p)) << " soit " << std::to_string( (area(p)*100.00)/area(t.S[0]) ) << "%" << std::endl;
        //countSplit++;
      }
    }
    /*
    if(countSplit==2){
      std::cout << "├ └──── Split : OUI" << std::endl;
      type = 6;
      std::cout << "├  └──── Type : ⑥" << std::endl;
    } else
     std::cout << "├ └──── Split : NON" << std::endl;
    */



    // Check : aire en bas a droite
    // Result : OUI - skip / Non - ②
    //std::cout << "Il y a une aire en bas à droite" << std::endl;
    //std::cout << "Type : ②" << std::endl;
    
    // Check : nb aires > 3 au total
    // Result : OUI - ① / Non - ③
    //std::cout << "Type : ①" << std::endl;
    //std::cout << "Type : ③" << std::endl;
    
    // DONE
  }

  std::cout << "└─────── DONE" << std::endl;

  /*
  //DEBUG

  std::cout << "REF : " << std::to_string(area(t.S[0])) << std::endl;
  double part = (area(p)*100.00)/area(t.S[0]);
  std::cout << std::to_string(area(p)) << " soit " << std::to_string(part) << "%" << std::endl;

  std::cout << "├ └─ Largeur : " << input.bbox().ncols() << std::endl;
  std::cout << "├ └─ Hauteur : " << input.bbox().nrows() << std::endl;
  */
  
}
