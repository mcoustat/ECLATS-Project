#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>

#include <iostream>

#include "max_tree.hh"
#include "min_tree.hh"

#include "compute_area_image.hh"
#include "area_filter.hh"

#include "compute_attribute.hh"
#include "bbox.hh"

#include "dimension_filter.hh"



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm max|min length and|or width output.pgm" << std::endl;
  std::abort();
}


mln::my::tree tree_method(mln::image2d<mln::value::int_u8> image, char* argv[])
{
  if(std::string(argv[2]) == "min"){
    std::cout << "├─── Building Min-Tree" << std::endl;
    return mln::my::min_tree(image);
  } else if(std::string(argv[2]) == "max"){
    std::cout << "├─── Building Max-Tree" << std::endl;
    return mln::my::max_tree(image);
  } else {
    std::cout << "ERROR : Tree method is not set (min|max)" << std::endl;
    usage(argv);
  }
}

// g++ -std=c++11 -DNDEBUG -O3 -I. rect_filter.cc -o rect_filter


int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 7)
    usage(argv);
  
  std::cout << "├── Read input image" << std::endl;
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm

  my::tree t = tree_method(input, argv);
  
  std::cout << "├──── Filtering" << std::endl;
  image2d<int_u8> result = my::dimension_filter(t, argv[4], std::stoi(argv[3]), std::stoi(argv[5]));
  
  std::cout << "├───── Saving result" << std::endl;
  io::pgm::save(result, argv[6]);

  std::cout << "└────── DONE" << std::endl;
  
}
