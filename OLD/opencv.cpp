#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>

using namespace cv;
using namespace std;

//g++ -std=c++11 opencv.cpp -o output `pkg-config --cflags --libs opencv`

int main(int argc, char** argv) {
  
  Mat image;
  Mat imageBin;
  Mat canny_output;
  int thresh = 127;
  int max_thresh = 255;
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  vector<Moments> mu(contours.size());
  double hu[7];

  // stockage de l'image
  image = cv::imread(argv[1] , IMREAD_COLOR);
  if(!image.data){
    cout <<  "Could not open or find the image" << endl ;
    return -1;
  }
  
/*
  //threshold(image, imageBin, thresh, max_thresh, 0);
  
  SimpleBlobDetector::Params params;
  //params.blobColor = 0;
  //params.minThreshold = 40;
  //params.maxThreshold = 200;
  params.thresholdStep = 1;
  params.filterByColor = false;
  params.filterByArea = false;
  params.filterByCircularity = false;
  params.filterByConvexity = false;
  params.filterByInertia = false;
  params.minDistBetweenBlobs = 1;
  
  cv::Ptr<cv::SimpleBlobDetector> detector = cv::SimpleBlobDetector::create(params);
  std::vector<KeyPoint> keypoints;
  detector->detect(image, keypoints);
  //detector->detect(imageBin, keypoints);

  std::cout << "Blob count : " << keypoints.size() << std::endl;
 
  // Draw detected blobs as red circles.
  // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to
  // the size of blob
  Mat im_with_keypoints;
  drawKeypoints(image,keypoints,im_with_keypoints,Scalar(0,0,255),DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
 
  // Show blobs
  imwrite("opencv.png", im_with_keypoints );
  //imshow("keypoints", im_with_keypoints );
  //waitKey(0);
*/  

/*
  // binarisation
  threshold(image, imageBin, thresh, max_thresh, 0);
*/

  // filtre de canny
  Canny(image, canny_output, thresh, thresh*2, 3);
  // detection de contour
  findContours(canny_output, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

 
  // Dimensions
  for(int i=0; i<contours.size(); i++){
    Rect box = boundingRect(contours[i]);
    cout << "Hauteur:\t" << box.height << endl;				// hauteur
    cout << "Largeur:\t" << box.width << endl;				// largeur
    double excentricite = (double)box.height/(double)box.width;
    cout << "Excentricite:\t" << excentricite << endl;			// excentricité
    cout << "Aire:\t\t" << contourArea(contours[i],false) << endl;	// aire
    cout << "Perimetre:\t" << arcLength(contours[i],true) << endl;	// perimetre
    double circularite = (double)(4*CV_PI*contourArea(contours[i],false))/(double)(arcLength(contours[i],true)*arcLength(contours[i],true));
    cout << "Circularite:\t" << circularite << endl;			// circularité
    double compacite = (double)(sqrt((4.0/CV_PI)*contourArea(contours[i],false)))/(double)(box.height);
    cout << "Compacite:\t" << compacite << endl;			// compacité
    HuMoments(moments(contours[i]), hu);
    for(int i=0; i<(sizeof(hu)/sizeof(*hu)); i++)
      cout << "Hu" << i << ":\t\t" << hu[i] << endl; 			// moment de hu
    cout << endl;
  }

  /*
  // perimetre enveloppe convexe
  vector<vector<Point> > hullh(contours.size());
  for (int i = 0; i < contours.size(); i++){
    convexHull(contours[i], hullh[i], false);
  }
  for(int i=0;i<hullh.size();i++){
    cout << arcLength(hullh[i],true) << endl << endl;
  }
  */
  
  cout << "Contours count : " << contours.size() << endl;
  

  // affichage
  RNG rng(12345);
  vector<vector<Point> > contours_poly( contours.size() );
  vector<Rect> boundRect( contours.size() );
  vector<Point2f>centers( contours.size() );
  vector<float>radius( contours.size() );
  for( size_t i = 0; i < contours.size(); i++ )
  {
      approxPolyDP( contours[i], contours_poly[i], 3, true );
      boundRect[i] = boundingRect( contours_poly[i] );
      minEnclosingCircle( contours_poly[i], centers[i], radius[i] );
  }
  Mat drawing = Mat::zeros( canny_output.size(), CV_8UC3 );
  for( size_t i = 0; i< contours.size(); i++ )
  {
      Scalar color = Scalar( rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256) );
      drawContours( drawing, contours_poly, (int)i, color );
      //rectangle( drawing, boundRect[i].tl(), boundRect[i].br(), color, 1 );
      //circle( drawing, centers[i], (int)radius[i], color, 1 );
  }
  imwrite("opencv.png",drawing);

/*
  namedWindow("Display window", cv::WINDOW_AUTOSIZE);
  imshow("Display window", canny_output);
*/
  //imwrite("opencv.png", canny_output );

  //waitKey(0);
  
  return 0;
}
