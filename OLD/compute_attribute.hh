#ifndef COMPUTE_ATTRIBUTE_HH
# define COMPUTE_ATTRIBUTE_HH

# include "tree.hh"



namespace mln
{

  namespace my
  {


    template <typename A>
    image2d<typename A::value_type>
    compute_attribute(const tree& t)
    {
      image2d<A> acc(t.domain());

      for (unsigned i = 0; i < t.S.size(); ++i)
        {
          point2d p = t.S[i];
          if (t.is_representative(p))
	    acc(p).init(p); // initialization for each node
        }

      image2d<typename A::value_type> attr(t.domain());
      for (int i = t.S.size() - 1; i >= 0; --i)
        {
          point2d
	    p = t.S[i],  // p goes from leaves to root
            q = t.parent(p);

          if (t.is_representative(p))
            {
              // done with this node so store the result
              attr(p) = acc(p).value();
              // and push info towards the parent node
              if (not t.is_root(p))
                acc(q).take(acc(p));			
            }
          else
            // take into account p
            acc(q).take(p);
        }

      t.back_propagate(attr);
      return attr;
    }

    
  } // mln::my

} // mln


#endif // ndef COMPUTE_ATTRIBUTE
