#include <cmath>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>

#include <mln/morpho/closing/structural.hh>
#include <mln/morpho/opening/structural.hh>
#include <mln/arith/min.hh>
#include <mln/data/median.hh>

#include <mln/win/hline2d.hh>
#include <mln/win/vline2d.hh>



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.xxx output.xxx" << std::endl;
  std::abort();
}

// g++ -std=c++11 -DNDEBUG -O3 -I. regroup.cc -o regroup

int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 3)
    usage(argv);
  
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]);

  image2d<int_u8>
    //v = morpho::opening::structural(input, win::hline2d(13)),
    v = morpho::opening::structural(input, win::hline2d(21)),
    h = morpho::opening::structural(input, win::vline2d(1));

  //v = data::median(v, win::hline2d(5));
  v = data::median(v, win::hline2d(7));
  h = data::median(h, win::vline2d(1));

  input = arith::min(h, v);
  io::pgm::save(input, argv[2]);
  
}
