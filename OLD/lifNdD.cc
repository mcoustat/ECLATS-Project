#include <iostream>
#include "json.hpp"

#include <unistd.h>
#include <stdio.h>
#include <errno.h>

#include <mln/io/pgm/load.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include "min_tree.hh"
#include "compute_attribute.hh"
#include "bbox.hh"

#include <limits.h>
#include <stdlib.h>


// g++ -std=c++11 -DNDEBUG -O3 -I. lifNdD.cc -o lifNdD

int main(int argc, char* argv[])
{
  using json = nlohmann::json;
  using namespace mln;
  using value::int_u8;
  
  // BEFORE USE
  // convert step7.pgm -threshold 70% step8.pgm
  
  // Setting up
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]);
  my::tree t = my::min_tree(input);
  
  // Finding image's path
  std::string imgpath = std::string("{ \"imagePath\": \"");
  char *full_path = realpath(argv[1], NULL);
  imgpath = imgpath + full_path;
  free(full_path);
  /*
  std::string imgpath;
  std::string attr = std::string("{ \"imagePath\": \"");
  char cwd[1024];
  if (getcwd(cwd, sizeof(cwd)) != NULL){
    std::string dir = std::string(cwd);
    std::string img = std::string(argv[1]);
    imgpath = attr + dir + "/" + img;
  } else
    perror("getcwd() error");
  */

  // Setting default attributes
  std::string defaults = std::string("\",\"fillColor\": [255,0,0,128],\"lineColor\": [0,255,0,128],\"shapes\": [");

  // Adding shapes of GT
  image2d<box2d> bbs = my::compute_attribute< my::bbox >(t);
  std::string shapes;
  for (unsigned i = 1; i < t.S.size(); ++i){ 
    point2d p = t.S[i];		// from beginning (except root [i=1 not 0 in the previous line]) to leaves
    if (
        t.is_representative(p)		// noeud représentatif
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmin().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y1)!=(X2,Y2) diagonales opposées
        and ( point2d(bbs(p).pmin().col(),bbs(p).pmax().row()) != point2d(bbs(p).pmax().col(),bbs(p).pmax().row()) )			// (X1,Y2)!=(X2,Y2) minimum 4 points distincts
        and bbs(p).pmin().col() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmin().row() != 0	// point d'origine différent du sommet de l'image (contour)
        and bbs(p).pmax().col() != bbs(t.S[0]).pmax().col()	// point d'extrémité différent du bas de l'image (contour)
        and bbs(p).pmax().row() != bbs(t.S[0]).pmax().row()	// point d'extrémité différent du bas de l'image (contour)
        and bbs(p).pmax().col()-bbs(p).pmin().col() > bbs(t.S[0]).pmax().col()*0.0080 // longueur minimale d'un nom de département
        and bbs(p).pmax().col()-bbs(p).pmin().col() < bbs(t.S[0]).pmax().col()*0.0500 // longueur maximale d'un nom de département
        and bbs(p).pmax().row()-bbs(p).pmin().row() > bbs(t.S[0]).pmax().row()*0.0040 // hauteur minimale d'un nom de département
        and bbs(p).pmax().row()-bbs(p).pmin().row() < bbs(t.S[0]).pmax().row()*0.0065 // hauteur maximale d'un nom de département
       ){
      // X1,Y1 -> ┌------------------┐ <- X2, Y1
      //          │   bounding-box   │
      // X1,Y2 -> └------------------┘ <- X2, Y2
      // points:[ X1 , Y1 ],[ X2 , Y1 ],[ X2 , Y2 ],[ X1 , Y2 ]
      // X1 -> bbs(p).pmin().col()  ---  Y1 -> bbs(p).pmin().row()
      // X2 -> bbs(p).pmax().col()  ---  Y2 -> bbs(p).pmax().row()
      shapes = shapes + "{\"fill_color\": null,\"label\": \"dept\",\"line_color\": null,\"points\": [["+std::to_string(bbs(p).pmin().col())+","+std::to_string(bbs(p).pmin().row())+"],["+std::to_string(bbs(p).pmax().col())+","+std::to_string(bbs(p).pmin().row())+"],["+std::to_string(bbs(p).pmax().col())+","+std::to_string(bbs(p).pmax().row())+"],["+std::to_string(bbs(p).pmin().col())+","+std::to_string(bbs(p).pmax().row())+"]]},";
    }  
  }
  shapes.pop_back();

  // End of the LIF
  std::string footer = std::string("]}");

  // Concate strings into JSON
  auto j = json::parse( imgpath + defaults + shapes + footer );
  /*
  size_t slashindex = std::string(argv[1]).find_last_of("/");  //A PAUFINER POUR LES CAS COMPLEXES -> REDIRECTION DE DOSSIER
  if( slashindex == std::string::npos)
    slashindex = 0;
  */
  size_t pointindex = std::string(argv[1]).find_last_of(".");
  if( pointindex == std::string::npos)
    pointindex = std::string(argv[1]).size();
  //std::string filename = std::string(argv[1]).substr(slashindex, pointindex); 
  std::string filename = std::string(argv[1]).substr(0, pointindex);
  std::ofstream o(filename+".lif");
  o << std::setw(4) << j << std::endl;

  // Print the JSON
  std::cout << j.dump(4) << std::endl;

}
