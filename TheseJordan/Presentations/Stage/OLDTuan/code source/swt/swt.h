#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <math.h>
#include <fstream>
#include <typeinfo>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace cv;
using namespace std;

#define PREFIX_IMAGE "img_"
#define PREFIX_DATA "data_"

#define PI 3.14159265
#define MAX_STROKE_WIDTH 15
#define MAX_VARIANCE_SW 3 // max variance of stroke width in a component
#define MAX_VARIANCE_MEANSW_RATIO 0.5
#define MIN_ASPECT_RATIO 0.1
#define MAX_ASPECT_RATIO 10
#define MAX_DIAMETER_MEDIAN_RATIO 10 // 15 ok
#define MAX_INSIDE_COMPONENTS 2
#define MIN_LETTER_HEIGHT 10 // min height of a letter
#define MAX_LETTER_HEIGHT 300 // max height of a letter
#define MAX_SW_RATIO 2
#define MAX_HEIGHT_RATIO 2
#define MAX_DISTANCE_WIDTH_RATIO 3
#define CHAIN_SIZE 2 //number of letters in a qualified groups, (3 not ok :OF, A ...)


enum Purpose{
	COMPUTING = 1,
	REFINING = 2
};
struct Pair{
    int left;
    int right;
    Pair(int left, int right):left(left), right(right){}
};
enum Direction{
    BLACK2WHITE = -1,
    WHITE2BLACK = 1
};

double t0;
double elapsed;

int printTrace = 0;
int showImages = 0;
int writeImages = 0;
int comparing = 0;
int debug = 0;
int testing = 0; // testing mode

Mat orgImg; // original image
Mat grayImg; // gray image
Mat binImg; // binary image
Mat invBinImg; // inverse binary image
Mat gausImg; // gaussian image
Mat edgeImg; // edge image
vector<Point> edgePoints;
Mat theta; // gradient direction per pixels
Mat swtImg; // stroke image
Mat ccImg; // connected components image
vector<Rect> componentsROI;
size_t nbComponent;
bool *isLetters;
bool *isGrouped;
vector<Pair> hCompGroups;
vector<Pair> vCompGroups;
vector<Rect> nonGroupedComponents;
vector< vector<int> > horizontalChains;
vector< vector<int> > verticalChains;
vector<Rect> boundingBoxes;


// functions
void printHelp(char *s);
void setTimeCounter();
void getElapsedTime();
void printElapsedTime(const int isIndent=1);

void showImage(const Mat img, const char* title, const int isAutoSize=1);
void writeImage(const Mat img, const string fname);
void printImage(const Mat img, int top=-1, int left=-1, int bottom=-1, int right=-1, const int floatingPoint=0);
void printImage2File(const Mat img, const char* fname="out.txt", const int isNormal=1, int top=-1, int left=-1, int bottom=-1, int right=-1);
double getMSE(const Mat img1, const Mat img2);
void compareImageFiles(const string f1, const string f2);
void printMatInfo(const Mat img, const string vname);
void drawROIs(const Mat img, const vector<Rect> ROIs, Mat& drawingROI);
void extractROIs(const Mat img, const vector<Rect> ROIs, Mat &extractedROI, int background=0);
bool isPointInROIs(Point p, vector<Rect> rois);


void estimateThreshold(const Mat img, int &lowThreshold, int &highThreshold);
void getStrokeImage(const Direction drt);
void computeStrokeWidth(vector<Point>& strokePoints, const Direction drt, const Purpose p);
int findComponents(); // version 4.0
void getLetterCandidates(); // version 2.0
float getMeanSwComponent(const Rect comp, const int compIndex);
void groupLetters();
void merge(const vector<int> token, vector<int> &chain); // merge not found item in token to end of chain
bool mergePairs(const vector< vector<int> > initChains, vector< vector<int> > &chains);
void mergePairs(const vector<Pair> groups, vector< vector<int> > &chains, bool* chainedComps = NULL, bool bVertical = false);
void chains2Boxes(const vector< vector<int> > chains, vector<Rect> &boxes, bool* chainedComponents = NULL);
void chainPairs();


void test();
