#include "swt.h"
#include "functions.cpp"

int main(int argc, char* argv[]){
//  /*  GET OPTIONS
    int c;
    char *fname = NULL;
    while( (c = getopt(argc, argv, "f:t:s:w:d:e:h")) != EOF ){
        extern char* optarg;
        switch (c){
            case 'f': // input image file
                fname = optarg;
                break;
            case 't': // print trace
                printTrace = atoi(optarg);
                break;
            case 's': // show images in each step
                showImages = atoi(optarg);
                break;
            case 'w': // write images in each step
                writeImages = atoi(optarg);
                break;
            case 'd': // debugging mode
                debug = atoi(optarg);
                break;
            case 'e': // testing mode
                testing = atoi(optarg);
                break;
            case 'h': // display help
                printHelp(argv[0]);
                return 0;
                break;
        }
    }
    extern int optind;
    fname = (fname == NULL) ? (char*)("exmap.tif") : fname;
//    GET OPTIONS */
    

    // 1. Read image
    if(printTrace < 0 || printTrace == 1){
        cout << "1. Read image" << endl;
        cout << "--- input: fname" << "\n--- output: grayImg" << endl;
    }
    
    setTimeCounter();
    grayImg = imread(fname, 0);
    getElapsedTime();
    
    if(grayImg.empty()){
        cout << "Could NOT read the image: " << fname << endl;
        return -1;
    }
    
    if(printTrace < 0 || printTrace == 1){
        cout << "--- file name: " << fname << endl;
        cout << "--- image size: " << grayImg.cols << "x" << grayImg.rows << endl;
        printElapsedTime();
    }
    if(debug == 1 || debug < 0){
        printMatInfo(grayImg, "grayImg");
        cout << "[debug] range (0,0):(4,4) of gray image" << endl;
        printImage(grayImg, 0, 0, 4, 4);
        int isNormal = 0;
        printImage2File(grayImg, "gray.txt", isNormal, 0, 0, 4, 4);
        cout << "[debug] data of gray image is saved to file " << PREFIX_DATA << "gray.txt" << endl;
    }
    if(showImages == 1 || showImages < 0) showImage(grayImg, "Gray Image");
    if(writeImages == 1 || writeImages < 0){
        writeImage(grayImg, "gray.jpg");
        cout << "[writeImage] gray image is saved in file " << PREFIX_IMAGE << "gray.jpg" << endl;
    }


    // 2. Convert image to binary using OTSU's algorithm
    if(printTrace < 0 || printTrace == 2){
        cout << "2. Convert image to BINARY using OTSU algorithm" << endl;
        cout << "--- input: grayImg" << "\n--- output: binImg" << endl;
    }
    
    setTimeCounter();
    threshold(grayImg, binImg, 0, 255, THRESH_OTSU);
    bitwise_not(binImg, invBinImg);
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 2) printElapsedTime();
    if(debug == 2 || debug < 0){
        cout << "[debug] MSE(grayImg, binImg) = " << getMSE(grayImg, binImg) << endl;
        cout << "[debug] range (0,0):(4,4) of binary image" << endl;
        printImage(binImg, 0, 0, 4, 4);
        printImage2File(binImg, "binary.txt");
        cout << "[debug] data of binary image is saved to file " << PREFIX_DATA << "binary.txt" << endl;
    }
    if(showImages == 2 || showImages < 0) showImage(binImg, "Binary Image");
    if(writeImages == 2 || writeImages < 0){
        writeImage(binImg, "binary.jpg");
        cout << "[writeImage] binary image is saved in file " << PREFIX_IMAGE << "binary.jpg" << endl;
    }

    
    // 3. Remove noise using Gaussian blur
    if(printTrace < 0 || printTrace == 3){
        cout << "3. Remove NOISE using Gaussian blur" << endl;
        cout << "--- input: binImg" << "\n--- output: gausImg" << endl;
    }
    
    setTimeCounter();
    GaussianBlur(binImg, gausImg, Size(5,5), 0, 0); // remove noise
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 3) printElapsedTime();
    if(debug == 3 || debug < 0){
        int isNormal = 1;
        printImage2File(gausImg, "gaussian.txt", isNormal);
        cout << "[debug] data of gaussian image is saved to file " << PREFIX_DATA << "gaussian.txt" << endl;
    }
    if(showImages == 3 || showImages < 0) showImage(gausImg, "Gaussian Image");
    if(writeImages == 3 || writeImages < 0){
        writeImage(gausImg, "gaussian.jpg");
        cout << "[writeImage] gaussian image is saved in file " << PREFIX_IMAGE << "gaussian.jpg" << endl;
    }
    
    // 4. Compute EDGE image by using Canny
    if(printTrace < 0 || printTrace == 4){
        cout << "4. Compute EDGE by using Canny" << endl;
        cout << "--- input: gausImg" << "\n--- output: edgeImg" << endl;
    }

    setTimeCounter();
    int lowThreshold; int highThreshold;
    estimateThreshold(gausImg, lowThreshold, highThreshold);
    Canny(gausImg, edgeImg, lowThreshold, highThreshold);
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 4){
        cout << "--- low threshold = " << lowThreshold << "\n--- high threshold = " << highThreshold << endl;
        cout << "--- map points = " << edgeImg.rows * edgeImg.cols << endl;
        printElapsedTime();
    }
    
    if(debug == 4 || debug < 0) cout << "[debug] #NonZero pixel on edge image = " << countNonZero(edgeImg) << endl;
    if(showImages == 4 || showImages < 0) showImage(edgeImg, "Edge Image");
    if(writeImages == 4 || writeImages < 0){
        writeImage(edgeImg, "edge.jpg");
        cout << "[writeImage] edge image is saved in file " << PREFIX_IMAGE << "edge.jpg" << endl;
    }

    
    // 5. Compute GRADIENT by using Sobel
    if(printTrace < 0 || printTrace == 5){
        cout << "5. Compute GRADIENT by using Sobel" << endl;
        cout << "--- input: edgeImg" << "\n--- output: edgePoints, theta" << endl;
    }
    
    setTimeCounter();
    Mat dx, dy;    
    Sobel(gausImg, dx, CV_32FC1, 1, 0, 3);
    Sobel(gausImg, dy, CV_32FC1, 0, 1, 3);

    if(edgePoints.size()) edgePoints.clear();
    theta = Mat(grayImg.size(), CV_32FC1);
    for(int i = 0; i < grayImg.rows; i++){
        uchar* row_edge = edgeImg.ptr<uchar>(i);
        float* row_theta = theta.ptr<float>(i);
        float* row_dx = dx.ptr<float>(i);
        float* row_dy = dy.ptr<float>(i);
        
        for(int j = 0; j < grayImg.cols; j++){
            if((int)row_edge[j] == 255){ // white means the point is on edge
                row_theta[j] = atan2(row_dy[j], row_dx[j]); // gradient direction
                edgePoints.push_back(Point(j,i));
            }
        }
    }    
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 5){
        cout << "--- edge points = " << edgePoints.size() << endl;
        printElapsedTime();
    }
    if(debug == 5 || debug < 0){
        printImage2File(theta, "theta.txt");
        cout << "[debug] values of theta are saved in the file " << PREFIX_DATA << "theta.txt" << endl;
        
        ofstream f;
        f.open("data_theta_negative.txt", ios::out);
        for(int i=0; i < grayImg.rows; i++){
            float* row = theta.ptr<float>(i);
            for(int j=0; j < grayImg.cols; j++)
                if(row[j] < 0) f << "(" << i << "," << j << "): " << row[j] << endl;
        }
        f.close();
        cout << "[debug] negative values of theta are saved in the file " << PREFIX_DATA << "data_theta_negative.txt" << endl;
    }
    if(showImages == 5 || showImages < 0){ showImage(dx, "dx"); showImage(dy, "dy"); }
    if(writeImages == 5 || writeImages < 0){
        writeImage(dx, "dx.jpg"); writeImage(dy, "dy.jpg");
        cout << "[writeImage] dx, dy images are saved in files " << PREFIX_IMAGE << "dx.jpg, and " << PREFIX_IMAGE << "dy.jpg" << endl;
    }


    // 6. Compute STROKE WIDTH
    if(printTrace < 0 || printTrace == 6){
        cout << "6. Compute STROKE WIDTH" << endl;
        cout << "--- input: edgeImg, edgePoints, theta" << "\n--- output: swtImg" << endl;
    }
    
    setTimeCounter();
    float initSW = MAX_STROKE_WIDTH * 2;
    swtImg = Mat(grayImg.size(), CV_32FC1, Scalar(initSW));
    getStrokeImage(BLACK2WHITE);
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 6) printElapsedTime();
    if(debug == 6 || debug < 0){
        printImage2File(swtImg, "swt.txt");
        cout << "[debug] data of stroke image is saved in the file " << PREFIX_DATA << "swt.txt" << endl;
        cout << "[debug] #nonZeroPixels of stroke image = " << countNonZero(swtImg) << endl;
    }
    if(showImages) showImage(swtImg, "Stroke Image");
    if(writeImages){
        writeImage(swtImg*100, "stroke.jpg");
        cout << "[writeImage] stroke image is saved in file " << PREFIX_IMAGE << "stroke.jpg" << endl;
    }


    // 7. Find connected components
    if(printTrace < 0 || printTrace == 7){
        cout << "7. Find COMPONENTS" << endl;
        cout << "--- input: swtImg" << "\n--- output: ccImg, componentsROI, nbComponent" << endl;
    }
    
    setTimeCounter();
    int initCompVal = -1;
    ccImg = Mat(grayImg.size(), CV_32FC1, Scalar(initCompVal));
    componentsROI.clear();
    nbComponent = findComponents(); // version 4.0
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 7){
        cout << "--- #Components = " << componentsROI.size() << endl;
        printElapsedTime();
    }
    
    if(debug == 7 || debug < 0){
        Mat extractedComp(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, componentsROI, extractedComp);
        writeImage(extractedComp, "components_black.jpg");
        cout << "[debug] image of components extracting from inverted binary image is saved in file " << PREFIX_IMAGE << "components_black.jpg" << endl;
        
        extractedComp = Mat(grayImg.size(), CV_32FC1);
        extractROIs(binImg, componentsROI, extractedComp, 255);
        writeImage(extractedComp, "components_white.jpg");
        cout << "[debug] image of components extracting from binary image is saved in file " << PREFIX_IMAGE << "components_white.jpg" << endl;
    }
    if(showImages == 7 || showImages < 0){
        Mat drawnComp(grayImg.size(), CV_32FC1);
        drawROIs(ccImg, componentsROI, drawnComp);
        showImage(drawnComp*nbComponent, "Connected Components");
    }
    if(writeImages == 7 || writeImages < 0){
        Mat drawnComp(grayImg.size(), CV_32FC1);
        drawROIs(ccImg, componentsROI, drawnComp);
        writeImage(drawnComp*nbComponent*20, "components.jpg");
        cout << "[writeImage] image of components is saved in file " << PREFIX_IMAGE << "components.jpg" << endl;
    }


    // 8. Get Letter Candidates
    if(printTrace < 0 || printTrace == 8){
        cout << "8. Get Letter CANDIDATES" << endl;  
        cout << "--- input: componentsROI, nbComponent" << "\n--- output: isLetters" << endl;
    }
    
    setTimeCounter();
    getLetterCandidates(); // version 2.0
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 8){
        int nbLetterCandidate = 0;
        for(int i = 0; i < nbComponent; i++)
            if(isLetters[i]) nbLetterCandidate++;
        cout << "--- #LetterCandidates = " << nbLetterCandidate << endl;
        printElapsedTime();
    }
    if(debug == 8 || debug < 0){
        vector<Rect> lettersROI;
        for(int i = 0; i < nbComponent; i++)
            if(isLetters[i]) lettersROI.push_back(componentsROI[i]);
        Mat letterCandidate(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, lettersROI, letterCandidate);
        writeImage(letterCandidate, "candidate.jpg");
        cout << "[debug] image of letter candidates is saved in file " << PREFIX_IMAGE << "candidate.jpg" << endl;
    }


    // 9. GROUP Letters
    if(printTrace < 0 || printTrace == 9){
        cout << "9. GROUP Letters" << endl;
        cout << "--- input: componentsROI, nbComponent, isLetters" << "\n--- output: hCompGroups, vCompGroups, nonGroupedComponents" << endl;
    }
    
    setTimeCounter();
    groupLetters();
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 9){
        cout << "--- #hCompGroups = " << hCompGroups.size() << endl;
        cout << "--- #vCompGroups = " << vCompGroups.size() << endl;
        cout << "--- #nonGroupedComponents = " << nonGroupedComponents.size() << endl;
        printElapsedTime();
    }
    if(debug == 9 || debug < 0){
        Mat nonGroup(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, nonGroupedComponents, nonGroup);
        writeImage(nonGroup, "non_group.jpg");
        cout << "[debug] image of non grouped components is saved in file " << PREFIX_IMAGE << "non_group.jpg" << endl;

        vector<Rect> groupedComponents;
        for(size_t i = 0; i < nbComponent; i++){
            if(isGrouped[i]) groupedComponents.push_back(componentsROI[i]);
        }
        Mat group(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, groupedComponents, group);
        writeImage(group, "group.jpg");
        cout << "[debug] image of grouped components is saved in file " << PREFIX_IMAGE << "group.jpg" << endl;
    }
    
    
    // 10. Chain Pairs
    if(printTrace < 0 || printTrace == 10){
        cout << "10. CHAIN Pairs" << endl;
        cout << "--- input: hCompGroups, nbComponent, componentsROI, grayImg" << "\n--- output: horizontalChains, boundingBoxes" << endl;
    }
    
    setTimeCounter();
    chainPairs();
    getElapsedTime();
    
    if(printTrace < 0 || printTrace == 10){
        cout << "--- #horizontalChains = " << horizontalChains.size() << endl;
        cout << "--- #boundingBoxes = " << boundingBoxes.size() << endl;
        printElapsedTime();
    }
    
    Mat boxes(grayImg.size(), CV_32FC1);
    extractROIs(invBinImg, boundingBoxes, boxes);
    writeImage(boxes, "boxes.jpg");
    cout << "[result] image of bounding boxes extracted from inverted binary image is saved in file " << PREFIX_IMAGE << "boxes.jpg" << endl;
    
    boxes = Mat(grayImg.size(), CV_32FC1);
    orgImg = imread(fname, 1);
    drawROIs(orgImg, boundingBoxes, boxes);
    writeImage(boxes, "boxes_org.jpg");
    cout << "[result] image of bounding boxes drawn on original image is saved in file " << PREFIX_IMAGE << "boxes_org.jpg" << endl;
    
    
    waitKey();
    return 0;
}
