void printHelp(char *s){
    cout << "Usage:    " << s << " [-option] [argument]" << endl;
    cout << "option:   " << "-h    display help" << endl;
    cout << "          " << "-f    name of input image. Default: exmap.tif" << endl;
    cout << "          " << "-t    print trace in each step. Default: 0" << endl;
    cout << "          " << "-s    show images in each step. Default: 0" << endl;
    cout << "          " << "-w    write images in each step. Default: 0" << endl;
    cout << "          " << "-d    enter to debug mode. Default: 0" << endl;
}
void setTimeCounter(double &t){
    t = (double)getTickCount();
}
void getElapsedTime(double &t){
    elapsed = ((double)getTickCount() - t)/getTickFrequency();
}
void printElapsedTime(const int isIndent){
    if(isIndent) cout << "--- elapsed time = " << elapsed << " seconds" << endl;
    else cout << "elapsed time = " << elapsed << " seconds" << endl;
}


void showImage(const Mat img, const char* title, const int isAutoSize){
    if(isAutoSize) namedWindow(title, WINDOW_AUTOSIZE);
    else namedWindow(title, WINDOW_NORMAL);
    imshow(title, img);
}
void writeImage(const Mat img, const string fname){
    imwrite(PREFIX_IMAGE + fname, img);
}

// print image data to console
void printImage(const Mat img, int top, int left, int bottom, int right, const int floatingPoint){
    char fpn[50];
    sprintf(fpn, "%%.%df ", floatingPoint);
    
    top = (top < 0) ? 0 : top;
    left = (left < 0) ? 0 : left;
    bottom = (bottom < 0) ? img.rows-1 : bottom;
    right = (right < 0) ? img.cols-1 : right;
    
    top = (top < img.rows) ? top : img.rows-1;
    bottom = (bottom < img.rows) ? bottom : img.rows-1;
    left = (left < img.cols) ? left : img.cols-1;
    right = (right < img.cols) ? right : img.cols-1;
    
    for(int i=top; i<=bottom; i++){
        for(int j=left; j<=right; j++)
            if(!floatingPoint) printf("%.0f ", (double)img.at<uchar>(i,j));
            else printf(fpn, (double)img.at<uchar>(i,j));
        cout << endl;
    }
}

// print image to file
void printImage2File(const Mat img, const char* fname, const int isNormal, int top, int left, int bottom, int right){
    char filename[50];
    sprintf(filename, "%s%s", PREFIX_DATA, fname);
    ofstream f;
    f.open(filename, ios::out);
    
    if(isNormal) f << img;
    else{
        top = (top < 0) ? 0 : top;
        left = (left < 0) ? 0 : left;
        bottom = (bottom < 0) ? img.rows-1 : bottom;
        right = (right < 0) ? img.cols-1 : right;
        
        top = (top < img.rows) ? top : img.rows-1;
        bottom = (bottom < img.rows) ? bottom : img.rows-1;
        left = (left < img.cols) ? left : img.cols-1;
        right = (right < img.cols) ? right : img.cols-1;
        
        for(int i=top; i <= bottom; i++){
            for(int j=left; j <= right; j++)
                f << (double)img.at<uchar>(i,j) << " ";
            f << endl;
        }
    }
    
    f.close();
}

// get Mean Square Error
double getMSE(const Mat img1, const Mat img2){
    if(img1.size() != img2.size()) return -1;
    
    double MSE;   
    Mat diff;
    absdiff(img1, img2, diff);
    diff.convertTo(diff, CV_32FC1);
    Mat diff2(diff.size(), CV_32FC1);
    pow(diff, 2, diff2);
    MSE = sum(diff2)[0]/(img1.rows*img1.cols);
        
    return MSE;
}

// compare image files
void compareImageFiles(const string f1, const string f2){
    Mat img1 = imread(PREFIX_IMAGE + f1, 0);
    Mat img2 = imread(PREFIX_IMAGE + f2, 0);
    cout << "COMPARING " << f1 << " and " << f2 << ": MSE = " << getMSE(img1, img2) << endl;
}

// show Mat type info
void printMatInfo(const Mat img, const string vname){
    cout << vname << " INFO:" << endl;
    cout << "--- type name = " << typeid(img).name() << endl;
    cout << "--- depth = " << img.depth() << endl;
    cout << "--- channels = " << img.channels() << endl;
}

// draw ROIs
void drawROIs(const Mat img, const vector<Rect> ROIs, Mat &drawnROI){
//    size_t nbROI = rois.size();
//    drawnROI = img*(1.0/static_cast<float>(nbROI));
    drawnROI = img.clone();
    for(size_t i = 0; i < ROIs.size(); i++){
        Rect r = ROIs[i];
        rectangle(drawnROI, Point(r.x, r.y), Point(r.x + r.width - 1, r.y + r.height - 1), Scalar(255,0,0), 1);
    }
}

// extract ROIs
void extractROIs(const Mat img, const vector<Rect> ROIs, Mat &extractedROI, int background){
    extractedROI = Mat(img.size(), CV_32FC1, background);
    for(size_t i = 0; i < ROIs.size(); i++){
        Rect r = ROIs[i];
        img(Rect(r.x, r.y, r.width, r.height)).copyTo(extractedROI(Rect(r.x, r.y, r.width, r.height)));
    }
}

bool isPointInROIs(Point p, vector<Rect> rois){
    bool res = false;
    for(size_t i = 0; i < rois.size(); i++){
        Rect *ir = &rois[i];
        if(ir->contains(p)) return true;
    }
    
    return res;
}

/*
    ESTIMATE_THRESHOLD
    estimates hysteresis threshold, assuming that the top X% (as defined by the HIGH_THRESHOLD_PERCENTAGE) of edge pixels with the greatest intesity are true edges
    and that the low threshold is equal to the quantity of the high threshold plus the total number of 0s at the low end of the histogram divided by 2
    
    NEED improve after
*/
void estimateThreshold(const Mat img, int &lowThreshold, int &highThreshold){
    Mat eqhImg = img.clone();
    equalizeHist(eqhImg, eqhImg);
    
    lowThreshold = 0.66 * mean(eqhImg)[0];
    highThreshold = 1.33 * mean(eqhImg)[0];
}

void getStrokeImage(const Direction drt){
    vector<Point> strokePoints;

    computeStrokeWidth(strokePoints, drt, COMPUTING); // compute strokes normaly
    computeStrokeWidth(strokePoints, drt, REFINING); // refine strokes based on median stroke value
}

void computeStrokeWidth(vector<Point>& strokePoints, const Direction drt, const Purpose p){
    vector<Point> pointStack;
    vector<float> swValues;
    for(vector<Point>::iterator ipt = edgePoints.begin(); ipt != edgePoints.end(); ipt++){
        pointStack.clear();
        swValues.clear();
        
        float step = 1;
        float ix = (*ipt).x; float iy = (*ipt).y;
        float curX = ix; float curY = iy;
        bool isStroke = false;
        float iTheta = theta.at<float>(curY, curX);
        pointStack.push_back(Point(curX, curY));
        swValues.push_back(swtImg.at<float>(curY, curX));
        
        while(step < MAX_STROKE_WIDTH){
            float nextX = round(ix + cos(iTheta)*drt*step);
            float nextY = round(iy + sin(iTheta)*drt*step);
            
            if(nextX < 0 || nextX >= edgeImg.cols || nextY < 0 || nextY >= edgeImg.rows) break;
            step += 1;
            if(curX == nextX && curY == nextY) continue;
            
            curX = nextX; curY = nextY;
            pointStack.push_back(Point(curX, curY));
            swValues.push_back(swtImg.at<float>(curY, curX));
            
            if(edgeImg.at<uchar>(curY, curX) == 255){ // condition of pixel on edge
                float jTheta = theta.at<float>(curY, curX);
                if( abs(abs(iTheta-jTheta)-PI) < PI/2 ){ // condition of roughly opposing
                    isStroke = true;
                    if(p == COMPUTING) strokePoints.push_back(Point(ix, iy));
                }
                break;
            }
        }
        
        if(isStroke){
            float newSwtVal;
            if(p == COMPUTING){ // compute stroke width based on distance between edges
                newSwtVal = sqrt( pow((curY - iy), 2) + pow((curX - ix), 2) );
            } else if(p == REFINING){ // refine strokes based on median value
                nth_element( swValues.begin(), swValues.begin() + swValues.size()/2, swValues.end() );
                newSwtVal = swValues[swValues.size()/2];
            }
            
            for(size_t i = 0; i < pointStack.size(); i++){
                swtImg.at<float>(pointStack[i].y, pointStack[i].x) = min(swtImg.at<float>(pointStack[i].y, pointStack[i].x), newSwtVal);
            }
        }
    } // end loop through edge points
    
    // set unchanged value pixel to 0
    float initSW = MAX_STROKE_WIDTH * 2;
    for(int i = 0; i < swtImg.rows; i++){
        float* row = swtImg.ptr<float>(i);
        for(int j = 0; j < swtImg.cols; j++){
            if(row[j] == initSW) row[j] = 0;
        }
    }
}

// version 4.0
int findComponents(){
    int initCompVal = ccImg.at<float>(0, 0);
    int rejectedCompVal = -2;
    vector<Point2i> offsets; // offsets for 8 neighbor pixels
    int label = 0; // label of found component
    
    for(int i=-1; i < 2; i++){
        for(int j=-1; j<2; j++)
            if(pow(i,2) + pow(j,2) > 0) offsets.push_back(Point2i(i,j));
    }
    
    for(int i=0; i < ccImg.rows; i++){
        float* rowSw = swtImg.ptr<float>(i);
        float* rowCc = ccImg.ptr<float>(i);
        for(int j = 0; j < ccImg.cols; j++){
            if(rowSw[j] == 0) rowCc[j] = rejectedCompVal;
        }
    }
    
    vector<Point2i> connectedPixels;
    for(int i=0; i < ccImg.rows; i++){
        float* rowCc = ccImg.ptr<float>(i);
        for(int j=0; j < ccImg.cols; j++){
            bool isConnected = false;
            connectedPixels.clear();
            if(rowCc[j] == initCompVal){ // this pixel is not yet scanned
                int scanning = 0;
                int iPixel = 0;
                connectedPixels.push_back(Point2i(j,i));
                while(scanning >= 0){
					scanning--;
                    Point2i p = connectedPixels[iPixel];
                    for(int inb = 0; inb < offsets.size(); inb++){ // consider neighbor pixels q of p
                        Point2i q = p + offsets[inb];
                        
                        if(q.x < 0 || q.x >= ccImg.cols || q.y < 0 || q.y >= ccImg.rows) continue;
                        if(ccImg.at<float>(q.y, q.x) == rejectedCompVal) continue;
                        if(ccImg.at<float>(q.y, q.x) == initCompVal){
                            float pSw = swtImg.at<float>(p.y, p.x);
                            float qSw = swtImg.at<float>(q.y, q.x);
                            if(max(pSw, qSw)/min(pSw, qSw) <= MAX_SW_GROUPABLE_PIXELS_RATIO){
                                ccImg.at<float>(p.y, p.x) = label;
                                ccImg.at<float>(q.y, q.x) = label;
                                connectedPixels.push_back(q);
                                scanning++;
                                isConnected = true;
                            }
                        }
                    }
                    if(isConnected) iPixel++;
                }
                
                if(isConnected){
                    int left = ccImg.cols, right = 0;
                    int top = ccImg.rows, bottom = 0;
                    for(int iPx=0; iPx < connectedPixels.size(); iPx++){
                        Point2i p = connectedPixels[iPx];
                        left = min(left, p.x); right = max(right, p.x);
                        top = min(top, p.y); bottom = max(bottom, p.y);
                    }
                    int width = right - left + 1;
                    int height = bottom - top + 1;
                    
                    Rect compROI(left, top, width, height);
                    componentsROI.push_back(compROI);
                    label++;
                } else rowCc[j] = rejectedCompVal;
            }
        }
    }

    return label;
}

// version 2.0
void getLetterCandidates(){
    isLetters = new bool[nbComponent];
    memset(isLetters, true, nbComponent*sizeof(bool));
    
    for(size_t i = 0; i < nbComponent; i++){
        Rect r = componentsROI[i];
        
        // compute stroke width variance of component i
        vector<float> sw;
        vector<int> insideComps;
        int left = r.x; int right = left + r.width - 1;
        int top = r.y; int bottom = top + r.height - 1;
        for(int r = top; r <= bottom; r++){
            for(int c = left; c <= right; c++){
                float ccVal = ccImg.at<float>(r, c);
                if(ccVal < 0) continue; // init or rejected pixel
                if(ccVal != i){ // pixel belong to an inside component
                    if(find(insideComps.begin(), insideComps.end(), ccVal) == insideComps.end())
                        insideComps.push_back(ccVal);
                }
                if(ccVal == i) sw.push_back(swtImg.at<float>(r, c));
            }
        }

        float sumSw = 0.0f;
        size_t nbSw = sw.size();
        for(size_t j = 0; j < nbSw; j++) sumSw += sw[j];
        float meanSw = sumSw/nbSw;
        float varianceSw = 0.0f;
        for(size_t j = 0; j < nbSw; j++) varianceSw += pow(sw[j] - meanSw, 2);
        varianceSw /= nbSw;
        
        // rule 1: aspect ratio
        float ar = (float)r.height/r.width;
        if(ar < MIN_LETTER_ASPECT_RATIO || ar > MAX_LETTER_ASPECT_RATIO) isLetters[i] = false;
        
        // rule 2: reject too short and high components
        if(r.height < MIN_LETTER_HEIGHT || r.height > MAX_LETTER_HEIGHT) isLetters[i] = false;        
    }
}

void groupLetters(){
    isGrouped = new bool[nbComponent];
    memset(isGrouped, false, nbComponent*sizeof(bool));
    
    for(size_t i = 0; i < nbComponent-1; i++){
        if(!isLetters[i]) continue;
        Rect compi = componentsROI[i];
        
        for(size_t j = i+1; j < nbComponent; j++){
            if(!isLetters[j]) continue;
            Rect compj = componentsROI[j];
            
            // rule 1: check horizontal & vertical
            bool horizontal = !((compi.y > compj.y + compj.height - 1) || (compj.y > compi.y + compi.height - 1));
            bool vertical = !((compi.x > compj.x + compj.width - 1) || (compj.x > compi.x + compi.width - 1));
            if((!horizontal) && (!vertical)) continue;
            
            // rule 2: check distance height ratio
            float distance;
            if(horizontal && vertical){
                int hdistance = abs((compi.x + compi.width/2) - (compj.x + compj.width/2));
                int vdistance = abs((compi.y + compi.height/2) - (compj.y + compj.height/2));
                horizontal = (hdistance >= vdistance);
                vertical = !horizontal;
            }
            if(horizontal){
                distance = min(abs(compi.x - (compj.x + compj.width - 1)), abs(compj.x - (compi.x + compi.width - 1)));
            }
            if(vertical){
                distance = min(abs(compi.y - (compj.y + compj.height - 1)), abs(compj.y - (compi.y + compi.height - 1)));
            }
            if(distance/min(compi.height, compj.height) > MAX_DISTANCE_HEIGHT_RATIO) continue;
            
            // rule 3: aligment
            if(horizontal){
                if((compi.y + compi.height) < (compj.y + compj.height/3)) continue;
                if(compi.y > (compj.y + 2*compj.height/3)) continue;
                
                if((compj.y + compj.height) < (compi.y + compi.height/3)) continue;
                if(compj.y > (compi.y + 2*compi.height/3)) continue;
            }
            
            // otherwise
            isGrouped[i] = true;
            isGrouped[j] = true;
            
            if(horizontal) hCompGroups.push_back(Pair(i,j));
            if(vertical) vCompGroups.push_back(Pair(i,j));
        }
    }
    
    for(size_t i = 0; i < nbComponent; i++){
        if(!isGrouped[i]) nonGroupedComponents.push_back(componentsROI[i]);
    }
}

// merge not found item in token to end of chain
void merge(const vector<int> token, vector<int> &chain){
    vector<int>::iterator iter;
    for(size_t i = 0; i < token.size(); i++){
        iter = find(chain.begin(), chain.end(), token[i]);
        if(iter == chain.end()){ // not found token[i] in chain
            chain.push_back(token[i]);
        }
    }
}

bool mergePairs(const vector< vector<int> > initChains, vector< vector<int> > &chains){
    if(chains.size()) chains.clear();
    
    bool merged = false;
    size_t nbChain = initChains.size();
    int* merged2Word = new int[nbChain];
    memset(merged2Word, -1, nbChain*sizeof(int));
    
    for(size_t i = 0; i < nbChain; i++){
        if(merged2Word[i] != -1) continue;
        
        vector<int> chaini = initChains[i];
        for(size_t j = i+1; j < nbChain; j++){
            vector<int> chainj = initChains[j];
            for(size_t u = 0; u < chaini.size(); u++){
                for(size_t v = 0; v < chainj.size(); v++){
                    if(chaini[u] == chainj[v]){
                        merged = true;
                        if(merged2Word[j] != -1){ // chainj is already merged with others
                            merge(chaini, chains[merged2Word[j]]);
                            merged2Word[i] = merged2Word[j];
                        } else{ // new chain
                            vector<int> nchain;
                            merge(chaini, nchain);
                            merge(chainj, nchain);
                            chains.push_back(nchain);
                            merged2Word[i] = chains.size() - 1;
                            merged2Word[j] = chains.size() - 1; //merged2Word[i];
                        }
                        break;
                    }
                }
                
                if((merged2Word[i] != -1) && (merged2Word[j] != -1)) break;
            }
        }
        
        if(merged2Word[i] == -1){ // chaini could not be chained to other
            chains.push_back(chaini);
            merged2Word[i] = chains.size() - 1;
        }
    }
    
    if(!merged) chains = initChains;
    delete [] merged2Word;
    
    return merged;
}

void mergePairs(const vector<Pair> groups, vector< vector<int> > &chains, bool* chainedComps, bool bVertical){
    vector< vector<int> > initChains;
    
    size_t nbGroup = groups.size();
    initChains.resize(nbGroup);
    for(size_t i = 0; i < nbGroup; i++){
        vector<int> tmp;
        Pair groupi = groups[i];
        if(!bVertical || !chainedComps[groupi.left]) tmp.push_back(groupi.left);
        if(!bVertical || !chainedComps[groupi.right]) tmp.push_back(groupi.right);
        initChains[i] = tmp;
    }
    
    while(mergePairs(initChains, chains)) initChains = chains;
}

void chains2Boxes(const vector< vector<int> > chains, vector<Rect> &boxes, bool* chainedComponents){
    for(size_t i = 0; i < chains.size(); i++){
        vector<int> chaini = chains[i];
        if(chaini.size() < CHAIN_SIZE) continue;
        int left = grayImg.cols, right = 0;
        int top = grayImg.rows, bottom = 0;
        
        for(size_t j = 0; j < chaini.size(); j++){
            Rect *roi = &componentsROI[chaini[j]];
            
            left = min(left, roi->x); right = max(right, roi->x + roi->width - 1);
            top = min(top, roi->y); bottom = max(bottom, roi->y + roi->height - 1);
            
            if(chainedComponents != NULL) chainedComponents[chaini[j]] = true;
        }
        
        boxes.push_back(Rect(left, top, right - left + 1, bottom - top + 1));
    }
}

void chainPairs(){
    // merge horizontal
    mergePairs(hCompGroups, horizontalChains);
    vector<Rect> initHorizontalBoxes;
    bool chainedComponents[nbComponent];
    memset(chainedComponents, false, nbComponent*sizeof(bool));
    chains2Boxes(horizontalChains, initHorizontalBoxes, chainedComponents);
    boundingBoxes.insert(boundingBoxes.end(), initHorizontalBoxes.begin(), initHorizontalBoxes.end());    
}

void removeOutlines(vector<Rect> &outlineBoxes){
    vector<Rect> tmpBoxes;
    float sumHeight = 0.0f;
    size_t nbBox = boundingBoxes.size();
    
    for(size_t i = 0; i < nbBox; i++) sumHeight += boundingBoxes[i].height;
    float meanHeight = sumHeight/nbBox;
    for(size_t i = 0; i < nbBox; i++){
        Rect box = boundingBoxes[i];
        if(boundingBoxes[i].height > 2*meanHeight) outlineBoxes.push_back(box);
        else tmpBoxes.push_back(box);
    }
    
    boundingBoxes = tmpBoxes;
}

void takeoutRegions(vector<Rect> &regionBoxes, vector<Rect> &nonRegionBoxes){
    for(size_t i = 0; i < boundingBoxes.size(); i++){
        Rect box = boundingBoxes[i];
        bool isInside = false;
        
        for(size_t j = 0; j < nbComponent; j++){
            Rect comp = componentsROI[j];
            if(isLetters[j] && (comp.x < box.x) && (comp.y < box.y) 
               && (comp.x + comp.width > box.x + box.width)
               && (comp.y + comp.height > box.y + box.height)
              ){
                    isInside = true;
                    regionBoxes.push_back(box);
                    break;
              }
        }
        
        if(!isInside) nonRegionBoxes.push_back(box);
    }
}

void takeoutNumbers(const Mat ccImg, const vector<Rect> nonRegionBoxes, vector<Rect> &numberBoxes, vector<Rect> &nonRegionNumberBoxes){
    size_t nbBox = nonRegionBoxes.size();
    double *cc = new double[nbBox];
    double sumCc = 0.0f;
    
    for(size_t i = 0; i < nbBox; i++){
        Rect box = nonRegionBoxes[i];
        int nbPixel = 0;
        for(int r = box.y; r < box.y + box.height; r++){
            for(int c = box.x; c < box.x + box.width; c++)
                if(ccImg.at<float>(r, c) > 0) nbPixel++;
        }
        
        cc[i] = (double)nbPixel/(box.width * box.height);
        sumCc += cc[i];
    }
    double meanCc = sumCc/nbBox;
    
    double *sw = new double[nbBox];
    double sumSw = 0.0f;
    
    for(size_t i = 0; i < nbBox; i++){
        Rect box = nonRegionBoxes[i];
        double sumTmp = 0.0f;
        int nbPixel = 0;
        for(int r = box.y; r < box.y + box.height; r++){
            for(int c = box.x; c < box.x + box.width; c++){
                int swVal = swtImg.at<float>(r, c);
                if(swVal > 0){
                    sumTmp += swVal;
                    nbPixel++;
                }
            }
        }
        
        sw[i] = sumTmp/nbPixel;
        sumSw += sw[i];
    }
    double meanSw = sumSw/nbBox;
    
    for(size_t i = 0; i < nbBox; i++){
        Rect box = nonRegionBoxes[i];
        if(cc[i] < 1.1*meanCc && sw[i] < 1.1*meanSw) nonRegionNumberBoxes.push_back(box);
        else numberBoxes.push_back(box);
    }
    
    delete [] cc;
    delete [] sw;
}

void takeoutPhonetics(const vector<Rect> nonRegionNumberBoxes, vector<Rect> &phoneticBoxes, vector<Rect> &sundryBoxes){
    size_t nbBox = nonRegionNumberBoxes.size();
    double *sw = new double[nbBox];
    double sumSw = 0.0f;
    
    for(size_t i = 0; i < nbBox; i++){
        Rect box = nonRegionNumberBoxes[i];
        double sumTmp = 0.0f;
        int nbPixel = 0;
        
        for(int r = box.y; r < box.y + box.height; r++){
            for(int c = box.x; c < box.x + box.width; c++){
                int swVal = swtImg.at<float>(r, c);
                if(swVal > 0){
                    sumTmp += swVal;
                    nbPixel++;
                }
            }
        }
        sw[i] = sumTmp/nbPixel;
        sumSw += sw[i];
    }
    double meanSw = sumSw/nbBox;
    
    for(size_t i = 0; i < nbBox; i++){
        Rect box = nonRegionNumberBoxes[i];
        if(sw[i] > MAX_BOLD_WIDTH * meanSw) sundryBoxes.push_back(box);
        else{
            box.y -= 11;
            box.height += 11;
            phoneticBoxes.push_back(box);
        }
    }
}

void test(){
    vector<int> token, chain;
    
    for(int i = 0; i < 51; i+=10) token.push_back(i);
    for(int i = 0; i < 26; i+=5) chain.push_back(i);
    
    cout << "token: ";
    for(size_t i = 0; i < token.size(); i++) cout << token[i] << " ";
    cout << endl;
    cout << "chain: ";
    for(size_t i = 0; i < chain.size(); i++) cout << chain[i] << " ";
    cout << endl;
    merge(token, chain);
    cout << "merged: ";
    for(size_t i = 0; i < chain.size(); i++) cout << chain[i] << " ";
    cout << endl;
}
