#include "proposition.h"
#include "functions.cpp"

int main(int argc, char* argv[]){
    
    setTimeCounter(t0);
//  /*  GET OPTIONS
    int c;
    char *fname = NULL;
    while( (c = getopt(argc, argv, "f:t:s:w:d:e:h")) != EOF ){
        extern char* optarg;
        switch (c){
            case 'f': // input image file
                fname = optarg;
                break;
            case 't': // print trace
                printTrace = atoi(optarg);
                break;
            case 's': // show images in each step
                showImages = atoi(optarg);
                break;
            case 'w': // write images in each step
                writeImages = atoi(optarg);
                break;
            case 'd': // debugging mode
                debug = atoi(optarg);
                break;
            case 'e': // testing mode
                testing = atoi(optarg);
                break;
            case 'h': // display help
                printHelp(argv[0]);
                return 0;
                break;
        }
    }
    extern int optind;
    fname = (fname == NULL) ? (char*)("exmap.tif") : fname;
//    GET OPTIONS */
    

    // 1. Read image & convert to gray scale image
    if(printTrace < 0 || printTrace == 1){
        cout << "1. Read image" << endl;
        cout << "--- input: fname" << "\n--- output: grayImg" << endl;
    }
    
    setTimeCounter(t1);
    orgImg = imread(fname, 1);
    cvtColor(orgImg, grayImg, CV_RGB2GRAY);
    
    if(grayImg.empty()){
        cout << "Could NOT read the image: " << fname << endl;
        return -1;
    }
    
    if(debug == 1 || debug < 0){
        printMatInfo(grayImg, "grayImg");
        printImage2File(grayImg, "gray.txt", 0);
        cout << "[debug] data of gray image is saved to file " << PREFIX_DATA << "gray.txt" << endl;
    }
    if(showImages == 1 || showImages < 0) showImage(grayImg, "Gray Image");
    if(writeImages == 1 || writeImages < 0){
        writeImage(grayImg, "gray.jpg");
        cout << "[writeImage] gray image is saved in file " << PREFIX_IMAGE << "gray.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 1){
        cout << "--- file name: " << fname << endl;
        cout << "--- image size: " << grayImg.cols << "x" << grayImg.rows << endl;
        printElapsedTime();
    }


    // 2. Convert image to binary using OTSU's algorithm
    if(printTrace < 0 || printTrace == 2){
        cout << "2. Convert image to BINARY using OTSU algorithm" << endl;
        cout << "--- input: grayImg" << "\n--- output: binImg" << endl;
    }
    
    setTimeCounter(t1);
    threshold(grayImg, binImg, 0, 255, THRESH_OTSU);
    bitwise_not(binImg, invBinImg);
    
    if(debug == 2 || debug < 0){
        cout << "[debug] MSE(grayImg, binImg) = " << getMSE(grayImg, binImg) << endl;
        printImage2File(binImg, "binary.txt");
        cout << "[debug] data of binary image is saved to file " << PREFIX_DATA << "binary.txt" << endl;
    }
    if(showImages == 2 || showImages < 0) showImage(binImg, "Binary Image");
    if(writeImages == 2 || writeImages < 0){
        writeImage(binImg, "binary.jpg");
        cout << "[writeImage] binary image is saved in file " << PREFIX_IMAGE << "binary.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 2) printElapsedTime();

    
    // 3. Remove noise using Gaussian blur
    if(printTrace < 0 || printTrace == 3){
        cout << "3. Remove NOISE using Gaussian blur" << endl;
        cout << "--- input: binImg" << "\n--- output: gausImg" << endl;
    }
    
    setTimeCounter(t1);
    GaussianBlur(binImg, gausImg, Size(5,5), 0, 0); // remove noise
    
    if(debug == 3 || debug < 0){
        printImage2File(gausImg, "gaussian.txt", 1);
        cout << "[debug] data of gaussian image is saved to file " << PREFIX_DATA << "gaussian.txt" << endl;
    }
    if(showImages == 3 || showImages < 0) showImage(gausImg, "Gaussian Image");
    if(writeImages == 3 || writeImages < 0){
        writeImage(gausImg, "gaussian.jpg");
        cout << "[writeImage] gaussian image is saved in file " << PREFIX_IMAGE << "gaussian.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 3) printElapsedTime();
    
    // 4. Compute EDGE image by using Canny
    if(printTrace < 0 || printTrace == 4){
        cout << "4. Compute EDGE by using Canny" << endl;
        cout << "--- input: gausImg" << "\n--- output: edgeImg" << endl;
    }

    setTimeCounter(t1);
    int lowThreshold; int highThreshold;
    estimateThreshold(gausImg, lowThreshold, highThreshold);
    Canny(gausImg, edgeImg, lowThreshold, highThreshold);
    
    if(debug == 4 || debug < 0) cout << "[debug] #NonZero pixel on edge image = " << countNonZero(edgeImg) << endl;
    if(showImages == 4 || showImages < 0) showImage(edgeImg, "Edge Image");
    if(writeImages == 4 || writeImages < 0){
        writeImage(edgeImg, "edge.jpg");
        cout << "[writeImage] edge image is saved in file " << PREFIX_IMAGE << "edge.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 4){
        cout << "--- low threshold = " << lowThreshold << "\n--- high threshold = " << highThreshold << endl;
        cout << "--- #edge points = " << countNonZero(edgeImg) << endl;
        printElapsedTime();
    }

    
    // 5. Compute GRADIENT by using Sobel
    if(printTrace < 0 || printTrace == 5){
        cout << "5. Compute GRADIENT by using Sobel" << endl;
        cout << "--- input: edgeImg" << "\n--- output: edgePoints, theta" << endl;
    }
    
    setTimeCounter(t1);
    Mat dx, dy;
    Sobel(gausImg, dx, CV_32FC1, 1, 0, 3);
    Sobel(gausImg, dy, CV_32FC1, 0, 1, 3);

    if(edgePoints.size()) edgePoints.clear();
    theta = Mat(grayImg.size(), CV_32FC1);
    for(int i = 0; i < grayImg.rows; i++){
        uchar* row_edge = edgeImg.ptr<uchar>(i);
        float* row_theta = theta.ptr<float>(i);
        float* row_dx = dx.ptr<float>(i);
        float* row_dy = dy.ptr<float>(i);
        
        for(int j = 0; j < grayImg.cols; j++){
            if((int)row_edge[j] == 255){ // white means the point is on edge
                row_theta[j] = atan2(row_dy[j], row_dx[j]); // gradient direction
                edgePoints.push_back(Point(j,i));
            }
        }
    }

    if(debug == 5 || debug < 0){
        printImage2File(theta, "theta.txt");
        cout << "[debug] values of theta are saved in the file " << PREFIX_DATA << "theta.txt" << endl;
        
        ofstream f;
        f.open("data_theta_negative.txt", ios::out);
        for(int i=0; i < grayImg.rows; i++){
            float* row = theta.ptr<float>(i);
            for(int j=0; j < grayImg.cols; j++)
                if(row[j] < 0) f << "(" << i << "," << j << "): " << row[j] << endl;
        }
        f.close();
        cout << "[debug] negative values of theta are saved in the file " << PREFIX_DATA << "theta_negative.txt" << endl;
    }
    if(showImages == 5 || showImages < 0){ showImage(dx, "dx"); showImage(dy, "dy"); }
    if(writeImages == 5 || writeImages < 0){
        writeImage(dx, "dx.jpg"); writeImage(dy, "dy.jpg");
        cout << "[writeImage] dx, dy images are saved in files " << PREFIX_IMAGE << "dx.jpg, and " << PREFIX_IMAGE << "dy.jpg" << endl;
    }
    dx.release(); dy.release();
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 5){
        cout << "--- #edge points = " << edgePoints.size() << endl;
        printElapsedTime();
    }


    // 6. Compute STROKE WIDTH
    if(printTrace < 0 || printTrace == 6){
        cout << "6. Compute STROKE WIDTH" << endl;
        cout << "--- input: edgeImg, edgePoints, theta" << "\n--- output: swtImg" << endl;
    }
    
    setTimeCounter(t1);
    float initSW = MAX_STROKE_WIDTH * 2;
    swtImg = Mat(grayImg.size(), CV_32FC1, Scalar(initSW));
    getStrokeImage(BLACK2WHITE);
    
    if(debug == 6 || debug < 0){
        printImage2File(swtImg, "swt.txt");
        cout << "[debug] data of stroke image is saved in the file " << PREFIX_DATA << "swt.txt" << endl;
        cout << "[debug] #nonZeroPixels of stroke image = " << countNonZero(swtImg) << endl;
    }
    if(showImages == 6 || showImages < 0) showImage(swtImg, "Stroke Image");
    if(writeImages == 6 || writeImages < 0){
        writeImage(swtImg*100, "stroke.jpg");
        cout << "[writeImage] stroke image is saved in file " << PREFIX_IMAGE << "stroke.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 6) printElapsedTime();


    // 7. Find connected components
    if(printTrace < 0 || printTrace == 7){
        cout << "7. Find COMPONENTS" << endl;
        cout << "--- input: swtImg" << "\n--- output: ccImg, componentsROI, nbComponent" << endl;
    }
    
    setTimeCounter(t1);
    int initCompVal = -1;
    ccImg = Mat(grayImg.size(), CV_32FC1, Scalar(initCompVal));
    componentsROI.clear();
    nbComponent = findComponents(); // version 4.0
    
    if(debug == 7 || debug < 0){
        Mat extractedComp(grayImg.size(), CV_32FC1);
        drawROIs(orgImg, componentsROI, extractedComp);
        writeImage(extractedComp, "debug_components.jpg");
        cout << "[debug] image of components extracting from original image is saved in file " << PREFIX_IMAGE << "debug_components.jpg" << endl;
    }
    if(showImages == 7 || showImages < 0){
        Mat drawnComp(grayImg.size(), CV_32FC1);
        drawROIs(ccImg, componentsROI, drawnComp);
        showImage(drawnComp*nbComponent, "Connected Components");
    }
    if(writeImages == 7 || writeImages < 0){
        Mat drawnComp(grayImg.size(), CV_32FC1);
        drawROIs(ccImg, componentsROI, drawnComp);
        writeImage(drawnComp*nbComponent*20, "components.jpg");
        cout << "[writeImage] image of components is saved in file " << PREFIX_IMAGE << "components.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 7){
        cout << "--- #Components = " << componentsROI.size() << endl;
        printElapsedTime();
    }


    // 8. Get Letter Candidates
    if(printTrace < 0 || printTrace == 8){
        cout << "8. Get Letter CANDIDATES" << endl;
        cout << "--- input: componentsROI, nbComponent" << "\n--- output: isLetters" << endl;
    }
    
    setTimeCounter(t1);
    getLetterCandidates(); // version 2.0
    vector<Rect> lettersROI;
    for(int i = 0; i < nbComponent; i++){
        if(isLetters[i]) lettersROI.push_back(componentsROI[i]);
    }
    
    if(debug == 8 || debug < 0){
        Mat letterCandidates(grayImg.size(), CV_32FC1);
        drawROIs(orgImg, lettersROI, letterCandidates);
        writeImage(letterCandidates, "debug_candidates.jpg");
        cout << "[debug] image of letter candidates is saved in file " << PREFIX_IMAGE << "debug_candidates.jpg" << endl;
    }
    if(showImages == 8 || showImages < 0){
        Mat letterCandidates(grayImg.size(), CV_32FC1);
        extractROIs(binImg, lettersROI, letterCandidates);
        showImage(letterCandidates, "Letter Candidates");
    }
    if(writeImages == 8 || writeImages < 0){
        Mat letterCandidates(grayImg.size(), CV_32FC1);
        extractROIs(binImg, lettersROI, letterCandidates);
        writeImage(letterCandidates, "candidates.jpg");
        cout << "[debug] image of letter candidates is saved in file " << PREFIX_IMAGE << "candidates.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 8){
        int nbLetterCandidate = 0;
        for(int i = 0; i < nbComponent; i++){
            if(isLetters[i]) nbLetterCandidate++;
        }
        cout << "--- #LetterCandidates = " << nbLetterCandidate << endl;
        printElapsedTime();
    }


    // 9. GROUP Letters
    if(printTrace < 0 || printTrace == 9){
        cout << "9. GROUP Letters" << endl;
        cout << "--- input: componentsROI, nbComponent, isLetters" << "\n--- output: hCompGroups, vCompGroups, nonGroupedComponents" << endl;
    }
    
    setTimeCounter(t1);
    groupLetters();
    
    if(debug == 9 || debug < 0){
        vector<Rect> groupedComponents;
        for(size_t i = 0; i < nbComponent; i++){
            if(isGrouped[i]) groupedComponents.push_back(componentsROI[i]);
        }
        Mat group(grayImg.size(), CV_32FC1);
        drawROIs(orgImg, groupedComponents, group);
        writeImage(group, "debug_group.jpg");
        cout << "[debug] image of grouped components is saved in file " << PREFIX_IMAGE << "debug_group.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 9){
        cout << "--- #hCompGroups = " << hCompGroups.size() << endl;
        cout << "--- #vCompGroups = " << vCompGroups.size() << endl;
        cout << "--- #nonGroupedComponents = " << nonGroupedComponents.size() << endl;
        printElapsedTime();
    }
    
    
    // 10. Chain Pairs
    if(printTrace < 0 || printTrace == 10){
        cout << "10. CHAIN Pairs" << endl;
        cout << "--- input: hCompGroups, nbComponent, componentsROI, grayImg" << "\n--- output: horizontalChains, boundingBoxes" << endl;
    }
    
    setTimeCounter(t1);
    chainPairs();
    
    if(debug == 10 || debug < 0){
        Mat boxes(grayImg.size(), CV_32FC1);
        drawROIs(orgImg, boundingBoxes, boxes);
        writeImage(boxes, "debug_boxes.jpg");
        cout << "[debug] image of bounding boxes extracted from inverted binary image is saved in file " << PREFIX_IMAGE << "debug_boxes.jpg" << endl;        
    }
    if(showImages == 10 || showImages < 0){
        Mat boxes(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, boundingBoxes, boxes);
        showImage(boxes, "Boxes");
    }
    if(writeImages == 10 || writeImages < 0){
        Mat boxes(grayImg.size(), CV_32FC1);
        extractROIs(invBinImg, boundingBoxes, boxes);
        writeImage(boxes, "boxes.jpg");
        cout << "[writeImages] image of bounding boxes extracted from inverted binary image is saved in file " << PREFIX_IMAGE << "boxes.jpg" << endl;
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 10){
        cout << "--- #horizontalChains = " << horizontalChains.size() << endl;
        cout << "--- #boundingBoxes = " << boundingBoxes.size() << endl;
        printElapsedTime();
    }

   
   // 11. Remove OUTLINES
   if(printTrace < 0 || printTrace == 11){
        cout << "11. Remove OUTLINES" << endl;
        cout << "--- input: boundingBoxes" << "\n--- output: boundingBoxes, outlineBoxes" << endl;
    }
    
    setTimeCounter(t1);
    vector<Rect> outlineBoxes;
    removeOutlines(outlineBoxes);
    
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 11){
        cout << "--- #boundingBoxes = " << boundingBoxes.size() << endl;
        cout << "--- #outlineBoxes = " << outlineBoxes.size() << endl;
        printElapsedTime();
    }
    
    
   // 12. Take out REGIONS
   if(printTrace < 0 || printTrace == 12){
        cout << "12. Take out REGIONS" << endl;
        cout << "--- input: boundingBoxes" << "\n--- output: regionBoxes, nonRegionBoxes" << endl;
    }
    
    setTimeCounter(t1);
    vector<Rect> regionBoxes, nonRegionBoxes;
    takeoutRegions(regionBoxes, nonRegionBoxes);
    
    Mat regions(grayImg.size(), CV_32FC1);
    extractROIs(invBinImg, regionBoxes, regions);
    writeImage(regions, "regions.jpg");
    cout << "[result] image of regions is saved in file " << PREFIX_IMAGE << "regions.jpg" << endl;

    if(showImages == 12 || showImages < 0){
        showImage(regions, "Regions", 0);
    }
    if(debug == 12 || debug < 0){
        drawROIs(orgImg, regionBoxes, regions);
        writeImage(regions, "debug_regions.jpg");
        cout << "[debug] image of drawn regions is saved in file " << PREFIX_IMAGE << "debug_regions.jpg" << endl; 
    }
    getElapsedTime(t1);    
    if(printTrace < 0 || printTrace == 12){
        cout << "--- #regionBoxes = " << regionBoxes.size() << endl;
        cout << "--- #nonRegionBoxes = " << nonRegionBoxes.size() << endl;
        printElapsedTime();
    }
    
    
   // 13. Take out NUMBERS
   if(printTrace < 0 || printTrace == 13){
        cout << "13. Take out NUMBERS" << endl;
        cout << "--- input: nonRegionBoxes, ccImg, swtImg" << "\n--- output: numberBoxes, nonRegionNumberBoxes" << endl;
    }
    
    setTimeCounter(t1);
    vector<Rect> numberBoxes, nonRegionNumberBoxes;
    takeoutNumbers(ccImg, nonRegionBoxes, numberBoxes, nonRegionNumberBoxes);
    
    Mat numbers(grayImg.size(), CV_32FC1);
    extractROIs(invBinImg, numberBoxes, numbers);
    writeImage(numbers, "numbers.jpg");
    cout << "[result] image of numbers is saved in file " << PREFIX_IMAGE << "numbers.jpg" << endl;
    
    if(showImages == 13 || showImages < 0){
        showImage(regions, "Numbers", 0);
    }
    if(debug == 13 || debug < 0){
        drawROIs(orgImg, numberBoxes, numbers);
        writeImage(numbers, "debug_numbers.jpg");
        cout << "[debug] image of drawn numbers is saved in file " << PREFIX_IMAGE << "debug_numbers.jpg" << endl; 
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 13){
        cout << "--- #numberBoxes = " << numberBoxes.size() << endl;
        cout << "--- #nonRegionNumberBoxes = " << nonRegionNumberBoxes.size() << endl;
        printElapsedTime();
    }
    
    
    // 14. Take out PHONETICS
   if(printTrace < 0 || printTrace == 14){
        cout << "14. Take out PHONETICS" << endl;
        cout << "--- input: nonRegionNumberBoxes, swtImg" << "\n--- output: phoneticsBoxes, sundryBoxes" << endl;
    }
    
    setTimeCounter(t1);
    vector<Rect> phoneticBoxes, sundryBoxes;
    takeoutPhonetics(nonRegionNumberBoxes, phoneticBoxes, sundryBoxes);
    
    Mat phonetics(grayImg.size(), CV_32FC1);
    extractROIs(invBinImg, phoneticBoxes, phonetics);
    writeImage(phonetics, "phonetics.jpg");
    cout << "[result] image of phonetics is saved in file " << PREFIX_IMAGE << "phonetics.jpg" << endl;
    
    if(showImages == 14 || showImages < 0){
        showImage(phonetics, "Phonetics", 0);
    }
    if(debug == 14 || debug < 0){
        drawROIs(orgImg, phoneticBoxes, phonetics);
        writeImage(phonetics, "debug_phonetics.jpg");
        cout << "[debug] image of drawn phonetics is saved in file " << PREFIX_IMAGE << "debug_phonetics.jpg" << endl; 
    }
    getElapsedTime(t1);
    if(printTrace < 0 || printTrace == 14){
        cout << "--- #phoneticsBoxes = " << phoneticBoxes.size() << endl;
        cout << "--- #sundryBoxes = " << sundryBoxes.size() << endl;
        printElapsedTime();
    }
    

    getElapsedTime(t0);
    cout << endl << "total "; printElapsedTime(0);
    
    
    waitKey();
    return 0;
}
