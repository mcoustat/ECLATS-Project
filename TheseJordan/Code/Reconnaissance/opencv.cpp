#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/features2d.hpp>
#include <iostream>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <sstream>

using namespace cv;
using namespace std;

//g++ -std=c++11 opencv.cpp -o opencvTest `pkg-config --cflags --libs opencv`

vector<Point> contoursConvexHull(vector<vector<Point>> contours)
{
    vector<Point> result;
    vector<Point> pts;
    for (size_t i = 0; i< contours.size(); i++)
        for (size_t j = 0; j< contours[i].size(); j++)
            pts.push_back(contours[i][j]);
    convexHull(pts, result);
    return result;
}


int main(int argc, char** argv) {
  
  Mat image;
  Mat imageCrop;
  Mat imageGrey;
  Mat imageRes;
  Mat imageBin;
  Mat canny_output;
  int thresh = 127;
  int max_thresh = 255;
  int blockSize = 0;
  vector<vector<Point>> contours;
  vector<Vec4i> hierarchy;
  double hu[7];

  srand((unsigned) time(0));

   /// --- READING SRC IMAGE ---
  image = imread(argv[1] , IMREAD_COLOR);
  if(!image.data){
    cout <<  "Could not open or find the image" << endl ;
    return -1;
  }
 
  Vec3b alpha = image.at<Vec3b>(0,0);			// IMAGE PNG Vec4b OU TIF Vec3b ???
 
  if ( alpha.val[0] == 0 && alpha.val[1] == 0 && alpha.val[2] == 0 ){
    Rect roi;
    roi.x = 1;
    roi.y = 1;
    roi.width = image.cols - 2;
    roi.height = image.rows - 2;
    imageCrop = image(roi);	// Crop the original image to the defined ROI
  } else {
    imageCrop = image.clone();
  }
 
  /// --- BINARIZATION ---
 
  //threshold(image, imageBin, thresh, max_thresh, 0);
  // imageGrey = imread(argv[1] , 0);
  cvtColor(imageCrop, imageGrey, COLOR_BGR2GRAY);
  Size size(64,64);
  if(imageGrey.rows <= 30 || imageGrey.cols <= 30 ){
    resize(imageGrey, imageRes, size);
  } else {
    imageRes = imageGrey.clone();
  }
  if(imageRes.rows & 1){
    blockSize = (int)(imageRes.rows);
  } else {
    blockSize = (int)(imageRes.rows - 1);
  }
  adaptiveThreshold(imageRes, imageBin, max_thresh, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, blockSize, (int)(blockSize/8));
  //adaptiveThreshold(imageRes, imageBin, max_thresh, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, blockSize, (int)(blockSize/4));
  imwrite("opencvBin.png",imageBin);



  /// --- MASK ---
  // Problème des deux formats, il faudrait convertir tous les Vec3b en Vec4b en rajoutant a la fin une valeur de transparente opaque (alpha.val[3] = 255)
  int colorPBin; // Couleur pixel image binarisé
  Vec3b colorP;  // Couleur pixel image entrée		// IMAGE PNG Vec4b OU TIF Vec3b ???
  Vec3b colorLetter;					// (lié)
  Vec3b colorBackground;				// (lié)
  vector<int> moyColorLetterB{};
  vector<int> moyColorLetterG{};
  vector<int> moyColorLetterR{};
  vector<int> moyColorBackgroundB{};
  vector<int> moyColorBackgroundG{};
  vector<int> moyColorBackgroundR{};

  Mat imageBinRes = imageBin.clone();
  Size sizeImg(image.cols, image.rows);
  if(imageBinRes.rows != image.rows){
    resize(imageBin, imageBinRes, sizeImg);
    imwrite("opencvimageBinRes.png",imageBinRes);
  }

  for(int x=0; x<imageBinRes.rows; x++){
    for(int y=0; y<imageBinRes.cols; y++){
      colorP = image.at<Vec3b>(x,y);			// IMAGE PNG Vec4b OU TIF Vec3b ???
      colorPBin = imageBinRes.at<unsigned char>(x,y);
      if(colorPBin == 0){ // Si pixel noir
        moyColorLetterB.push_back(colorP.val[0]);
        moyColorLetterG.push_back(colorP.val[1]);
        moyColorLetterR.push_back(colorP.val[2]);
      } else { // Si pixel blanc
        moyColorBackgroundB.push_back(colorP.val[0]);
        moyColorBackgroundG.push_back(colorP.val[1]);
        moyColorBackgroundR.push_back(colorP.val[2]);
      }
    }
  }
  colorLetter[0] = (int)((accumulate(begin(moyColorLetterB), end(moyColorLetterB), 0))/moyColorLetterB.size());
  colorLetter[1] = (int)((accumulate(begin(moyColorLetterG), end(moyColorLetterG), 0))/moyColorLetterG.size());
  colorLetter[2] = (int)((accumulate(begin(moyColorLetterR), end(moyColorLetterR), 0))/moyColorLetterR.size());
  colorBackground[0] = (int)((accumulate(begin(moyColorBackgroundB), end(moyColorBackgroundB), 0))/moyColorBackgroundB.size());
  colorBackground[1] = (int)((accumulate(begin(moyColorBackgroundG), end(moyColorBackgroundG), 0))/moyColorBackgroundG.size());;
  colorBackground[2] = (int)((accumulate(begin(moyColorBackgroundR), end(moyColorBackgroundR), 0))/moyColorBackgroundR.size());;

  cout << "Color Letter : " << (int)colorLetter.val[0] << " " << (int)colorLetter.val[1] << " " <<(int)colorLetter.val[2] << endl;			// (lié)
  cout << "Color Background : " << (int)colorBackground.val[0] << " " << (int)colorBackground.val[1] << " " <<(int)colorBackground.val[2] << endl;		// (lié)
  

  /// DEBUG
  Mat imageColorMoy(imageBinRes.size(), CV_8UC3);
  for(int x=0; x<imageBinRes.rows; x++){
    for(int y=0; y<imageBinRes.cols; y++){
      colorPBin = imageBinRes.at<unsigned char>(x,y);
      if(colorPBin == 0){
        imageColorMoy.at<Vec3b>(x,y) = colorLetter;	// IMAGE PNG Vec4b OU TIF Vec3b ???
      } else {
        imageColorMoy.at<Vec3b>(x,y) = colorBackground;	// IMAGE PNG Vec4b OU TIF Vec3b ???
      }
    }
  }
  imwrite("opencvColorMoy.png",imageColorMoy);



  /// --- FILTERS ---

  Canny(imageBinRes, canny_output, thresh, thresh*2, 3);
  findContours(canny_output, contours, hierarchy, RETR_EXTERNAL, CHAIN_APPROX_NONE); // detection de contour extérieurs
  //findContours(canny_output, contours, hierarchy, RETR_LIST, CHAIN_APPROX_NONE);
  // RETR_LIST, RETR_TREE, RETR_CCOMP, RETR_EXTERNAL

 
/*
  /// --- DIMENSIONS ---

  for(int i=0; i<contours.size(); i++){
    Rect box = boundingRect(contours[i]);
    cout << "Hauteur:\t" << box.height << endl;				// hauteur
    cout << "Largeur:\t" << box.width << endl;				// largeur
    double excentricite = (double)box.height/(double)box.width;
    cout << "Excentricite:\t" << excentricite << endl;			// excentricité
    cout << "Aire:\t\t" << contourArea(contours[i],false) << endl;	// aire
    cout << "Perimetre:\t" << arcLength(contours[i],true) << endl;	// perimetre
    double circularite = (double)(4*CV_PI*contourArea(contours[i],false))/(double)(arcLength(contours[i],true)*arcLength(contours[i],true));
    cout << "Circularite:\t" << circularite << endl;			// circularité
    double compacite = (double)(sqrt((4.0/CV_PI)*contourArea(contours[i],false)))/(double)(box.height);
    cout << "Compacite:\t" << compacite << endl;			// compacité
    HuMoments(moments(contours[i]), hu);
    for(int i=0; i<(sizeof(hu)/sizeof(*hu)); i++)
      cout << "Hu" << i << ":\t\t" << hu[i] << endl; 			// moment de hu
    cout << endl;
  }
*/
/*
  // perimetre enveloppe convexe
  vector<vector<Point> > hullh(contours.size());
  for (int i = 0; i < contours.size(); i++){
    convexHull(contours[i], hullh[i], false);
  }
  for(int i=0;i<hullh.size();i++){
    cout << arcLength(hullh[i],true) << endl << endl;
  }
*/
  cout << "Contours count : " << contours.size() << endl;
  


  /// --- DISPLAY ---

  RNG rng(12345);
/*
  vector<vector<Point>> contours_poly(contours.size());
  vector<Rect> boundRect(contours.size());
  vector<Point2f> centers(contours.size());
  vector<float> radius(contours.size());
  for(size_t i=0; i<contours.size(); i++){
      approxPolyDP(contours[i], contours_poly[i], 3, true);
      boundRect[i] = boundingRect(contours_poly[i]);
      minEnclosingCircle(contours_poly[i], centers[i], radius[i]);
  }
*/
  Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
  for(size_t i=0; i<contours.size(); i++){
      Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256));
      //drawContours(drawing, contours_poly, (int)i, color);
      drawContours(drawing, contours, (int)i, color);
      //rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 1);
      //circle(drawing, centers[i], (int)radius[i], color, 1);
  }
  imwrite("opencvContours.png",drawing);

  

/*
  
  /// --- CONVEX HULL (Enclosing polygon) ---
  
  Mat drawing = Mat::zeros(canny_output.size(), CV_8UC3);
  vector<vector<Point>> hull(contours.size());
  for(int i = 0; i < contours.size(); i++){
    convexHull(Mat(contours[i]), hull[i], false);
  }
  for(int i = 0; i < contours.size(); i++){
    Scalar color_contours = Scalar(255, 255, 255); // green - color for contours
    Scalar color = Scalar(0, 0, 255); // blue - color for convex hull
    drawContours(drawing, contours, i, color_contours, 1, 8, vector<Vec4i>(), 0, Point()); // draw contour
    drawContours(drawing, hull, i, color, 1, 8, vector<Vec4i>(), 0, Point()); // draw convex hull
  }
  imwrite("opencv.png",drawing);
*/



  /// --- HISTOGRAM ---

  vector<Mat> bgr_planes;
  split(image, bgr_planes);	// Separate the image in 3 places ( B, G and R )
  int histSize = 256;	// Establish the number of bins
  float range[] = {0, 256};
  const float* histRange = {range};	// Set the ranges ( for B,G,R) )
  bool uniform = true;
  bool accumulate = false;
  Mat b_hist, g_hist, r_hist;

  // Compute the histograms:
  calcHist(&bgr_planes[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate);
  calcHist(&bgr_planes[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniform, accumulate);
  calcHist(&bgr_planes[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniform, accumulate);

  // Draw the histograms for B, G and R
  int hist_w = 512;
  int hist_h = 400;
  int bin_w = cvRound((double)hist_w/histSize);
  Mat histImage(hist_h, hist_w, CV_8UC3, Scalar(0,0,0));

  // Normalize the result to [ 0, histImage.rows ]
  normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
  normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());
  normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat());

  // Draw for each channel
  for(int i=1; i<histSize; i++){
      line(histImage, Point(bin_w*(i-1), hist_h-cvRound(b_hist.at<float>(i-1))), Point(bin_w*(i), hist_h-cvRound(b_hist.at<float>(i))), Scalar(255, 0, 0), 2, 8, 0);
      line(histImage, Point(bin_w*(i-1), hist_h-cvRound(g_hist.at<float>(i-1))), Point( bin_w*(i), hist_h-cvRound(g_hist.at<float>(i))), Scalar(0, 255, 0), 2, 8, 0);
      line(histImage, Point(bin_w*(i-1), hist_h-cvRound(r_hist.at<float>(i-1))), Point( bin_w*(i), hist_h-cvRound(r_hist.at<float>(i))), Scalar(0, 0, 255), 2, 8, 0);
  }
  imwrite("opencvHist.png", histImage);

/*
  /// --- BLOB DETECTION ---

  //threshold(image, imageBin, thresh, max_thresh, 0);
  
  SimpleBlobDetector::Params params;
  //params.blobColor = 0;
  //params.minThreshold = 40;
  //params.maxThreshold = 200;
  params.thresholdStep = 1;
  params.filterByColor = false;
  params.filterByArea = false;
  params.filterByCircularity = false;
  params.filterByConvexity = false;
  params.filterByInertia = false;
  params.minDistBetweenBlobs = 1;
  
  Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
  std::vector<KeyPoint> keypoints;
  detector->detect(image, keypoints);
  //detector->detect(imageBin, keypoints);

  cout << "Blob count : " << keypoints.size() << endl;
 
  // Draw detected blobs as red circles.
  // DrawMatchesFlags::DRAW_RICH_KEYPOINTS flag ensures the size of the circle corresponds to the size of blob
  Mat im_with_keypoints;
  drawKeypoints(image, keypoints, im_with_keypoints, Scalar(0,0,255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
 
  // Show blobs
  imwrite("opencvBlob.png", im_with_keypoints);
*/ 



  /// --- NOISE N°1 - INVERTED PIXELS ---

  Mat noise1;
  resize(imageCrop, noise1, sizeImg);
  int nbNoisePixels = 0;
  int randomIndex = 0;
  int randomVoisin = 0;
  vector<Point> listPointsContours;
  vector<Point> listPointsNoise;
  Point noise;
  double noiseRatio = 0.25;	// 25% de bruit sur les contours de l'image

  cout << "Dim image : " << noise1.cols  << " x " << noise1.rows << endl;

  // Etablir le nombre de pixels a changer
  //int nbPixelsImage = image.total();			// Si on passe sur un ratio image
  int nbPixelsContours = 0;
  for(size_t i=0; i<contours.size(); i++){
    nbPixelsContours = nbPixelsContours + contours[i].size();
  }
  //nbNoisePixels = (int)(nbPixelsImage*noiseRatio);	// Si on passe sur un ratio image
  nbNoisePixels = (int)(nbPixelsContours*noiseRatio);
  cout << "nbPixelsContours : " << nbPixelsContours << endl;
  cout << "nbNoisePixels : " << nbNoisePixels << endl;
  cout << "noiseRatio : " << noiseRatio << endl;

  // Selectionner X pixels aléatoires des contours
  for(size_t i=0; i<contours.size(); i++){
    for(size_t j=0; j<contours[i].size(); j++){
      listPointsContours.push_back(contours[i][j]);	// Dupliquer la liste des contours (et changement de type vector<vector<Point>> pour vector<Point>)
    }
  }
  for(size_t i=0; i<nbNoisePixels; i++){
    randomIndex = rand() % listPointsContours.size();	// Selectionner un point random
    listPointsNoise.push_back(listPointsContours[randomIndex]);	// Deplacer dans la nouvelle liste le point en question
    listPointsContours.erase(listPointsContours.begin() + randomIndex);
  }
  
  // Creer liste positions voisins [-1;-1],[0;-1],[1;-1],[-1;0],[0;0],[1;0],[-1;1],[0;1],[1;1]  
  vector<Point> voisins;
  voisins.push_back(Point(-1,-1));
  voisins.push_back(Point(0,-1));
  voisins.push_back(Point(1,-1));
  voisins.push_back(Point(-1,0));
  voisins.push_back(Point(0,0));
  voisins.push_back(Point(1,0));
  voisins.push_back(Point(-1,1));
  voisins.push_back(Point(0,1));
  voisins.push_back(Point(1,1));

  // Pour chaque pixel de la liste de pixels selectionnés
  for(size_t i=0; i<listPointsNoise.size(); i++){
    // Prendre un voisin proche 
    randomVoisin = rand() % voisins.size();	// Randomiser le voisin parmis la liste
    noise = listPointsNoise[i] + voisins[randomVoisin];	// Modifier le point en fonction du voisin
    // Normalisation pour na pas dépasser de l'image 
    if(noise.x<0){ noise.x=0; }else if(noise.x>image.cols-1){ noise.x=image.cols-1; }
    if(noise.y<0){ noise.y=0; }else if(noise.y>image.rows-1){ noise.y=image.rows-1; }
    // Regarder sa couleur
    Vec3b colorPixel = image.at<Vec3b>(noise);
    double distLetter = norm(colorPixel, colorLetter, NORM_L2);
    double distBackground = norm(colorPixel, colorBackground, NORM_L2);
    // Changer la couleur pour son inverse
    if(distBackground>distLetter){ 	// Si c'est un pixel de Lettre
      noise1.at<Vec3b>(noise) = colorBackground;	// On lui donne la couleur du Background (car bruit)
    } else {				// Si c'est un pixel de Background
      noise1.at<Vec3b>(noise) = colorLetter;	// On lui donne la couleur du Background (car bruit)
    }
  }
  imwrite("opencvNoise1.png",noise1);




  /// --- NOISE N°2 - BLURRED IMAGE ---

  Mat noise2 = imageCrop.clone();  
  int DELAY_CAPTION = 1500;
  int DELAY_BLUR = 100;
  int MAX_KERNEL_LENGTH = 31;
  string prefix;
  string filename;

  

  prefix = "opencvNoise2/Homogeneous";
  /// Homogeneous blur
  for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
    blur( imageCrop, noise2, Size( i, i ), Point(-1,-1) );
    filename = prefix + to_string(i) + ".png";
    imwrite(filename,noise2);
  }
  
  prefix = "opencvNoise2/Gaussian";
  /// Gaussian blur
  for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
    GaussianBlur( imageCrop, noise2, Size( i, i ), 0, 0 );
    filename = prefix + to_string(i) + ".png";
    imwrite(filename,noise2);
  }

  prefix = "opencvNoise2/Median";
  /// Median blur
  for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
    medianBlur ( imageCrop, noise2, i );
    filename = prefix + to_string(i) + ".png";
    imwrite(filename,noise2);
  }

  prefix = "opencvNoise2/Bilateral";
  /// Bilateral Filter
  for ( int i = 1; i < MAX_KERNEL_LENGTH; i = i + 2 ){
    bilateralFilter ( imageCrop, noise2, i, i*2, i/2 );
    filename = prefix + to_string(i) + ".png";
    imwrite(filename,noise2);
  }


  return 0;
 }
