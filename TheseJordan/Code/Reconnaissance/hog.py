from sklearn.neighbors import KNeighborsClassifier
from skimage import feature
import cv2
import numpy as np


# data = "Dataset_CharRousselotGillerion/"
data = "Dataset_CharSymbola/"

modPath = data + "/Imagettes_mod/"

for i in range(3):
    print("{} test/train".format(i))
    csv_path = data+"train-{}.txt".format(i)
    csv = np.loadtxt(csv_path, delimiter=',', dtype=str)

    datas = []
    labels = []

    for l in csv:
        inFile = modPath+(l[0]).split('/')[-1].split('.')[0]+".png"

        logo = cv2.imread(inFile)

        # extract Histogram of Oriented Gradients from the logo
        H = feature.hog(logo, orientations=9, pixels_per_cell=(10, 10),
        cells_per_block=(2, 2), transform_sqrt=True, block_norm="L1")

        # update the data and labels
        datas.append(H)
        labels.append(l[1])

    # print("[INFO] training classifier...")
    model = KNeighborsClassifier(n_neighbors=1)
    model.fit(datas, labels)
    # print("[INFO] evaluating...")

    csv_pathTest = data+"test-{}.txt".format(i)
    csvTest = np.loadtxt(csv_pathTest, delimiter=',', dtype=str)
    ok = 0
    ko = 0
    for l in csvTest:
        inFile = modPath+(l[0]).split('/')[-1].split('.')[0]+".png"
        logo = cv2.imread(inFile)

        # extract Histogram of Oriented Gradients from the logo
        H = feature.hog(logo, orientations=9, pixels_per_cell=(10, 10),
        cells_per_block=(2, 2), transform_sqrt=True, block_norm="L1")
        pred = model.predict(H.reshape(1, -1))[0]
        if l[1] == pred.title().lower():
            ok += 1
        else:
            print(l[0], pred.title())
            ko += 1
        # print(l[1], pred.title().lower())
    print("ok {}, ko {} {}".format(ok, ko, ok/(ok+ko)))
    # exit()