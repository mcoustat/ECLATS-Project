from tools import *

# data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/"
data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharSymbola/"


outBins = data+"SFC/bins.txt"
csv_path = data+"CorrespondancesModif.csv"
sfc_path = data+"SFC/"

testFiles = data+"test-"
trainFiles = data+"train-"

classes = buildClass(csv_path)

sum = 0
for k in classes.keys():
        cl = [k for _ in range(len(classes[k]))]
        te = classes[k]
        traintest = kfoldMe(te, cl)

        cpt=0
        for e in traintest:
            fout = open(trainFiles+"{}.txt".format(cpt), "a")
            for i in e[0]:
                print(te[i])
                fout.write("{},{}\n".format(te[i].split('.')[0]+".txt", k))
            fout.close()

            fout = open(testFiles+"{}.txt".format(cpt), "a")

            for i in e[1]:
                print(te[i])
                fout.write("{},{}\n".format(te[i].split('.')[0]+".txt", k))
            fout.close()

            cpt += 1

        sum += len(classes[k])
        print(k, len(classes[k]))

# print(sum)
# print(sum)



# a1, a2 = classToArray(classes)
#
# # print(a1)
# # print(a2)
#
# traintest = kfoldMe(a1, a2)
# csv = np.loadtxt(csv_path, delimiter=';', dtype=str)
#
# cpt=0
# for e in traintest:
#     # print(len(e))
#     fout = open(trainFiles+"{}.txt".format(cpt), "w")
#
#     for i in e[0]:
#         # print(csv[i])
#         fout.write("{},{}\n".format(csv[i][0].split('.')[0]+".txt", csv[i][1], csv[i][2]))
#     fout.close()
#
#     fout = open(testFiles+"{}.txt".format(cpt), "w")
#
#     for i in e[1]:
#         # print(csv[i])
#         fout.write("{},{}\n".format(csv[i][0].split('.')[0]+".txt", csv[i][1], csv[i][2]))
#     fout.close()
#
#     cpt += 1
