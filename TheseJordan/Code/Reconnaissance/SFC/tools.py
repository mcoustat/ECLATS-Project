import sys
import numpy as np
from sklearn.model_selection import KFold

LENWEI = (50,50)

def printImage(img):
    x, y = img.shape
    for i in range(x):
        for j in range(y):
            if img[i][j] == 255:
                print("X", end=" ")
            else:
                print("0", end=" ")
        print("")

def prettyPrint(mat, f=sys.stdout):
    x, y = mat.shape

    for i in range(x):
        for j in range(y):
            newi = int(round((255*i)/LENWEI[0]))
            newj = int(round((255*j)/LENWEI[1]))
            h = int(round(255*mat[i][j][0]/179))
            # f.write("{},{},{},{},{}\n".format(newi, newj, h, mat[i][j][1], mat[i][j][2]))
            f.write("{},{},{},{},{}\n".format(i, j, h, mat[i][j][1], mat[i][j][2]))

def distance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

def growingRegion(img, blackPix):
    # print(len(blackPix))
    height, width = img.shape
    res = []
    # neighbors = [(-1, 0), (1, 0), (0, -1), (0, 1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
    neighbors = [(-1, 0), (1, 0), (0, -1), (0, 1)]


    while not not blackPix:
        current = blackPix.pop()
        toDo = [current]
        done = []

        while not not toDo:
            current = toDo.pop()
            # for i in range(8):
            for i in range(4):
                #Compute the neighbor pixel position
                x_new = current[0] + neighbors[i][0]
                y_new = current[1] + neighbors[i][1]
                #Boundary Condition - check if the coordinates are inside the image
                check_inside = (x_new >= 0) & (y_new >= 0) & (x_new < height) & (y_new < width)
                if check_inside:
                    if img[x_new][y_new] != 255 and (x_new, y_new) not in done and (x_new, y_new) not in toDo:
                        # print(x_new, y_new, img[x_new][y_new])
                        blackPix.remove((x_new, y_new))
                        toDo.append((x_new, y_new))
            done.append(current)

        res.append(done)
    return res

def resizeMe(img):
    up = -1
    left = -1
    right = -1
    down = -1
    x, y = img.shape
    # up
    for i in range(x):
        for j in range(y):
            if img[i][j] != 255:
                up = i
                break
        if up != -1:
            break
    #left
    for i in range(y):
        for j in range(x):
            if img[j][i] != 255:
                left = i
                break
        if left != -1:
            break

    # down
    for i in range(x-1, 0, -1):
        for j in range(y):
            if img[i][j] != 255:
                down = i
                break
        if down != -1:
            break

    # right
    for i in range(y-1, 0, -1):
        for j in range(x):
            if img[j][i] != 255:
                right = i
                break
        if right != -1:
            break
    return (img[up : down+1, left : right+1], up, left)
    # return img

def buildClass(fin):
    res = {}
    resFinal = {}

    csv = np.loadtxt(fin, delimiter=';', dtype=str)

    for l in csv:
        try:
            res[l[1]].append(l[0])
        except KeyError:
            res[l[1]] = [l[0]]

    for k in res.keys():
        if len(res[k]) > 3:
            resFinal[k] = res[k]

    return resFinal

def classToArray(classes):
    resName = []
    resClass = []
    for k in classes.keys():
        for e in classes[k]:
            resName.append(e)
            resClass.append(k)
    return (resName, resClass)

def kfoldMe(arrayName, arrayC):
    res = []
    kf = KFold(n_splits=4)
    kf.get_n_splits(arrayName)
    for train_index, test_index in kf.split(arrayName):
        res.append([train_index, test_index])

    return res
