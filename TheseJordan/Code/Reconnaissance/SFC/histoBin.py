import cv2
import os
import math
import numpy as np
from tools import *

def sumBetween(count, bin):
    res = 0

    for i in range(bin[0], bin[1]):
        # if count[i] > 0:
            # res += 1
            res += count[i]

    return res


data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/"
# data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharSymbola/"


# csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif.csv"

csv_path = data+"train-0.txt"
# csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif.csv"


to_sfc_path = data+"Imagette_SFC/"
# csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif-test.csv"

sfc_path = data+"SFC/"


# outBins = "Dataset_CharRousselotGillerion/SFC/bins-0.txt"

file = 0
c = 0

d=3
n=8

for i in range(4):
    csv_path = data+"train-{}.txt".format(i)
    outBins = data+"SFC/bins-{}.txt".format(i)
    # csv_path = data+"train-all.txt"
    # outBins = data+"SFC/bins-all.txt"

    csv = np.loadtxt(csv_path, delimiter=',', dtype=str)
    # print(csv)

    beg = 0
    end = pow(2,d*n)

    limitCpt = end / 10000
    # limitCpt = end / 100000


    bins = [[beg, end]]
    begins = [0]

    count = [0 for _ in range(end)]

    for l in csv:
        inFile = sfc_path+(l[file]).split('/')[-1].split('.')[0]+".txt"
        # print(l[file], inFile)
        inFileValue = np.loadtxt(inFile, dtype=int)

        for i in range(len(inFileValue)):
            count[inFileValue[i]] += 1
            # count[inFileValue[i]] = 1


    # cmean = np.mean(count) * 2
    # for i in range(len(count)):
    #     if count[i] > cmean:
    #         count[i] = 0
    # print(count)
    # SFCValue = sorted(count.keys())

    res = []

    while bins:
        b = bins.pop()
        mid = b[0] + int((b[1] - b[0]) / 2)
        binA = [b[0], mid]
        binB = [mid+1, b[1]]

        sumA = sumBetween(count, binA)
        sumB = sumBetween(count, binB)

        if sumA > limitCpt:
            bins.append(binA)
        else:
            res.append(binA)


        if sumB > limitCpt:
            bins.append(binB)
        else:
            res.append(binB)
    res = sorted(res)

    with open(outBins, "w") as fout:
        for r in res:
            fout.write("{}, {}\n".format(r[0], r[1]))

    exit()
