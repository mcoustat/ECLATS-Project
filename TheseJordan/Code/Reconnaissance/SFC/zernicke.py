from sklearn.neighbors import KNeighborsClassifier
import mahotas as mh
import cv2
import numpy as np


# data = "Dataset_CharRousselotGillerion/"
data = "Dataset_CharSymbola/"

modPath = data + "/Imagettes_mod/"

for i in range(3):
    print("{} test/train".format(i))
    csv_path = data+"train-{}.txt".format(i)
    csv = np.loadtxt(csv_path, delimiter=',', dtype=str)

    datas = []
    labels = []

    for l in csv:
        inFile = modPath+(l[0]).split('/')[-1].split('.')[0]+".png"

        # logo = cv2.imread(inFile)
        logo = mh.imread(inFile)

        Z = mh.features.zernike_moments(logo, 32)
        # print(Z)
        # update the data and labels
        datas.append(Z)
        labels.append(l[1])
    # exit()
    # print("[INFO] training classifier...")
    model = KNeighborsClassifier(n_neighbors=1)
    model.fit(datas, labels)
    # print("[INFO] evaluating...")

    csv_pathTest = data+"test-{}.txt".format(i)
    csvTest = np.loadtxt(csv_pathTest, delimiter=',', dtype=str)
    ok = 0
    ko = 0
    for l in csvTest:
        inFile = modPath+(l[0]).split('/')[-1].split('.')[0]+".png"

        logo = mh.imread(inFile)

        Z = mh.features.zernike_moments(logo, 32)

        pred = model.predict(Z.reshape(1, -1))[0]
        if l[1] == pred.title().lower():
            ok += 1
        else:
            # print(l[0], pred.title())
            ko += 1
        print(l[1], pred.title().lower(), l[0])
    print("ok {}, ko {} {}".format(ok, ko, ok/(ok+ko)*100))
    # exit()
