import cv2
import math
import numpy as np
from tools import *
import os

def binJordan(img):
    bin = "build/main"
    print(img)
    pid2 = os.fork()
    if pid2 == 0:
        os.execlp(bin, bin, img)

    st = os.waitpid(pid2, 0)

    res = cv2.imread("/tmp/ocrTemp.png", cv2.IMREAD_GRAYSCALE)
    return res


data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/"
# data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharSymbola/"

img_path = data+"Imagettes/"
mod_path = data+"Imagettes_mod/"
csv_path = data+"CorrespondancesModif.csv"
# to_sfc_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/Imagette_SFC/"
to_sfc_path = data+"Imagette_SFC_polar/"
# csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif-redo.csv"

miss_path = data+"missClass/"



def cart2pol(x, y):
    x1 = x - 32
    y1 = y - 32
    rho = np.sqrt(x1**2 + y1**2)
    phi = np.arctan2(y1, x1)
    return(rho, phi + math.pi)

newX = 64
newY = 64

file = 0
c = 0

csv = np.loadtxt(csv_path, delimiter=';', dtype=str)
print(csv)

for l in csv:
    print(l[file])

    # img = binJordan(img_path+l[file]+".png")
    img = binJordan(img_path+l[file])
    #
    path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"-bin.png"
    # cv2.imwrite(path_out, img)
    #
    threshold = 127
    img, _, _ = resizeMe(img)
    x, y = img.shape
    #
    blackPix = []
    for i in range(x):
        for j in range(y):
            if img[i][j] < threshold:
                # img[i][j] = 0
                blackPix.append((i,j))

    maxlen = 0
    bigBlack = None
    blackSeg = growingRegion(img, blackPix)
    for e in blackSeg:
        if len(e) < (x * y) / 64:
                for i in e:
                    img[i[0]][i[1]] = 255
        if len(e) > maxlen:
            maxlen = len(e)
            bigBlack = e

    img, up, left = resizeMe(img)

    # path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"-crop.png"
    # cv2.imwrite(path_out, img)

    sumX = 0
    sumY = 0
    for i in bigBlack:
        sumX += i[0] - up
        sumY += i[1] - left

    gx = math.ceil(sumX / len(bigBlack))
    gy = math.ceil(sumY / len(bigBlack))


    # img[int(x/2)][int(y/2)] = 127

    cx = math.ceil(len(img) / 2)
    cy = math.ceil(len(img[0]) / 2)

    # for i in range(len(img[0])):
        # row[0][i]

    if cx < gx:
        while gx != cx-1:
            gx -= 1
            img = np.insert(img, len(img), 255, axis=0)

    elif cx > gx:
        while gx != cx+1:
            gx += 1
            img = np.insert(img, 0, 255, axis=0)

    if cy < gy:
        while gy != cy-1:
            gy -= 1
            img = np.insert(img, len(img[0]), 255, axis=1)

    elif cy > gy:
        while gy != cy+1:
            gy += 1
            img = np.insert(img, 0, 255, axis=1)

    img = cv2.resize(img, (newX, newY))
    # img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
    # cv2.imshow('test', img)
    path_out = mod_path+(l[file]).split('/')[-1].split('.')[0]+".png"
    cv2.imwrite(path_out, img)

    path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"-centered.png"
    cv2.imwrite(path_out, img)

    with open(to_sfc_path+(l[file]).split('/')[-1].split('.')[0]+".txt", "w") as fout:
        for i in range(len(img)):
            for j in range(len(img[0])):
                if img[i][j] != 255:
                    # fout.write("{}, {}, {}\n".format(i, j, int(img[i][j])))
                    polCoord = cart2pol(i, j)
                    fout.write("{}, {}, {}\n".format(int(polCoord[0]), int(polCoord[1]*10), int(img[i][j])))

    # k = cv2.waitKey(0)
    # cv2.destroyAllWindows()
