# import cv2
import os
import math
import numpy as np
from tools import *

data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/"
# data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharSymbola/"


csv_path = data+"CorrespondancesModif.csv"
# to_sfc_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/Imagette_SFC/"
to_sfc_path = data+"Imagette_SFC_polar/"

# csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif-test.csv"

sfc_path = data+"SFC/"


sfcBuild = "fileSfc"
curveD3 = "../../Space-filling-curves/curveBank/dim3/d3-rbg.txt"

file = 0
c = 0

orderD = "8"

csv = np.loadtxt(csv_path, delimiter=';', dtype=str)
# print(csv)

for l in csv:
    txtFile = to_sfc_path+(l[file]).split('/')[-1].split('.')[0]+".txt"
    outFile = sfc_path+(l[file]).split('/')[-1].split('.')[0]+".txt"
    print(l[file], txtFile)

    args = ["-O" + orderD, "-C" + curveD3,
            "-F" + txtFile, "-f" + outFile, "-d"+","]

    pid2 = os.fork()
    if pid2 == 0:
        os.execlp(sfcBuild, sfcBuild, args[0], args[1], args[2], args[3], args[4])

    st = os.waitpid(pid2, 0)
