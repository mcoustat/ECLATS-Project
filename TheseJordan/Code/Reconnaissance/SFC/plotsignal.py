import numpy as np
import matplotlib.pyplot as plt

a = sorted(np.loadtxt("Dataset_CharRousselotGillerion/SFC/image1.txt"))
c = sorted(np.loadtxt("Dataset_CharRousselotGillerion/SFC/image10.txt"))
d = sorted(np.loadtxt("Dataset_CharRousselotGillerion/SFC/image100.txt"))
f = sorted(np.loadtxt("Dataset_CharRousselotGillerion/SFC/image110.txt"))


b = np.loadtxt("Dataset_CharRousselotGillerion/SFC/bins-0.txt", delimiter=",")

bin = []
for e in b:
    bin.append(e[1])


fig, axs = plt.subplots(1, 4, sharey=True, tight_layout=True)

# We can set the number of bins with the `bins` kwarg
axs[0].hist(a, bins=bin[:210])
axs[1].hist(c, bins=bin[:210])
axs[2].hist(d, bins=bin[:210])
axs[3].hist(f, bins=bin[:210])


plt.show()
