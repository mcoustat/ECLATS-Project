from scipy.spatial import distance
import numpy as np
import sys
from shutil import copyfile

# data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/"
data = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharSymbola/"


inBins = data+"SFC/bins-{}.txt".format(sys.argv[1])
sfc_path = data+"SFC/"
train_path = data+"train-{}.txt".format(sys.argv[1])
test_path = data+"test-{}.txt".format(sys.argv[1])

mod_path = data+"Imagettes_mod/"
miss_path = data+"missClass/"


def concat(l):
    res = [0 for i in range(len(l[1]))]
    for i in range(1, len(l)):
        for j in range(len(l[i])):
            res[j] += l[i][j]

    for j in range(len(l[i])):
        res[j] = res[j] / (len(l)-1)

    return res


def fileToHisto(file, bins):
    res = [0 for i in range(len(bins))]

    f = np.loadtxt(sfc_path+file, delimiter=',', dtype=int)

    for p in f:
        for i in range(len(bins)):
            if p >= bins[i][0] and p<bins[i][1]:
                res[i] += 1
                break


    return res

def dist(a, b):
    return distance.cosine(a,b)
    # return distance.minkowski(a,b)
    # return distance.euclidean(a,b)


#load bin and trainClass
train_csv = np.loadtxt(train_path, delimiter=',', dtype=str)
bin = np.loadtxt(inBins, delimiter=',', dtype=int)
test_csv = np.loadtxt(test_path, delimiter=',', dtype=str)


threshold = 0.4

histoClass = []
classH = []
cpt = {}


for l in train_csv:
    h = fileToHisto(l[0], bin)

    insert = False
    for ke in classH:
        # if dist(h, ke[1]) < threshold and l[1] == ke[0]:
        if l[1] == ke[0]:
            ke.append(h)
            insert = True
            # print(l[1], ke[0])
            break
    if insert == False:
        classH.append([l[1], h])

# for e in classH:
#     print(e[0], len(e)-1)
# print(len(classH))

for e in classH:
    histoClass.append([e[0], concat(e)])
print(len(histoClass), len(train_csv))
# print(histoClass)

sumOk = 0
sumKo = 0
for l in test_csv:
    candidat = fileToHisto(l[0], bin)
    res = float("inf")
    c = None

    for k in histoClass:
        d = dist(candidat, k[1])
        if d < res:
            c = k[0]
            res = d
    # print(l[0], c, l[1])

    if c == l[1]:
        sumOk += 1
    else:
        # print(l[0], c, l[1])
        # copyfile(mod_path+l[0].split(".")[0]+".png", miss_path+l[0].split(".")[0]+"-"+c+".png")
        sumKo += 1

    # print(l[1], c, l[0])

    # if c != l[1]:
    #     print(l[0], c, l[1])

print("ok {}, ko {}, {}".format(sumOk, sumKo, sumOk*100/len(test_csv)))
