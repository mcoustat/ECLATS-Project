from scipy.spatial import distance
import numpy as np
import sys

inBins = "Dataset_CharRousselotGillerion/SFC/bins-{}.txt".format(sys.argv[1])
sfc_path = "Dataset_CharRousselotGillerion/SFC/"
train_path = "Dataset_CharRousselotGillerion/train-{}.txt".format(sys.argv[1])
test_path = "Dataset_CharRousselotGillerion/test-{}.txt".format(sys.argv[1])

def fileToHisto(file, bins):
    res = [0 for i in range(len(bins))]

    f = np.loadtxt(sfc_path+file, delimiter=',', dtype=int)

    for p in f:
        for i in range(len(bins)):
            if p >= bins[i][0] and p<bins[i][1]:
                res[i] += 1
                break


    return res

def dist(a, b):
    return distance.cosine(a,b)
    # return distance.minkowski(a,b)
    # return distance.euclidean(a,b)


#load bin and trainClass
train_csv = np.loadtxt(train_path, delimiter=',', dtype=str)
bin = np.loadtxt(inBins, delimiter=',', dtype=int)
test_csv = np.loadtxt(test_path, delimiter=',', dtype=str)



histoClass = {}
cpt = {}

for l in train_csv:
    h = fileToHisto(l[0], bin)
    try:
        for i in range(len(h)):
            histoClass[l[1]][i] += h[i]
        cpt[l[1]] += 1
    except KeyError:
            histoClass[l[1]] = h
            cpt[l[1]] = 1

for k in histoClass.keys():
    for i in range(len(histoClass[k])):
        histoClass[k][i] /= cpt[k]

print(len(histoClass.keys()), len(train_csv))
# print(histoClass)
sumOk = 0
sumKo = 0
for l in test_csv:
    candidat = fileToHisto(l[0], bin)
    res = float("inf")
    c = None
    for k in histoClass.keys():
        d = dist(candidat, histoClass[k])
        if d < res:
            c = k
            res = d
    if c == l[1]:
        sumOk += 1
    else:
        sumKo += 1
        # print(l[0], c, l[1])

    # print(l[1], c)

print("ok {}, ko {}, {}".format(sumOk, sumKo, sumOk*100/len(test_csv)))
