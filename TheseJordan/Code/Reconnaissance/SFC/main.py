import cv2
import math
import numpy as np
from tools import *
import os

def binJordan(img):
    bin = "build/main"
    print(img)
    pid2 = os.fork()
    if pid2 == 0:
        os.execlp(bin, bin, img)

    st = os.waitpid(pid2, 0)

    res = cv2.imread("/tmp/ocrTemp.png", cv2.IMREAD_GRAYSCALE)
    return res


img_path = "Dataset_CharRousselotGillerion/Imagettes/"
mod_path = "Dataset_CharRousselotGillerion/Imagettes_mod/"
csv_path = "Dataset_CharRousselotGillerion/CorrespondancesModif.csv"
# to_sfc_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/Imagette_SFC/"
to_sfc_path = "Dataset_CharRousselotGillerion/Imagette_SFC_polar/"
csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif-redo.csv"

miss_path = "Dataset_CharRousselotGillerion/missClass/"


def cart2pol(x, y):
    x1 = x - 32
    y1 = y - 32
    rho = np.sqrt(x1**2 + y1**2)
    phi = np.arctan2(y1, x1)
    return(rho, phi + math.pi)

newX = 64
newY = 64

file = 0
c = 0

csv = np.loadtxt(csv_path, delimiter=';', dtype=str)
print(csv)

for l in csv:
    print(l[file])
    # print(img_path+l[file])
    # img = cv2.imread(img_path+l[file], 0)

    # path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"ori.png"
    # cv2.imwrite(path_out, img)
    # exit()
    # cv2.imshow('ori '+l[file].split("/")[-1], img)
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # img = img[:, :, 0]
    # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # img = (img - np.mean(img)) / np.std(img)
    # img = img / np.std(img)

    img = binJordan(img_path+l[file])

    path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"extract.png"
    cv2.imwrite(path_out, img)

    # threshold = np.mean(img) / np.std(img)
    threshold = 127
    # threshold = np.mean(img)

    # threshold = np.mean(img)
    print(img.shape)
    x, y = img.shape
    #boxing

    # for i in range(x):
    #     img[i][0] = 255
    #     img[i][y-1] = 255
    # for j in range(y):
    #     img[0][j] = 255
    #     img[x-1][j] = 255


    blackPix = []
    for i in range(x):
        for j in range(y):
            if img[i][j] < threshold:
                # img[i][j] = 0
                blackPix.append((i,j))
            # else:
            #     img[i][j] = 255

    # print(img)
    # exit()


    blackSeg = growingRegion(img, blackPix)
    for e in blackSeg:
        if len(e) < (x * y) / 100:
                for i in e:
                    img[i[0]][i[1]] = 255

    # print(img)

    img = resizeMe(img)
    x, y = img.shape
    # print(x, y)
    # print("img",img)

    # path_out = miss_path+(l[file]).split('/')[-1].split('.')[0]+"extract2.png"
    # cv2.imwrite(path_out, img)

    blackPix = []
    for i in range(x):
        for j in range(y):
            if img[i][j] < threshold:
                blackPix.append((i,j))

    maxlen = 0
    bigBlack = None
    blackSeg = growingRegion(img, blackPix)
    # print(blackSeg, blackPix)
    for e in blackSeg:
        if len(e) > maxlen:
            maxlen = len(e)
            bigBlack = e

    sumX = 0
    sumY = 0
    for i in bigBlack:
        sumX += i[0]
        sumY += i[1]

    gx = math.ceil(sumX / len(bigBlack))
    gy = math.ceil(sumY / len(bigBlack))

    # img[gx][gy] = 127

    cx = math.ceil(len(img) / 2)
    cy = math.ceil(len(img[0]) / 2)

    # for i in range(len(img[0])):
        # row[0][i]

    if cx < gx:
        while gx != cx-1:
            gx -= 1
            img = np.insert(img, len(img), 255, axis=0)

    elif cx > gx:
        while gx != cx+1:
            gx += 1
            img = np.insert(img, 0, 255, axis=0)

    if cy < gy:
        while gy != cy-1:
            gy -= 1
            img = np.insert(img, len(img[0]), 255, axis=1)

    elif cy > gy:
        while gy != cy+1:
            gy += 1
            img = np.insert(img, 0, 255, axis=1)

    img = cv2.resize(img, (newX, newY))
    # img = cv2.cvtColor(img, cv2.COLOR_HSV2BGR)
    # cv2.imshow('test', img)
    path_out = mod_path+(l[file]).split('/')[-1].split('.')[0]+".png"
    cv2.imwrite(path_out, img)

    with open(to_sfc_path+(l[file]).split('/')[-1].split('.')[0]+".txt", "w") as fout:
        for i in range(len(img)):
            for j in range(len(img[0])):
                if img[i][j] != 255:
                    # fout.write("{}, {}, {}\n".format(i, j, int(img[i][j])))
                    polCoord = cart2pol(i, j)
                    fout.write("{}, {}, {}\n".format(int(polCoord[0]), int(polCoord[1]*10), int(img[i][j])))

    # k = cv2.waitKey(0)
    # cv2.destroyAllWindows()
