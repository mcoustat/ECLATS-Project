from functools import reduce
import itertools
import numpy as np
import matplotlib.pyplot as plt
import sys

from sklearn.metrics import confusion_matrix

def plot_confusion_matrix(cm, target_names, title='Confusion matrix', cmap=None, normalize=False):
    accuracy = np.trace(cm) / np.sum(cm).astype('float')
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)


    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")


    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()

if len(sys.argv) != 2:
    print("i need a file")
    exit()


fileIn = sys.argv[1]
# import some data to play with
fin = open(fileIn, "r")

x = [] #classe
y = [] #predict

class_names = ["a", "c", "d", "e", "g", "i", "n", "o", "oe", "ou", "r", "u", "z"]
class_b = {"a":0, "c":1, "d":2, "e":3, "g":4, "i":5, "n":6, "o":7, "oe":8, "ou":9, "r":10, "u":11, "z":12}

buff = fin.readline().split(" ")

while buff != ['']:
    pred, classe = buff[1].split("\n")[0], buff[0].split("\n")[0]
    x.append(class_b[pred])
    y.append(class_b[classe])

    buff = fin.readline().split(" ")

# Compute confusion matrix
cnf_matrix = confusion_matrix(x, y)
# num_classes = len(cnf_matrix)

# TruePositive = np.diag(cnf_matrix)

# FalsePositive = []
# for i in range(num_classes):
#     FalsePositive.append(sum(cnf_matrix[:,i]) - cnf_matrix[i,i])

# FalseNegative = []
# for i in range(num_classes):
#     FalseNegative.append(sum(cnf_matrix[i,:]) - cnf_matrix[i,i])

# TrueNegative = []
# for i in range(num_classes):
#     temp = np.delete(cnf_matrix, i, 0)   # delete ith row
#     temp = np.delete(temp, i, 1)  # delete ith column
#     TrueNegative.append(sum(sum(temp)))

# print(TruePositive)
# print(FalsePositive)
# print(FalseNegative)
# print(TrueNegative)

sumTotal = 0
sumOk = 0

for i in range(len(cnf_matrix)):
    for j in range(len(cnf_matrix[0])):
        sumTotal += cnf_matrix[i][j]
        if i == j:
            sumOk += cnf_matrix[i][j]

# tn = reduce((lambda x, y : x+y), TrueNegative)
# fp = reduce((lambda x, y : x+y), FalsePositive)
# fn = reduce((lambda x, y : x+y), FalseNegative)
# tp = reduce((lambda x, y : x+y), TruePositive)

# prec = tp/(tp+fp)
# recall = tp /(tp+fn)
# fscore = 2* ((prec*recall)/(prec+recall))
print("total ok", (sumOk*100) / (sumTotal) )
print("total ko", (((sumTotal) - sumOk)*100) /sumTotal )
# print("tp, tn, fp, fn", tn, tp, fp, fn)
# print("recall", recall)
# print("precision", prec)
# print("fscore", fscore)
#
# np.set_printoptions(precision=0)
#
# # Plot non-normalized confusion matrix
# plt.figure()
# plot_confusion_matrix(cnf_matrix, classes=class_names,
#                      title='Confusion matrix, without normalization')
#
# # Plot normalized confusion matrix
# plt.figure()
# plot_confusion_matrix(cnf_matrix, classes=class_names, normalize=True,
#                      title='Normalized confusion matrix')

# plot_confusion_matrix(cnf_matrix,
#                           class_names,
#                           title='Confusion matrix')

plt.show()

print(cnf_matrix)
