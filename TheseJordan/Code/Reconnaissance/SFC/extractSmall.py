import cv2
import numpy as np

img_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/Imagettes/"
csv_path = "/Users/valentinowczarek/Documents/GitHub/These/ocrSfc/Dataset_CharRousselotGillerion/CorrespondancesModif.csv"

file = 0
c = 1

csv = np.loadtxt(csv_path, delimiter=';', dtype=str)
# print(csv)

for l in csv:
    img = cv2.imread(img_path+l[file])

    if len(img) < 20 or len(img[0])< 20:
        print(l[file])
