<?php

$cartesClass = 'formatTIF';
$nomsClass = 'carteID';
$dom = new DOMDocument();
$dom->loadHTMLFile("D:\Cartes\ALF.html");
$xpath = new DOMXPath($dom);
$cartes = $xpath->query("//*[@class='" . $cartesClass . "']/*[1]/@href");
$noms = $xpath->query("//*[@class='" . $nomsClass . "']"); 

if ($cartes->length > 0) {
    for ($i = 0; $i < $cartes->length; $i++) {
        $spaces = array("\t", "\r", "\n");
        $filename = str_replace($spaces, "", $noms->item($i)->nodeValue);
        file_put_contents('D:\Cartes\\'.$filename.'.tif', fopen($cartes->item($i)->value, 'r'));
    }
} else {
    echo "RIEN";
}

?>
</body>
</html>