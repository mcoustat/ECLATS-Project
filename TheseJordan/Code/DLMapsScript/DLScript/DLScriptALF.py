#!/usr/bin/python
import urllib
from urllib2 import urlopen
import bs4 as BeautifulSoup

url = "http://cartodialect.imag.fr"

html = urlopen(url+'/cartoDialect/carteTheme').read()
soup = BeautifulSoup.BeautifulSoup(html, "html.parser")

results = soup.findAll('td', attrs={"class":u"formatTIF"})
for result in results:
	urllib.urlretrieve( url+(result.a.get('href').split(';')[0]), result.a.get('href').split(';')[0].split('/')[-1])
