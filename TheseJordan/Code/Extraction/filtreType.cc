#include <mln/arith/diff_abs.hh>
#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>
#include <iostream>

#include "max_tree.hh"
#include "min_tree.hh"

#include "compute_area_image.hh"
#include "area_filter.hh"

void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm output.pgm" << std::endl;
  std::abort();
}

// g++ -std=c++11 -DNDEBUG -O3 -I. filtreType.cc -o filtreType

int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;
  
  if (argc != 3)
    usage(argv);

  std::cout << "├── Read input image" << std::endl;
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm

  
  if(input.bbox().ncols() > input.bbox().nrows()){
    std::cout << "Format PAYSAGE" << std::endl;
  } else {
    std::cout << "Format PORTRAIT" << std::endl;
  }

  // Aires blanches ou noires?
  std::cout << "├─── Min-Tree" << std::endl;
  my::tree t = my::min_tree(input);
  /*
  std::cout << "├─── Max-Tree" << std::endl;
  my::tree t = my::max_tree(input);
  */
  
  std::cout << "├──── Filtering" << std::endl;
  image2d<int_u8> result;
  if(input.bbox().ncols() > input.bbox().nrows()){
    result = my::area_filter(t, input.bbox().ncols()*2);
  } else {
    result = my::area_filter(t, input.bbox().nrows()*2);
  }

  std::cout << "├───── Saving result" << std::endl;
  io::pgm::save(result, argv[2]);

  std::cout << "└────── DONE" << std::endl;
  
  /*
  std::cout << "├ └─ Largeur : " << input.bbox().ncols() << std::endl;
  std::cout << "├ └─ Hauteur : " << input.bbox().nrows() << std::endl;
  */
  
}
