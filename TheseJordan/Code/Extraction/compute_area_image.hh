#ifndef COMPUTE_AREA_IMAGE_HH
# define COMPUTE_AREA_IMAGE_HH

# include "tree.hh"


namespace mln
{

  namespace my
  {
    

    // compute an image of the area attribute

    image2d<unsigned> compute_area_image(const tree& t)
    {
      image2d<unsigned> area(t.domain());
      data::fill(area, 1);

      for (unsigned i = t.S.size() - 1; i != 0; --i)
	{
	  point2d p = t.S[i];  // p goes from leaves to root (excluded thanx to "i != 0")
	  area(t.parent(p)) += area(p);
	}
      
      t.back_propagate(area);
      return area;
    }


  } // end of namespace mln::my

} // end of namespace mln


#endif // ndef COMPUTE_AREA_IMAGE_HH
