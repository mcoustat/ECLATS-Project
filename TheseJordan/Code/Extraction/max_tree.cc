#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>

#include "max_tree.hh"

#include "compute_area_image.hh"
#include "area_filter.hh"

#include "compute_attribute.hh"
#include "bbox.hh"



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm" << std::endl;
  std::abort();
}


// g++ -DNDEBUG -O3 -I. max_tree.cc -o max_tree


int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 2)
    usage(argv);

  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm
  debug::println("input", input);

  
  // max tree:
  
  my::tree t = my::max_tree(input);
  debug::println("parent", t.parent);


  // image of an attribute:
  
  debug::println("area", my::compute_area_image(t));

  
  // filtering:
  
  image2d<int_u8> ope = my::area_filter(t, 5);
  debug::println("area opening", ope);
  
  {
    my::tree t_ = my::max_tree(ope);
    debug::println("parent of ope", t_.parent);
    debug::println("area of ope", my::compute_area_image(t_));
  }


  image2d<box2d> bbs = my::compute_attribute< my::bbox >(t);
  debug::println("bboxes", bbs);
}
