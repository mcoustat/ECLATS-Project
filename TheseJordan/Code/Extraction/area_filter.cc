#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>

#include <iostream>

#include "min_tree.hh"

#include "compute_area_image.hh"
#include "area_filter.hh"

#include "compute_attribute.hh"
#include "bbox.hh"

#include "dimension_filter.hh"



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm area output.pgm" << std::endl;
  std::abort();
}


// g++ -std=c++11 -DNDEBUG -O3 -I. area_filter.cc -o area_filter


int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 4)
    usage(argv);
  
  std::cout << "├── Read input image" << std::endl;
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm
  
  std::cout << "├─── Building Min-Tree" << std::endl;
  my::tree t = my::min_tree(input);
  
  std::cout << "├──── Filtering Area" << std::endl;
  int lambda = std::stoi(argv[2]);
  image2d<int_u8> result = my::area_filter(t, lambda);

  std::cout << "├───── Saving result" << std::endl;
  io::pgm::save(result, argv[3]);

  std::cout << "└────── DONE" << std::endl;
  
}
