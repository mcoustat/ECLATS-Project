#ifndef DIMENSION_FILTER_HH
# define DIMENSION_FILTER_HH

# include "compute_attribute.hh"

#include <iostream>
#include <string>
#include <mln/debug/println.hh>



namespace mln
{

  namespace my
  {


    image2d<value::int_u8>
    dimension_filter(const tree& t, std::string op, unsigned length, unsigned width)
    {
      image2d<box2d> bbs = my::compute_attribute< my::bbox >(t);
      //debug::println("bboxes", bbs);

      image2d<value::int_u8> output(t.domain());

      if (op == "and") {
        std::cout << "├ └─── op AND" << std::endl;
        for (unsigned i = 0; i < t.S.size(); ++i)
	{
	  point2d p = t.S[i];  // from root to leaves
	  if (t.is_representative(p) and bbs(p).ncols() >= length and bbs(p).nrows() >= width){
	    //std::cout << "[ longueur : " + std::to_string(bbs(p).ncols()) + ",hauteur : " + std::to_string(bbs(p).nrows()) + " ]"<< std::endl;
	    output(p) = t.u(p);
	  }else
	    output(p) = output(t.parent(p));
	}
      } else if(op == "or") {
        std::cout << "├ └─── op OR" << std::endl;
        for (unsigned i = 0; i < t.S.size(); ++i)
	{
	  point2d p = t.S[i];  // from root to leaves
	  if ((t.is_representative(p) and bbs(p).ncols() >= length) or (t.is_representative(p) and bbs(p).nrows() >= width)){
            //std::cout << "[ longueur : " + std::to_string(bbs(p).ncols()) + ",hauteur : " + std::to_string(bbs(p).nrows()) + " ]"<< std::endl;
	    output(p) = t.u(p);
	  }else
	    output(p) = output(t.parent(p));
	}
      } else {
        std::cout << "├ └─── ERROR : op has no value [and,or]" << std::endl;
      }
      return output;
    }


  } // end of namespace mln::my

} // end of namespace mln


#endif // ndef AREA_FILTER_HH
