#ifndef MIN_TREE_HH
# define MIN_TREE_HH

#include "tree.hh"


namespace mln
{

  namespace my
  {

    
    std::vector<point2d>
    sort_decreasingly(const image2d<value::int_u8>& u)
    {
      // h
      std::vector<unsigned> h(256, 0);
      mln_piter(box2d) p(u.domain());
      for_all(p)
	++h[u(p)];
    
      // index
      std::vector<unsigned> index(256);
      index[255] = 0;
      for (int i = 254; i >= 0; --i)
	index[i] = index[i + 1] + h[i + 1];

      // S
      std::vector<point2d> S(u.nsites());
      for_all(p)
	  S[index[u(p)]++] = p;

      return S;
    }

 
    tree
    min_tree(const image2d<value::int_u8>& u)
    {
      tree t(u);
      t.S = sort_decreasingly(u);

      union_find(t);
      t.canonicalize();

      return t;
    }


  } // my

} // mln


#endif // ndef MIN_TREE_HH
