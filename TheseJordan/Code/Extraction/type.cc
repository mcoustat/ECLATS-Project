#include <mln/arith/diff_abs.hh>
#include <mln/io/pgm/all.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/debug/println.hh>
#include <iostream>

void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.pgm" << std::endl;
  std::abort();
}

// g++ -std=c++11 -DNDEBUG -O3 -I. type.cc -o type

int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;
  
  if (argc != 2)
    usage(argv);
  
  std::cout << "├── Read input image" << std::endl;
  image2d<int_u8> input;
  io::pgm::load(input, argv[1]); // /!\ fichier .pgm
  
  std::cout << "├ └─ Largeur : " << input.bbox().ncols() << std::endl;
  std::cout << "├ └─ Hauteur : " << input.bbox().nrows() << std::endl;
  
  if(input.bbox().ncols() > input.bbox().nrows()){
    // Vérifier le type de page (on peux regarder le titre s'il termine par A ou B avant tout)

    // points d'interets --> difficile a mettre en place, systeme de filtrage par dessus car grand image = grand nombre de points
    // densité de pixels --> le plus simple a mettre en place (a voir) MAIS la fiabilite tres certainement la moins elevee
    // aire des zones --> le juste milieu des deux, moins simple a mettre en place que la densité, mais des resultats peut-etre un peu mieux
    std::cout << "├   └─ Type : Format Paysage" << std::endl;
  } else {
    std::cout << "├   └─ Type : Format Portrait" << std::endl;
  }
  
  std::cout << "└────── DONE" << std::endl;
  
}
