#include <cmath>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>

#include <mln/morpho/closing/structural.hh>
#include <mln/arith/min.hh>
#include <mln/data/median.hh>

#include <mln/win/hline2d.hh>
#include <mln/win/vline2d.hh>



void usage(char* argv[])
{
  std::cerr << "usage: " << argv[0] << " input.xxx output.xxx" << std::endl;
  std::abort();
}

// g++ -std=c++11 -DNDEBUG -O3 -I. filtering.cc -o filtering

int main(int argc, char* argv[])
{
  using namespace mln;
  using value::int_u8;

  if (argc != 3)
    usage(argv);

  image2d<int_u8> input;
  io::pgm::load(input, argv[1]);

  image2d<int_u8>
    //v = morpho::closing::structural(input, win::hline2d(29)),
    //h = morpho::closing::structural(input, win::vline2d(29));
    v = morpho::closing::structural(input, win::hline2d(45)),
    h = morpho::closing::structural(input, win::vline2d(45));

  //v = data::median(v, win::hline2d(11));
  //h = data::median(h, win::vline2d(11));
  v = data::median(v, win::hline2d(17));
  h = data::median(h, win::vline2d(17));

  input = arith::min(h, v);
  io::pgm::save(input, argv[2]);
}
