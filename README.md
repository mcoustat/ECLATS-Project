# Projet ANR ECLAT

## Le Projet

Le  projet  ANR  ECLATS (Extraction  de  Contenus  géoLinguistiques  d'ATlas  et  analyse  Spatiale) concerne  la  valorisation  et l’analyse des documents cartographiques anciens, un patrimoine historique et culturel reconnu comme  source  d’information  particulièrement  riche mais difficilement   exploitable. Le   projets’intéresseplus particulièrement à l’Atlas Linguistiques de France (ALF), élaborés entre 1902 et 1910 et qui fournit les données de premier ordre en dialectologie. L’objectif est d’apporter un outillage logiciel et méthodologique facilitant l’extraction, l’analyse, la visualisation et la diffusion des données contenues  dans  les  atlas linguistiques  anciens  afin  de  permettre  des  recherches  novatrices  en dialectologie.


## L'objectif

À chaque concept exprimé par une entrée lexicale sous la forme d’un titre de carte en français est associée  une  et  une  seule  carte  sur  laquelle  figurent  toutes  les  formes  dialectales transcrites phonétiquement  désignant  le  concept  en  question.  Ces  cartes  affichent principalement  trois  types d’informations qui sont:
- Les noms de départements, toujours encadrés.
- Les numéros de point d’enquête, qui sont des regroupements de chiffres.
- Les prononciations en phonétique d’un mot, écrits en alphabet Rousselot-Gilliéron,  faire attention aux diacritiques (accents qui sont au-dessus ou en dessous des lettres).

Le but a donc été de savoir extraire ces informations contenues dans les cartes. L’objectif n’était pas seulement de les extraire, mais aussi  de  savoir  les  classer  dans  des  catégories (noms de département, numéros de point d’enquête, mots en phonétique, et savoir identifier les frontières).

Dans la suite logique du premier but, le second consiste à faire de la reconnaissance de la caractère sur les boites précédemment extraites, notamment sur les mots phonétiques.


## Prérequis
- Images
	- [Cartes ALF](http://cartodialect.imag.fr/)
	- [Datasets de mots et caractères](https://git2017.univ-lr.fr/mcoustat/ECLATS-Project/-/tree/master/TheseJordan/These/DatasetsALF_RousselotGillieron.zip)
- Annotation
    - [Apache](https://httpd.apache.org/download.cgi)
- Extraction
    - [Olena](https://www.lrde.epita.fr/wiki/Olena/Download)
- Reconnaissance
    - [OpenCV](https://opencv.org/releases/)


## Erreurs/Problèmes de reconnaissance

- Le volume du jeu de données est assez faible, il faut donc faire attention aux méthodes utilisées, voir penser à une méthode d'augmentation du volume de données par intégration de bruits sur celui-ci.
- Les mots phonétiques des cartes ne sont pas dans un contexte (phrase), et il n'y a pas non plus de dictionnaire de mots existants, ni de règles phonétiques établies ou possible à étalir, ce qui rend la correction POST-OCR infaisable.
- L'affichage des diacritiques peut varier en fonction de l'éditeur utilisé. Il faut donc mettre la priorité sur le caractère en unicode plutôt qu'à sa visualisation.