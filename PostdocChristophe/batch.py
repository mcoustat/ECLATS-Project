#!/bin/bash
# script by Christophe Rigaud <christophe.rigaud@univ-lr.fr> - http://www.christophe-rigaud.com


import io, os, re, sys, cv2, csv, numpy, pickle, shutil, collections, subprocess
#from matplotlib.pyplot import imshow
#from matplotlib import pyplot as plt

#Text recognition
from PIL import Image
#from tesserocr import PyTessBaseAPI, RIL, PSM, iterate_level, tesseract_version#, easyocr

#HTML export
from Levenshtein import distance as lev
import unicodedata

#%matplotlib inline

class ROI:
    """
        Class defining a detected region
        
        Attributes:
            id            its ID
            cnt           all the coordinates of the contour
            BdB           contour bounding box
            x             minimum left position
            y             minimum top position
            w             widht
            w             height
            transcription OCR transcript
    """
    
    def __init__(self, id, cnt, cat=None):
        """
            Object constructor

            param id: object id (string)
            param cnt: sequence of coordinate of the polugonal contour (Numpy array)
        """
        self.id = id
        self.type = cat #d/p/a
        self.links = [] #d/p/a ID list
        self.cnt = cnt
        self.BdB = cv2.boundingRect(cnt)
        self.x = self.BdB[0]
        self.y = self.BdB[1]
        self.w = self.BdB[2]
        self.h = self.BdB[3]
        self.cx = self.BdB[0]+int(self.BdB[2]/2)
        self.cy = self.BdB[1]+int(self.BdB[3]/2)
        self.transcription = ""
        self.retrotranscription = ""


def replaceOnce(lList, char, corresp, done):
    index = 0;
    #while index < len(sentence):
    for index, c in enumerate(lList):
        #index = sentence.find(char, index)
        #if index == -1:
        #    break
        #If searched caracter found
        if c == char:
            #print('{} found at {} replace by {}'.format(char, index, corresp))
            #Only if not processed yet
            if index not in done:
                #Replace into sentence
                #print(char,'found at pos',index,'replaced by',corresp)
                lList[index] = corresp
                done.append(index)
            #else:
            #    print(index,'is already done')
        #index += len(char)
    
    return lList, done
        
def convert(l, d):
    """
    Convert a string from API to Rousselot alphabet
    
    param d list of correspondances (API>Rousselot)
    param l text line in API language
    return converted text line, n?, number of ok and nok letters
    """
    
    #Convert text line string into a list
    lList = []
    skip = []
    for i,c in enumerate(l):
        #Skip
        if i in skip:
            continue
        #Add char
        if i > 0 and i < len(l)-1 and (l[i+1] == ' ̣ ' or l[i+1] == 'ː' or l[i] == 'ˈ'):
            if l[i] == 'ˈ' and i < len(l)-2:
                if l[i+2] == 'ː':
                    lList.append(l[i]+l[i+1]+l[i+2])
                    skip.append(i+1)
                    skip.append(i+2)
            else:
                lList.append(l[i]+l[i+1])
                skip.append(i+1)
        else:
            lList.append(c)
    #print(lList)
    ok = []
    nok = []
    
    #for c in l:
    f = False
    doneIdx = []
    #print(l)
        
    #Browse dic STARTING BY LONGUEST API CODES
    for k,v in d:#.items():
        #print(k,end=' ')
        if k in l and len(k)>0:
            #char_length = len(c) + repr(c).count('\\U') #6

            #Debug
            lList, doneIdx = replaceOnce(lList,k,v,doneIdx)
            #print(indexes,v)
            #for idx in indexes:
            #    doneIdx.append(idx)
            #out = out.replace(k,v)
            #print(k,'>',v,out)#,c.encode('unicode-escape'))
            f = True
            if k not in ok:
                ok.append(k)
            #print(lList)

    #if not f:
    #    if k not in nok:
    #        nok.append(k)
    
    #Convert list to string
    out = ""
    for l in lList:
        out = out + l
    return out, ok, nok


def process(mapID, mapFolder, outputFolder, resultFolder):
    """
        MAIN (BIG) LOOP (TO SPIT INTO FUNCTIONS...)
    """

    imageName = "CarteALF"+mapID+".tif" 

    if not os.path.isfile(mapFolder+imageName):
        print('ERROR: map image not found',mapFolder+imageName)
        return [],[],[]

    print('***',mapFolder+imageName,'***')

    #Load full image
    img = cv2.imread(mapFolder+imageName,1)
    img_gray = cv2.imread(mapFolder+imageName,0)
    #imshow(img,cmap='gray')

    #Full map
    roi = img
    roi_gray = img_gray
    #imshow(roi_gray, cmap='gray')

    #Create output folder
    try:
        os.makedirs(outputFolder+mapID, exist_ok=True)
    except OSError:
        print ("Output directory creation %s failed")

    #Binarize
    blockSize = 50 #for full image about 10000x10000 int(img_gray.shape[0] / 20)
    if blockSize %2 == 0:
        blockSize+=1
    #print('blockSize=',blockSize)
    #th, roi_bw = cv2.threshold(roi_gray,160,255,cv2.THRESH_BINARY) #cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    roi_bw = cv2.adaptiveThreshold(roi_gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, blockSize, 20) # (Adaptive Method, size of neighbourhood area, constant which is subtracted from the mean) 
    imgHeight, imgWidth = roi_bw.shape[:2]
    roi_bw_dep_survey = roi_bw.copy()
    #imshow(roi_bw, cmap='gray')

    ##########################
    #   Detect survey points
    ##########################

    picklePath = outputFolder+mapID+os.sep+mapID+'p.pickle'
    if not os.path.isfile(picklePath):

        drawing_pt = cv2.cvtColor(roi_bw.copy(), cv2.COLOR_GRAY2BGR)
        maskTmp = numpy.zeros([imgHeight,imgWidth], numpy.uint8)

        minH = int(imgWidth*0.004)
        maxH = int(imgWidth*0.0055)
        print('min/max survey point height',minH,maxH)

        #Detect contours
        contours, hierachy = cv2.findContours(roi_bw.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        #contours, hierachy = cv2.findContours(roi_bw.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

        #Filter big contours (connected borders)
        for i in range(0, len(contours)):
            
            #Precise size filter
            x,y,w,h = cv2.boundingRect(contours[i])
            if minH < h < maxH:        
                #Offsets
                ox = 2 #int(w/4)
                oy = 1 #int(w/4)
                #Draw candidate
                cv2.rectangle(drawing_pt,(x, y),(x+w, y+h),(255,0,0),1) #blue rectangle
                #Draw into merged mask
                cv2.rectangle(maskTmp,(x-ox, y-oy),(x+w+ox, y+h+oy),255,-1)
                
        #TODO: draw manually known missing survey point ROI (e.g. 354, 398, 733, 798, 830, 841, 878, 888)


        #Detect merged contours from mask
        contourTmp, hierachy = cv2.findContours(maskTmp.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        tmpList = []

        for i,cnt in enumerate(contourTmp):
            #Create a point block
            ID = f'{i:04}'
            p = ROI(str(ID),cnt)
            p.type = 'p'
            tmpList.append(p)
            
            x,y,w,h = cv2.boundingRect(cnt)
            cv2.rectangle(drawing_pt,(x, y),(x+w, y+h),(0,0,255),1) #red rectangle

        #cv2.imwrite('drawing_points.png',drawing_pt)
        #imshow(drawing_pt[5000:6000,4950:6100], cmap='gray')


        ############################
        # Recognize survey points
        ############################

        #API
        #blacklist = "()?![];{}#=%|”“’‘`'*@:§$€£¥><\/°_—«»~&ç¢©®"
        numDic = {}
        n = 0
        pointList = []
        maskTmp = numpy.zeros([imgHeight,imgWidth], numpy.uint8)


        #Create output directory if not exist (should be cleared manually)
        if not os.path.isdir("temp/"+mapID):
            os.mkdir("temp/"+mapID)
            
        #Log file BUG: not working
        #log = open('log.txt', 'w')

        #Draw ROI single numbers (1 to 9)
        cv2.rectangle(drawing_pt,(int(imgWidth/1.9), int(imgHeight/2.3)),(int(imgWidth/1.9+imgWidth/8), int(imgHeight/2.3+imgHeight/14)),(0,150,150),1)

        #with PyTessBaseAPI(lang='fra',path='/home/crigau02/tesstutorial/tesseract/tessdata/') as api: #lang='eng'
        for j,p in enumerate(tmpList):
            x,y,w,h = p.BdB        
            ocrResult = ""
            confList = []
            number = ""
            
            #Check location if single char number TODO: replace by NIEVRE dept distance/location
            isSingle = False
            if imgWidth/1.9 < x < imgWidth/1.9+imgWidth/8:
                if imgHeight/2.3 < y < imgHeight/2.3+imgHeight/14:
                    isSingle = True
                    
                    #Debug
                    #print(number,x,y,imgWidth/1.9,imgWidth/1.9+imgWidth/9)
                    #input_ = input('Press key')

            #Skip if  if higher than large to speed up process (1-9 locations should be added manually then...)
            if h > w and not isSingle:
                print('-',end='')
                continue

            #2021-03-04 replace by Tesseract direct call
            filename = "temp/"+mapID+"/tmp"+str(j)+'.png'
            cv2.imwrite(filename,roi[y:y+h,x:x+w])
            #!/usr/bin/tesseract $filename output --dpi 500 -l fra --psm 8 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot_fra
            #stdout = subprocess.check_output(["tesseract", inputBigPath , tempFilePath, "-l", ocrLang, "--psm", "5"], stderr=subprocess.STDOUT)#, "-psm", "5"])
            subprocess.run(["/usr/bin/tesseract",filename,mapID+"_output","--dpi", "500", "--psm", "8", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"], stderr=subprocess.STDOUT)
            #stdout = subprocess.check_output(["/usr/bin/tesseract",filename,"output","--dpi", "500", "--psm", "8", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"], stdout='log.txt', stderr=subprocess.STDOUT)

            with io.open(mapID+'_output.txt', 'r') as f:
                trans = f.read()
            val = trans.replace('\n','')#[:-1]
            for c in val:
                if c in "0123456789":
                    number += c

            #if len(number) > 0: #.isdigit():
                #conf = np.mean(confList)

                #if conf/100 < 0.5:
                    #plt.imshow(roi[y:y+h,x:x+w],cmap='gray')
                    #plt.show()

                #    print(val,'(',conf/100,')')
                
            
            #TODO: manually fix known issues (e.g. 89 in x,y => 899 etc.)

            n+=1
            
            #Add point candidate if cointains 1 to 4 numbers
            if 0 < len(number) < 5: # and conf/100 > 0.5:

                #Refine replace by new num model output if different
                tnumber = ""
                #!/usr/bin/tesseract $filename output --dpi 500 -l num --psm 8 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot_fra
                subprocess.run(["/usr/bin/tesseract",filename,mapID+"_output","--dpi", "500","-l", "num", "--psm", "8", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"])
                with io.open(mapID+'_output.txt', 'r') as f:
                    trans = f.read()
                tval = trans.replace('\n','')#[:-1]
                for c in tval:
                    if c in "0123456789":
                        tnumber += c

                #Skip if no number found
                if len(tnumber) == 0:
                    continue
                
                #Display
                if tnumber != number:
                    #plt.imshow(roi[y:y+h,x:x+w],cmap='gray')
                    #plt.show()
                    print(number,'->',tnumber)
                
                #Set transcription
                p.transcription = tnumber #number
                pointList.append(p)
                print(p.transcription)
                
                #Save for later checking
                numDic[tnumber] = tnumber
                
                #Draw for display
                cv2.rectangle(drawing_pt,(x-1, y-1),(x+w+1, y+h+1),(0,255,0),1)
                cv2.putText(drawing_pt, str(p.transcription), (x+w,y+h), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 2, cv2.LINE_AA) 
                
                #Draw into mask (for manual post processing only)
                cv2.rectangle(maskTmp,(x-1, y-1),(x+w+1, y+h+1),255,-1)
                
                if exportGT:
                    #Copy to pseudo ground truth GT
                    #shutil.copyfile(filename,'groundtruth/nums/input/'+str(n)+'.png')
                    cv2.imwrite('groundtruth/nums/tif/'+str(n)+'.tif',roi[y:y+h,x:x+w])
                    with io.open('groundtruth/nums/tif/'+str(n)+'.gt.txt', 'w') as f:
                        f.write(trans.replace('\n','').replace('',''))

            #Debug
            #if j > 50:
            #    break
            
            if j%100 == 0:
                print("******************\n",j,'/',len(tmpList),"\n******************")
                
                    
        #cv2.imwrite('drawing_points_reco.png',drawing_pt)
        #cv2.imwrite('drawing_point_mask.png',maskTmp)
        print(n,'candidates,',len(numDic),'numbers\n')#,sorted(num))

        #Close log file BUG: not working
        #log.close()

        #Checking
        n=0

        #Display missing survey points
        csvFile = "data/maps/ALF Nom patois Loc.csv"
        locDic = {}
        with open(csvFile, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
            for i,row in enumerate(spamreader):
                if i > 0 and not row[0] in numDic:
                    print(row[0],end=' ')
                    n+=1
        print('\n',(n/640)*100,'% error,',n,'missing points')

        #Serialize points
        with open(picklePath, 'wb') as f:
            pickle.dump(pointList, f)
        print('Serialize survey points into',picklePath,'...')

        #Debug
        #input()

    else:
        with open(picklePath, 'rb') as f:
            pointList = pickle.load(f)
            print('Load ',len(pointList),' points from',picklePath)



    #######################
    # Detect departments
    #######################

    picklePath = outputFolder+mapID+os.sep+mapID+'d.pickle'
    if not os.path.isfile(picklePath):

        print('Detecting departments...')

        depList = []
        minArea = 1000 #int((imgWidth*imgHeight)*0.00001)
        maxHeight = int(imgWidth/100)
        maxWidth = int(imgWidth/5)
        print('\tMin area =',minArea,'px,','max height =',maxHeight,'px','max width =',maxWidth,'px')

        #Detect connected-components
        contours, hierachy = cv2.findContours(roi_bw.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        drawing_c = roi.copy()
        drawing_b = cv2.cvtColor(roi_bw.copy(), cv2.COLOR_GRAY2BGR)
        depMask = numpy.zeros([imgHeight,imgWidth], numpy.uint8)

        n = 0
        hList = []

        #Filter contours
        for i in range(0, len(contours)):
            cnt = contours[i]
            x,y,w,h = cv2.boundingRect(cnt)

            #Skip if heigher than large
            if not w > h*2:
                continue

            #Skip too big contours
            if h > maxHeight or w > maxWidth:
                continue

            #Small contours (or add them to phonetic text/letter list)
            area = cv2.contourArea(cnt)
            if area < minArea:
                continue

            #DEPARTMENTS

            #Count number of children contours
            p = 0
            for j in range(0, len(contours)):
                
                #Skip too small contours
                area = cv2.contourArea(cnt)
                if area < minArea:
                    continue

                #hierachy[Next, Previous, First_Child, Parent]
                if i == hierachy[0][j][3]: # 3 denotes its parent contour id
                    p += 1
                
            #Filter on children number (between 3 and 99)
            if 2 < p < 100:        
                #Convexity
                #ctnPerimeter = cv2.arcLength(cnt,True) # closed contour = True
                #hullPerimeter = cv2.arcLength(cv2.convexHull(cnt),True)
                #cShape = hullPerimeter / ctnPerimeter
                
                #Filter on convexity
                #if cShape > 0.8:
                #depList.append(ROI(str(i),cnt))
                #Draw dep contour in purple
                cv2.drawContours(drawing_b,[cnt],0,(255,0,255),1)
                cv2.rectangle(drawing_c,(x, y),(x+w, y+h),(255,0,155),1)
                cv2.rectangle(depMask,(x, y),(x+w, y+h),255,-1)
                #print(p,'p',cShape)
                print('.',end='')
                    
                #Filter on content size homogeneity (caps letter are all same size)
                #TODO        
                hList.append(cv2.boundingRect(cnt)[3])
                
            else:
                #Manual constrained from manual estimation 
                if 40 < h < 75:
                    #depList.append(ROI(str(i),cnt))
                    #Draw answer contour in purple
                    cv2.drawContours(drawing_b,[cnt],0,(255,0,255),1)
                    cv2.rectangle(drawing_c,(x, y),(x+w, y+h),(255,0,155),1)
                    cv2.rectangle(depMask,(x, y),(x+w, y+h),255,-1)
                    print('*',end='')    
                        
        #Count number of (non overlapped) contours
        contoursDep, hierachy = cv2.findContours(depMask.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        for i in range(0, len(contoursDep)):
            cnt = contoursDep[i]
            
            #Store
            ID = f'{i:04}'
            depList.append(ROI(str(ID),cnt))
            
            #Draw (overlap) dep contour in red
            x,y,w,h = cv2.boundingRect(cnt)    
            cv2.rectangle(drawing_c,(x, y),(x+w, y+h),(0,0,255),1)
            n += 1
            
        #Stats
        print('\n\t',n,'/','97 dep. candidates found within',len(contours),'connected-components')
        print('\tmedian height =', numpy.median(hList))
        #imshow(drawing_b)



        #########################
        # Recognize departments
        #########################

        print('Recognizing departments...')

        #Create empty dir
        path = 'temp/'+mapID+'/'
        n = 0

        if os.path.isdir(path):
            shutil.rmtree(path, ignore_errors=True)
        try:
            os.makedirs(path)
        except OSError:
            os.remove(path)
            print ("\tDirectory creation %s failed" % path)
        else:
            print ("\tSuccessfully created the directory %s" % path)

        for i,p in enumerate(depList):
            filename = path+'d'+p.id+'.tif'
            #2020-02-19 switch from color to bw (cleaned) image for OCR
            cv2.imwrite(filename, roi_bw[p.y:p.y+p.h, p.x:p.x+p.w])
            
            #TODO: replace by Tesserocr? Faster but v4.0.1?
            #Text recognition using Tesseract v5.0.0-alpha --psm = 8 = single word --psm = 13 Raw line. Treat the image as a single text line (v4.0 and above)
            #!/usr/bin/tesseract $filename output --dpi 500 -l fra --psm 13 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot_fra
            subprocess.run(["/usr/bin/tesseract",filename,mapID+"_output","--dpi", "500", "-l", "fra", "--psm", "13", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"])

            with io.open(mapID+'_output.txt', 'r') as f:
                trans = f.read()
            p.transcription = trans.replace('\n','')#[:-1]   
            
            if len(p.transcription.replace(' ','')) > 0:
                print(p.transcription)
                n += 1
            else:
                print('?',p.id)
            
        print('\n\tDONE (',n,'deps)')

        #Serialize
        with open(picklePath, 'wb') as f:
            pickle.dump(depList, f)
        print('\tSerialize departments into',picklePath)

    else:
        with open(picklePath, 'rb') as f:
            depList = pickle.load(f)
            print('Load ',len(depList),' deps from',picklePath)


    ##############################################
    # Answer background detection and removal 
    ##############################################

    #Background detection and substraction
    roi_tmp = roi_bw.copy()
    imgHeight, imgWidth = roi_tmp.shape[:2]

    #Erase survey points
    print('Erasing point ROIs...')
    for p in pointList:
        x,y,w,h = p.BdB
        #cv2.drawContours(roi_bw_tmp,p.cnt,0,0,-1) #WARNING: cnt "inpainting" seems do not work properly
        cv2.rectangle(roi_tmp,(x, y),(x+w, y+h),255,-1)
        #print('.',end='')
        
    #Erase (enlarged) department rectangles
    print('\nErasing dep ROIs...')
    for d in depList:
        x,y,w,h = d.BdB
        #cv2.drawContours(roi_bw_tmp,p.cnt,0,0,-1) #WARNING: cnt "inpainting" seems do not work properly
        cv2.rectangle(roi_tmp,(x-4, y-4),(x+w+4, y+h+4),255,-1)
        #print('.',end='')

    # Defining the kernel to be used in Top-Hat 
    #filterSize =(51, 51) 
    #kernel = cv2.getStructuringElement(cv2.MORPH_RECT, 
    #                                   filterSize) 

    #Closing operation
    kernel = numpy.ones((9,9),numpy.uint8)
    #roi_bg = cv2.morphologyEx(roi_bw.copy(), cv2.MORPH_OPEN, kernel) # WARNING: use opening instead of closing because image will be inverted?
    #Erode white (grows black)
    roi_erode = cv2.erode(roi_tmp.copy(), kernel)

    #Draw a white frame
    #roi_erode[0:2][:] = 255 #top
    #roi_erode[-2:-1][:] = 255 #bottom
    #roi_erode[:][3000:3300] = 255 #right?

    #Invert
    #roi_erode_inv = cv2.bitwise_not(roi_erode)

    #cv2.imwrite('drawing_erode.png',roi_erode)
    drawing = cv2.cvtColor(roi_erode.copy(), cv2.COLOR_GRAY2BGR)

    # Applying the Black-Hat operation 
    #blackhat_img = cv2.morphologyEx(roi_bw,  
    #                              cv2.MORPH_BLACKHAT, 
    #                              kernel) 

    # Applying the Top-Hat operation 
    #tophat_img = cv2.morphologyEx(roi_bw,  
    #                              cv2.MORPH_TOPHAT, 
    #kernel) 

    #Detect contours
    #contours, hierachy = cv2.findContours(roi_erode.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    contours, hierachy = cv2.findContours(roi_erode.copy(), cv2.RETR_CCOMP, cv2.CHAIN_APPROX_NONE)

    roi_bg = roi_tmp.copy()
    roi_bw_tmp = roi_tmp.copy()

    #Construct ROI with only contour condidates


    #Filter big contours (connected borders)
    for i in range(0, len(contours)):
        cnt = contours[i]
        area = cv2.contourArea(cnt)
        x,y,w,h = cv2.boundingRect(cnt)
        
        #print(hierachy[0][i][],end=' ')
        #Skip if has children Next, Previous, First_Child, Parent
        #if hierachy[0][i][2] != -1:
        #    continue
        
        #Erase answer text from background
        if 200 < area < 100000 :
            #Erase contour in binary images
            cv2.drawContours(roi_bg,[cnt],0,255,-1)
            cv2.drawContours(roi_bw_tmp,[cnt],0,255,-1)
            cv2.drawContours(drawing,[cnt],0,(0,0,255),1)
            #cv2.drawContours(maskShow,[cnt],0,255,-10)
            #print('.',end='')

    #cv2.imwrite('drawing_bw.png',roi_bw)

    #Substract background
    roi_bw_minus_bg = cv2.subtract(roi_bw_tmp,roi_tmp)

    #Invert result
    roi_bw_minus_bg = cv2.bitwise_not(roi_bw_minus_bg)

    #Clean substracted image from erode image contours
    for i in range(0, len(contours)):
        cnt = contours[i]
        area = cv2.contourArea(cnt)
        x,y,w,h = cv2.boundingRect(cnt)
        
        #Erase small regions in subtracted inverted image
        if area < 200:
            cv2.drawContours(roi_bw_minus_bg,[cnt],0,255,-1)

    #imshow(roi_bw_tmp, cmap='gray')

    #Replace current bw image
    roi_bw_answer = roi_bw_minus_bg


    ##########################
    # Detect answers
    ##########################

    print('Detecting answers...')

    picklePath = outputFolder+mapID+os.sep+mapID+'a.pickle'
    if not os.path.isfile(picklePath):

        letList = []
        minArea = 1000 #int((imgWidth*imgHeight)*0.00001)
        print('\tMin area =',minArea,'px')

        #Detect connected-components
        contours, hierachy = cv2.findContours(roi_bw_answer.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        drawing_c = roi.copy()
        drawing_b = cv2.cvtColor(roi_bw_answer.copy(), cv2.COLOR_GRAY2BGR)

        n = 0
        hList = []

        #Filter contours (convexity + content)
        for i in range(0, len(contours)):
            cnt = contours[i]
            x,y,w,h = cv2.boundingRect(cnt)    
            
            #Skip too big contours
            if h > imgHeight/10:
                continue
            
            #ANSWERS
            
            #Small contours (or add them to phonetic text/letter list)
            area = cv2.contourArea(cnt)
            if area < minArea:
                if area > 100:
                    #Draw TMP answer contour in green
                    cv2.drawContours(drawing_b,[cnt],0,(0,255,0),1)
                    cv2.drawContours(drawing_c,[cnt],0,(0,255,0),1)
                    letList.append(ROI(str(i),cnt))
                continue
                        
        print('\n\t',len(letList),'letter candidates found within',len(contours),'CC')
        #print('median height =', numpy.median(hList))
        #imshow(drawing_b)

        #Save on HDD
        #cv2.imwrite('d_letter_BW.png',drawing_b)
        #cv2.imwrite('d_letter_color.png',drawing_c)

        #################################
        # Compute answer text blocks
        #################################

        #Compute answer text block candidates

        phoList = []
        #Init mask
        maskTmp = numpy.zeros([imgHeight,imgWidth], numpy.uint8)
        drawing_n = drawing_c.copy()

        #Draw letter contours
        for l in letList:
            #cv2.drawContours(mask,[letter.contour],0,255,-1)
            x,y,w,h = l.BdB
            #Offsets
            ox = int(w)
            oy = int(h/10)
            
            #Draw with automatic and manual offsets (2021-02-25: add -10 top offset to reduce upper diacritic cuts)
            cv2.rectangle(maskTmp,(x-ox, y-oy-5),(x+w+ox, y+h+oy),255,-1)
            
        #Erase department ROI
        for d in depList:
            x,y,w,h = d.BdB
            cv2.rectangle(maskTmp,(x, y),(x+w, y+h),0,-1)

        #Detect/count contours
        contourTmp, hierachy = cv2.findContours(maskTmp.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        #Create answer ROI object list
        for i,cnt in enumerate(contourTmp):
            #Create a phonetic block
            ID = f'{i:04}'
            p = ROI(str(ID),cnt)
            p.type = 'a'
            phoList.append(p)
            
            #Draw in blue
            cv2.drawContours(drawing_n,[cnt],0,(255,0,0),1)
            
            #Debug
            #if i > 100:
            #    break
            #else:
            #    print(i,p.BdB,p.y,p.h, p.x, p.w)
            #    x,y,w,h
            
        #imshow(maskTmp,cmap='gray')
        print('\t',len(phoList),' blocks/CC')

        #Save
        #cv2.imwrite('block_mask.png',maskTmp)
        #cv2.imwrite('block_color.png',drawing_n)

        ##############################
        # Recognize answers
        ##############################

        print('Recognizing answers...')

        #Transcribe all phonetic text (2021-03-05: still usefull?)

        #Create and empty dir
        path = 'temp/'+mapID+'/'

        if os.path.isdir(path):
            shutil.rmtree(path, ignore_errors=True)
        try:
            os.makedirs(path)
        except OSError:
            os.remove(path)
            print ("\tDirectory creation %s failed" % path)
        else:
            print ("\tSuccessfully created the directory %s" % path)
            
        for i,p in enumerate(phoList):
            filename = path+p.id+'.tif'
            #2020-02-19 switch from color to bw (cleaned) image for OCR
            cv2.imwrite(filename, roi_bw_answer[p.y:p.y+p.h, p.x:p.x+p.w])
            #stdout = subprocess.check_output(["/usr/bin/tesseract", "temp/44.tif" , "output", "-l", "fra", "--dpi", "300" "--psm", "5"], stderr=subprocess.STDOUT)#, "-psm", "5"])
            #print(stdout)
            
            #TODO: replace by Tesseracr? Faster but v4.0.1?
            #Text recognition using Tesseract v5.0.0-alpha
            #!/usr/bin/tesseract $filename output --dpi 500 -l fra --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot_fra
            subprocess.run(["/usr/bin/tesseract",filename,mapID+"_output","--dpi", "500", "-l", "fra", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"])

            with io.open(mapID+'_output.txt', 'r') as f:
                trans = f.read()
            p.transcription = trans.replace('\n','')#[:-1]
            
            if i%100 == 0:
                print("******************\n",i,'/',len(phoList),"\n******************")


        ##################################
        # Association answer <> points
        ##################################


        def euclidean(x,y):
            """
                Compute Euclidean distance between two pixels from a 2D image

                return (float)
            """

            sumSq=0.0

            #add up the squared differences
            for i in range(len(x)):
                sumSq+=(x[i]-y[i])**2

            #take the square root of the result
            return (sumSq**0.5)

        drawing_l = drawing_n.copy()

        #for a in phoList:
        for p in pointList:
            minDist = numpy.inf
            bestA = None
            #print(a.cx,a.cy)
            maxDist = int(p.h*5) #500?
            
            #Debig
            if maxDist > 500:
                print('WARNING: maxDist=',maxDist)
            
            #for p in pointList:
            for a in phoList:
                dist = euclidean((a.cx,a.cy),(p.cx,p.cy))
                #print(' ',p.cx,p.cy)
                if dist < minDist and dist < maxDist:
                    minDist = dist
                    bestA = a
            
            #Link one answer to a points
            if bestA is not None:
                p.links.append(bestA)
                print('add a',bestA.id,'to p',a.id)

                #Draw point box (may overwrite but who cares?)
                cv2.rectangle(drawing_l,(int(p.x),int(p.y)), (int(p.x+p.w),int(p.y+p.h)),(0,0,255),1)

                #Draw link line
                cv2.line(drawing_l, (p.cx,p.cy), (bestA.cx,bestA.cy), (0,255,255), 2)

                if p not in a.links:
                    print(' add p',p.id,'to a',bestA.id)
                    bestA.links.append(p)

                #print('anwser',a.id,'<> point',a.links[0].id,'(',a.links.transcription,')')
                
            else:
                #Draw point box in a different color
                cv2.rectangle(drawing_l,(int(p.x),int(p.y)), (int(p.x+p.w),int(p.y+p.h)),(150,150,150),1)


        #Serialize answers
        with open(picklePath, 'wb') as f:
            pickle.dump(phoList, f)
        print('Serialize survey points into',picklePath)


        #Serialize points (overwrite)
        with open(picklePath.replace('a.p','p.p'), 'wb') as f:
            pickle.dump(pointList, f)
        print('Serialize (overwrite) survey points into',picklePath.replace('a.p','p.p'))


    else:
        with open(picklePath, 'rb') as f:
            phoList = pickle.load(f)
            print('Load ',len(phoList),' answers from',picklePath)



    ##############################################
    # Generate thumbnails (for HTML export)
    ##############################################

    exportTIF = True
    exportFolder = resultFolder+mapID+'/'
    n = 0
    m = 0
    o = 0

    print('Generating thumbnails for later HTML export...')

    if exportTIF:
        try:
            os.makedirs(exportFolder)
        except OSError:
            print ("\tDirectory %s already exist" % exportFolder)
        else:
            print ("\tSuccessfully created the directory %s" % exportFolder)

    #TODO: remove >=3 limit
    for p in phoList:
        if len(p.transcription.replace(' ','')) >= 3 or p.transcription.isdigit():
            x,y,w,h = p.BdB
            #plt.imshow(roi_bw_answer[y:y+h,x:x+w],cmap='gray')
            #plt.show()
            #print(p.transcription)
            n += 1
            
            if exportTIF:
                m = int(p.id)
                filename = exportFolder+f'{m:04}'+'.tif'
                cv2.imwrite(filename, roi[p.y:p.y+p.h, p.x:p.x+p.w])
                filename = exportFolder+f'{m:04}'+'.jpg'
                cv2.imwrite(filename, roi[p.y:p.y+p.h, p.x:p.x+p.w])
                filename = exportFolder+f'{m:04}'+'b.jpg'
                cv2.imwrite(filename, roi_bw_answer[p.y:p.y+p.h, p.x:p.x+p.w])
                
    for p in pointList:
        x,y,w,h = p.BdB
        #plt.imshow(roi[y:y+h,x:x+w],cmap='gray')
        #plt.show()
        #print(p.transcription)
        m += 1
        if exportTIF:
            m = int(p.id)
            #filename = exportFolder+f'{m:04}'+'.tif'
            #cv2.imwrite(filename, roi_bw[p.y:p.y+p.h, p.x:p.x+p.w])
            filename = exportFolder+'p'+f'{m:04}'+'.jpg'
            cv2.imwrite(filename, roi[p.y:p.y+p.h, p.x:p.x+p.w])
            filename = exportFolder+'p'+f'{m:04}'+'b.jpg'
            cv2.imwrite(filename, roi_bw[p.y:p.y+p.h, p.x:p.x+p.w])

    for p in depList:
        x,y,w,h = p.BdB
        #plt.imshow(roi[y:y+h,x:x+w],cmap='gray')
        #plt.show()
        #print(p.transcription,'(',p.id,'.tif )')
        o += 1
        if exportTIF:
            m = int(p.id)
            #filename = exportFolder+f'{m:04}'+'.tif'
            #cv2.imwrite(filename, roi_bw[p.y:p.y+p.h, p.x:p.x+p.w])
            filename = exportFolder+'d'+f'{m:04}'+'.jpg'
            cv2.imwrite(filename, roi[p.y:p.y+p.h, p.x:p.x+p.w])
            filename = exportFolder+'d'+f'{m:04}'+'b.jpg'
            cv2.imwrite(filename, roi_bw[p.y:p.y+p.h, p.x:p.x+p.w])

    print('\t',n,' word found (>5 characters),',m,'points,',o,'dept')



    ############################################
    # Load convert API <> Rousselot functions
    ############################################

    print('Load API<>Rousselot conversion table...')


    #Load conversion table
    csvFileAPI = 'convertion_API_Rousselot/conversion API-Rousselot6.csv' #Conversion Rousselot-API-ECLATS-voyelles.csv'
    dic = collections.OrderedDict() #{}

    with open(csvFileAPI, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for i,row in enumerate(spamreader):
            if i == 0:
                print('\t',row)
            else:
                #print(row[13]+'.'+row[19])
                #API key, Rousselor value
                #dic[row[19]] = row[13]
                if len(row[0]) > 0:
                    dic[row[0]] = row[2]
                
                #src = imgFile+row[11].split('/')[-1]

    print('\t',len(dic),'convertible symbol loaded')

    #Order by API lenght
    listofTuples = list(reversed(sorted(dic.items() ,  key=lambda x: len (x[0]))))


    ###################################
    # Generate OCR output
    ###################################

    print('Generating OCR output...')

    #inPath = 'temp/'+mapID+'/'
    outFolder = resultFolder+mapID+'/'
    n = 0
    nbFile = 0

    #Create output directory if not exist (should be cleared manually)
    if not os.path.isdir(outFolder):
        os.mkdir(outFolder)

    #Count file number
    for f in os.listdir(outFolder):
        if f.endswith('.tif') and not f[0] in ['p','d']:
            nbFile += 1
        
    for f in os.listdir(outFolder):
        
        #Skip not tif or points image
        if not f.endswith('.tif') or f[0] in ['p','d']:
            continue
        
        filename = outFolder+f
        outPath = outFolder+f.split('.')[0]
        
        #Base
        #output =  outPath+'_base'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l eng --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #!/usr/bin/tesseract $filename $output --dpi 500 -l lat --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #!cat results/img/793_base.txt
        
        #OCR1 - fra
        output =  outPath+'_ocr1'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l fra --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot_fra
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "fra", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot_fra"])
        
        #OCR2 - rou13
        output =  outPath+'_ocr2'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l rou13 --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "rou13", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])
        
        #OCR3 - rou16500
        output =  outPath+'_ocr3'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l rou16500000 --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "rou16500000", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])
        
        #OCR4 - rou18999r
        output =  outPath+'_ocr4'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l rou18999r --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "rou18999r", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])

        #OCR5 - ans1100
        output =  outPath+'_ocr5'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l ans1100 --psm 13 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "ans1100", "--psm", "13", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])
        
        #OCR6 - ans50000
        output =  outPath+'_ocr6'
        #!/usr/bin/tesseract $filename $output --dpi 500 -l ans50000 --psm 13 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
        #subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "ans50000", "--psm", "13", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])
        
        #OCR7 - rou19999r
        output =  outPath+'_ocr7'
        if not os.path.isfile(output+'.txt'):
            #!/usr/bin/tesseract $filename $output --dpi 500 -l rou19999r --psm 7 --tessdata-dir /home/crigau02/tesstutorial/tesseract/tessdata/ rousselot
            subprocess.run(["/usr/bin/tesseract",filename, output,"--dpi", "500", "-l", "rou19999r", "--psm", "7", "--tessdata-dir", "/home/crigau02/tesstutorial/tesseract/tessdata/", "rousselot"])
        
        print(n,'/',nbFile-1)
        n += 1
      
    return depList, phoList, pointList  


def getNumList(imPath, pointList):
    """
        Compute survey points output and find missing ones
    """
    
    d = {}
    nFound = []
    nMiss = []
    
    for f in sorted(os.listdir(imPath)):
        
        #If strats by p and not ends with b (binary)
        if f[0] == 'p' and not f.endswith('b.jpg'):
            #continue
            #elif not f.endswith('.jpg'):
            #    continue
            
            #Retreive corresponding point object
            for p in pointList:
                if f[1:].split('.')[0] == str(p.id):
                    try:
                        d[f] = f'{int(p.transcription):04}'
                    except:
                        print('ERROR in transcription (must be a number?)"'+p.transcription+'"')
                        raise
                    if int(p.transcription) not in nFound:
                        nFound.append(int(p.transcription))

    #Sort by dep number (value)
    #TODO order using f'{m:04}' format
    sorted_tuples = sorted(d.items(), key=lambda item: item[1])
    #print(sorted_tuples)  # [(1, 1), (3, 4), (2, 9)]
    sorted_dict = {k: v for k, v in sorted_tuples}

    #Check missed
    for i in range(1,1000):
        if i not in nFound:
            nMiss.append(i)
            
    return sorted_dict, len(d), nMiss

def getRepList(imPath, nums, depts):
    """
        Get answer image list by removing num and dep images from global list
    """
    
    d = {}
    
    
    for f in sorted(os.listdir(imPath)):
        
        #Skip not jpg files
        if f.endswith('b.jpg'):
            continue
        elif not f.endswith('.jpg'):
            continue
            
        if f in nums or f in depts:
            continue
        else:
            #Get one of the OCR transcription for alphabetical ordering
            if os.path.isfile(imPath+f.split('.')[0]+'_ocr7.txt'):
                with open(imPath+f.split('.')[0]+'_ocr7.txt', 'r') as readFile:
                    s = readFile.read()
                d[f] = s[:-2]
            else:
                d[f] = ""
                #print("Skip rep",f)
            
    #Sort by value (alphabetically)
    sorted_tuples = sorted(d.items(), key=lambda item: item[1])
    #print(sorted_tuples)  # [(1, 1), (3, 4), (2, 9)]
    sorted_dict = {k: v for k, v in sorted_tuples}
    
    return sorted_dict, len(d)

def getDepList(imPath, depList):
    
    d = {}
    dList = ["Ain","Aisne","Allier","B.-Alpes","H.-Alpes","Alpes-Mar.","Ardèche","Arden.","Ariège","Aube","Aude","Aveyr.","B.-du-Rhône","Calvad.","Cantal","Charente","Char.-inf.","Cher","Corrèze","Côte-d'Or","Côtes-du-N.","Creuse","Dordogne","Doubs","Drôme","Eure","Eure-et-L.","*Finistère","Gard","H.-Gar.","Gers","Gironde","Hérault","Ille-et-V.","Indre","Indre-et-L.","Isère","Jura","Landes","Loir-et-C.","Loire","H.-Loire","Loire-Inf.","Loiret","Lot","Lot-et-G.","Lozère","Maine-et-L.","Manche","Marne","H.-Marne","Mayenne","Meurthe-et-M.","Meuse","Morbihan","*Moselle","Nièvre","Nord","Oise","Orne","Pas-de-C.","Puy-de-D.","B.-Pyren.","H.-Pyrén.","Pyrén.-Or.","Rhône","H.-Saône","Saône-et-L.","Sarthe","Savoie","H.-Savoie","Seine-Inf.","S.-et-Marne","D.-Sèvres","Somme","Tarn","Tarn-et-G.","Var","Vaucluse","Vendée","Vienne","H.-Vienne","Vosges","Yonne","S.-et-Oise","Seine","S.-et-Marne"]
    dFound = []
    dMissed = []
    
    for f in sorted(os.listdir(imPath)):
        
        if f[0] == 'd' and not f.endswith('b.jpg'):

            #Retreive corresponding dep object
            for p in depList:
                if f[1:].split('.')[0] == p.id:
                    depName = p.transcription.replace('\n','').replace('','')

                    #Check FRA content for mode selection
                    #depName = ""
                    minDist = numpy.inf

                    #Find closest correspandance
                    corr = ""
                    for dep in dList:
                        #Format?
                        #dep = dep.replace('é','e').replace('è','e').replace('ô','o').upper()
                        dist = lev(depName, dep.upper())
                        if dist <= minDist:
                            corr = dep.upper()
                            minDist = dist
                    #Store
                    if len(depName.replace(' ','')) > 1:
                        #Add ? if quite doubtful
                        if minDist > len(depName)/2:
                            corr += "?"
                        d[f] = corr #depName
                        dFound.append(corr)
                    else:
                        d[f] = "?"
                        #print(f,'-> ?')
                    
                    #else:
                    #    print('getDepList: path not found',imPath+f.split('.')[0]+'_ocr1.txt')
                    #    break

            #Sort by dep number (value)
            sorted_tuples = sorted(d.items(), key=lambda item: item[1])
            #print(sorted_tuples)  # [(1, 1), (3, 4), (2, 9)]
            sorted_dict = {k: v for k, v in sorted_tuples}

    
    #Check missed
    for dep in dList:
        if dep.upper() not in dFound:
            dMissed.append(dep)

    return sorted_dict, len(d), dMissed

def generateRows(imPath, reps, nums, depts, phoList, pointList, outputFolder):  
    """
        Generate HTML table rows content
    """
    
    n=0
    nbLink=0
    rows = ""
    foundGTMap = ""
    fullGT = ""
    fullTR = ""
    
    #Load point names
    csvFile = "data/maps/ALF Nom patois Loc.csv"
    locDic = {}
    with open(csvFile, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for i,row in enumerate(spamreader):
            if i > 0:
                locDic[row[0]] = row[2]+' ('+row[3]+')'
    
    #1) Add survey points rows (nums)
    s1List = []
    s2List = []
    s3List = []
    s4List = []
    s5List = []
    s6List = []
    csvList = []
    csvDone = []
    for k in nums:
        #2021-02-25 Filename = dic key
        f = k
        pLink = None
        
        #Retrieve corresponding survey point ROI object
        for p in pointList:
            if f.split('.')[0] == 'p'+str(p.id):
                px = p.cx
                py = p.cy
                pLink = p.links
                break
                    
        rows = rows+'<tr>\n'
               
        #Add image ID and link cell
        rows = rows+'\t<td id="'+f.split('.')[0]+'">'+f.split('.')[0]+'</td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f+'" alt="'+f+'"></td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f.split('.')[0]+'b.'+f.split('.')[1]+'" alt="'+f.split('.')[0]+'b.'+f.split('.')[1]+'"></td>\n'
        
        #Add survey point transcription cell
        rows = rows+'\t<td>'+nums[k].lstrip('0')+'</td>\n' #point number
        try:
            rows = rows+'\t<td>'+locDic[nums[k].lstrip('0')]+'</td>\n' #point names
        except KeyError:
            try:
                rows = rows+'\t<td>'+nums[k].lstrip('0').replace('4','1')+'? '+locDic[nums[k].lstrip('0').replace('4','1')]+'</td>\n' #point names
            except KeyError:
                try:
                    rows = rows+'\t<td>'+nums[k].lstrip('0').replace('1','4')+'? '+locDic[nums[k].lstrip('0').replace('1','4')]+'</td>\n' #point names
                except:
                    rows = rows+'\t<td>'+nums[k].lstrip('0')+'?</td>\n' #point names
                    print(nums[k].lstrip('0')+'?',end='')
        
        #Display associated answer (should be only only one if perfect results)
        if pLink is not None:
            if len(pLink) == 1:
            #for pl in pLink:
                pl = pLink[0]
                rows = rows+'\t<td><a href="#'+str(pl.id)+'"><img src="'+imPath.split(os.sep)[1]+os.sep+str(pl.id)+'.jpg" alt="'+str(pl.id)+'"'+'/></a></td>\n'
            elif len(pLink) > 1:
                print('WARNING: more than one link found for',pLink)
                raise
                
            #If single correspondance found
            if len(pLink) == 1:
                #Retrieve and add GT cell
                gt = ""
                gt_retro = ""
                for i in range(1,13):
                    csvFile = 'convertion_API_Rousselot/données Symila '+str(i)+'.csv'
                    with open(csvFile, newline='') as csvfile:
                        spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                        for o,row in enumerate(spamreader):
                            #Search corresponding map
                            for l in [1,3,5,7,9,11,13]:
                                if l < len(row):
                                    if str(mapID.lstrip('0')) == row[l]:#, row[3], row[5], row[7], row[9]):
                                        #Seach corresponding point line
                                        if str(nums[k].lstrip('0')) == row[0]:
                                            #print("GT point ID in map",mapID,'in',csvFile)
                                            print('*',end='')
                                            for j in range(1,11):
                                                try:
                                                    if row[j] == mapID.lstrip('0'):
                                                        foundGTMap = str(i)
                                                        gt = row[j+1]
                                                            
                                                        #Add GT to a new cell
                                                        try:
                                                            #Retro convert (REQUIRE 3.3 Convert API to be loaded first)
                                                            if mapID == "1334":
                                                                with open('convertion_API_Rousselot/GT-1334.csv', newline='') as csvfile:
                                                                    spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
                                                                    for i,row in enumerate(spamreader):
                                                                        if row[2] == nums[k].lstrip('0'):
                                                                            gt_retro = row[0]
                                                                            rows = rows+'\t<td>'+gt_retro+' (GT)</td>\n' #corresponding GT text
                                                                        #else:
                                                                        #    print(row[2],'!=',pl.id.lstrip('0'))
                                                            else:
                                                                gt_retro, ok, nok = convert(gt, listofTuples)
                                                                rows = rows+'\t<td>'+gt_retro+' (GT retro)</td>\n' #corresponding GT text
                                                                #print(gt_retro)
                                                        except:
                                                            print('WARNING: API<>Rousselot correspondances not loaded')
                                                            rows = rows+'\t<td>'+gt+' (GT API)</td>\n' #corresponding GT text
                                                        break
                                                    
                                                except:
                                                    continue
                                                    
                if len(foundGTMap) > 0 and len(gt_retro) > 0:
                    #Display (best?) OCR output
                    if os.path.isfile(imPath+pl.id+'_ocr7.txt'):
                        #Best OCR
                        with open(imPath+pl.id+'_ocr7.txt', 'r') as readFile:
                            s = readFile.read()
                        s = s.replace('\n','').replace('','')
                        #Real OCR
                        with open(imPath+pl.id+'_ocr5.txt', 'r') as readFile:
                            s6 = readFile.read()
                        s6 = s6.replace('\n','').replace('','')
                        rows = rows+'\t<td>'+s+' / '+s6+'</td>\n' #corresponding GT text
                        
                        #Compute and display scores for last OCR
                        s_no_accent = unicodedata.normalize('NFKD', s).encode('ASCII', 'ignore')
                        gt_no_accent = unicodedata.normalize('NFKD', gt_retro).encode('ASCII', 'ignore')
                        s1 = 1 - numpy.around(lev(s_no_accent,gt_no_accent) / max(len(gt_no_accent),len(s_no_accent)), decimals=2)
                        s2 = 1 - numpy.around(lev(s,gt_retro) / max(len(gt_retro),len(s)), decimals=2)
                        s1List.append(s1)
                        s2List.append(s2)
                        
                        #Compute and display scores for new real OCR
                        s_no_accent = unicodedata.normalize('NFKD', s6).encode('ASCII', 'ignore')
                        gt_no_accent = unicodedata.normalize('NFKD', gt_retro).encode('ASCII', 'ignore')
                        s5n = 1 - numpy.around(lev(s_no_accent,gt_no_accent) / max(len(gt_no_accent),len(s_no_accent)), decimals=2)
                        s6n = 1 - numpy.around(lev(s6,gt_retro) / max(len(gt_retro),len(s6)), decimals=2)
                        s5List.append(s5n)
                        s6List.append(s6n)
                        
                        rows += '\t<td>'+str(s1)[:4]+' / '+str(s5n)[:4]+'</td>\n' #corresponding GT text no accent
                        rows += '\t<td>'+str(s2)[:4]+' / '+str(s6n)[:4]+'</td>\n' #corresponding GT text
                        
                        #Add to full string for transcribe-compare
                        fullGT += gt_retro+' '
                        fullTR += s+' '
                        
                        #Add to CSV export list
                        if nums[k].lstrip('0') not in csvDone:
                            csvList.append([str(nums[k].lstrip('0')),str(locDic[nums[k].lstrip('0')]),px,py,s6,gt,pl.x,pl.y,pl.w,pl.h])
                            
                            csvDone.append(nums[k].lstrip('0'))

                        #Other OCR (for comparison)
                        with open(imPath+pl.id+'_ocr2.txt', 'r') as readFile:
                            s = readFile.read()
                        s = s.replace('\n','').replace('','')
                        
                        #Compute and display scores for old OCR
                        s_no_accent = unicodedata.normalize('NFKD', s).encode('ASCII', 'ignore')
                        s3 = 1 - numpy.around(lev(s_no_accent,gt_no_accent) / max(len(gt_no_accent),len(s_no_accent)), decimals=2)
                        s4 = 1 - numpy.around(lev(s,gt_retro) / max(len(gt_retro),len(s)), decimals=2)
                        s3List.append(s3)
                        s4List.append(s4)                        
                        
                        nbLink+=1
                    #else:
                    #    print(imPath+pl.id+'_ocr7.txt')
                    
                    if exportGT:
                        #Copy to pseudo ground truth GT
                        try:
                            #Copy GT image
                            copyfile('results'+os.sep+imPath.split(os.sep)[1]+os.sep+str(pl.id)+'.tif','groundtruth/answers/input/'+mapID+'_'+f.split('.')[0]+'.tif')
                            #Copy GT text
                            with io.open('groundtruth/answers/input/'+mapID+'_'+f.split('.')[0]+'.gt.txt', 'w') as f0:
                                f0.write(gt_retro)
                        except:
                            print('GT image not found for',imPath.split(os.sep)[1]+os.sep+str(pl.id)+'.jpg (or text retro error)')
                        #cv2.imwrite('groundtruth/anwsers/input/'+str(nbLink)+'.png',roi[y:y+h,x:x+w])
                        
                        #Copy GT image for Guylaine to verify
                        #csvFile = 'convertion_API_Rousselot/GT-MaPipe.csv'
                        #refDic = {}
                        #with open(csvFile, newline='') as csvfile:
                        #    spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
                        #    for o,row in enumerate(spamreader):
                        #        refDic[row[2]] = row[3]
                        #try:
                        #    copyfile('results'+os.sep+imPath.split(os.sep)[1]+os.sep+str(pl.id)+'.tif','groundtruth/answers/GBT/'+refDic[nums[k].lstrip('0')]+'_'+nums[k].lstrip('0')+'_'+mapID+'_'+f.split('.')[0]+'.tif')
                        #    #Copy GT text
                        #    with io.open('groundtruth/answers/GBT/'+refDic[nums[k].lstrip('0')]+'_'+nums[k].lstrip('0')+'_'+mapID+'_'+f.split('.')[0]+'.gt.txt', 'w') as f:
                        #        f.write(gt_retro)
                        #except:
                        #    print('GT IMG FOR GUYLAINE image not found for',imPath.split(os.sep)[1]+os.sep+str(pl.id)+'.jpg (or text retro error)')

                #Add to CSV export list
                if nums[k].lstrip('0') not in csvDone:

                    #Get OCR output
                    try:
                        with open(imPath+pl.id+'_ocr7.txt', 'r') as readFile:
                            s6 = readFile.read()
                        s6 = s6.replace('\n','').replace('','').replace(',','') #remove special character and comma (TOREMOVE)
                    except:
                        s6 = ""

                    #Generate CSV row
                    try:
                        csvList.append([str(nums[k].lstrip('0')),str(locDic[nums[k].lstrip('0')]),px,py,s6,gt,pl.x,pl.y,pl.w,pl.h])
                        csvDone.append(nums[k].lstrip('0'))
                    except:
                        print('-',end='')

        rows = rows+'</tr>\n'       
        print('.',end='')
        n+=1
        
    #Save full string for transcribe-compare
    #with io.open('groundtruth/answers/'+mapID+'_full.gt.txt', 'w') as f:
    #    f.write(fullGT)
    #with io.open('groundtruth/answers/'+mapID+'_full.tr.txt', 'w') as f:
    #    f.write(fullTR)
        
    #Export as CSV for CartoDialect
    with open(outputFolder+mapID+os.sep+mapID+'.csv', 'w') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter=';')#, quotechar='|', quoting=csv.QUOTE_MINIMAL)
        spamwriter.writerow(["n°","Nom","px","py","Rousselot","API","rx","ry","rw","rh"])
        for v in csvList:
            spamwriter.writerow(v)
    
    #Print total score nums
    rows += """
            <tr class="title">
                <td>SCORES</td>
                <td>----</td>
                <td>----</td>
                <td>----</td>
                <td>----</td>
                <td>----</td>
                <td>"""+str(nbLink)+'/'+str(len(nums))+""" points</td>
                <td>----</td>
                <td> """+str(numpy.around(numpy.mean(s1List), decimals=2))+' / '+str(numpy.around(numpy.mean(s5List), decimals=2))+' ('+str(numpy.around(numpy.mean(s3List), decimals=2))+')'+"""</td>
                <td> """+str(numpy.around(numpy.mean(s2List), decimals=2))+' / '+str(numpy.around(numpy.mean(s6List), decimals=2))+' ('+str(numpy.around(numpy.mean(s4List), decimals=2))+')'+"""</td>
            </tr>
    """
    
    print('\nAdding reps...')
    
    #2) Add reps/ans rows
    #for f in sorted(os.listdir(imPath)):
    for k in reps:
        #2021-02-25 Filename = dic key
        f = k
        pLink = None
        #Retrieve corresponding ROI object
        for p in phoList:
            if f.split('.')[0] == str(p.id):
                pLink = p.links
                break
            #else:
            #    print(f.split('.')[0],'!=', str(p.id))
            
        rows = rows+'<tr>\n'
        
        #Image ID and link
        if pLink is not None and len(pLink)>0:
            rows = rows+'\t<td id="'+f.split('.')[0]+'">'+f.split('.')[0]+'<br /><a href="#p'+str(pLink[0].id)+'" title="'+str(pLink[0].transcription.lstrip("0"))+'">p'+str(pLink[0].id)+'</a></td>\n'
        else:
            rows = rows+'\t<td id="'+f.split('.')[0]+'">'+f.split('.')[0]+'<br /><a href="#p" title="">p</a></td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f+'" alt="'+f+'"></td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f.split('.')[0]+'b.'+f.split('.')[1]+'" alt="'+f.split('.')[0]+'b.'+f.split('.')[1]+'"></td>\n'
        
        #Base - eng
        #if os.path.isfile(imPath+f.split('.')[0]+'_base.txt'):
        #    with open(imPath+f.split('.')[0]+'_base.txt', 'r') as readFile:
        #        s = readFile.read()
        #    rows = rows+'\t<td>'+s[:-2]+'</td>\n'
        #else:
        #    rows = rows+'\t<td></td>\n'
        
        #print('Searching OCR file...')
        #OCR 1-7
        for i in range(7,0,-1):    
            if os.path.isfile(imPath+f.split('.')[0]+'_ocr'+str(i)+'.txt'):
                with open(imPath+f.split('.')[0]+'_ocr'+str(i)+'.txt', 'r') as readFile:
                    s = readFile.read()
                #if i == 1:                    
                #    s = unicodedata.normalize('NFKD', s[:-2]).encode('ASCII', 'ignore')
                #    rows = rows+'\t<td>'+str(s)+'</td>\n'
                #else:
                rows = rows+'\t<td title="ocr'+str(i)+'">'+s[:-2]+'</td>\n'
            else:
                rows = rows+'\t<td></td>\n'
        
        rows = rows+'</tr>\n'
        
        n+=1
        print('.',end='')

    print('\nAdding deps...')
    
    #Add deps rows
    for k in depts:
        #2021-02-25 Filename = dic key
        f = k
        
        rows = rows+'<tr>\n'
        #Image ID and link
        rows = rows+'\t<td>'+f.split('.')[0]+'</td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f+'" alt="'+f+'"></td>\n'
        rows = rows+'\t<td><img src="'+imPath.split(os.sep)[1]+os.sep+f.split('.')[0]+'b.'+f.split('.')[1]+'" alt="'+f.split('.')[0]+'b.'+f.split('.')[1]+'"></td>\n'
        
        #OCR 1 - fra
        #if os.path.isfile(imPath+f.split('.')[0]+'_ocr1.txt'):
        #    with open(imPath+f.split('.')[0]+'_ocr1.txt', 'r') as readFile:
        #        s = readFile.read()
        #rows = rows+'\t<td>'+s[:-2]+'</td>\n'
        #rows = rows+'\t<td>=></td>\n'
        rows = rows+'\t<td>'+depts[k]+'</td>\n'
        #else:
        #    rows = rows+'\t<td></td>\n'
        rows = rows+'</tr>\n'
        n+=1
        
        print('.',end='')
        
    if len(foundGTMap) > 0:
        print('\nGT map found in données Symila',foundGTMap,'.csv')
    else:
        print('\nGT map not found')

    return rows, n, nbLink


def export(mapID, mapFolder, outputFolder, resultFolder, depList, phoList, pointList):
    """
        Export as HTML and CSV
    """

    print('Exporting HTML + CSV files for map',mapID)

    if exportGT:
        print('Export GT =',exportGT)

    nums, nbNum, nMiss = getNumList(resultFolder+mapID+os.sep, pointList)
    depts, nbDep, dMiss = getDepList(resultFolder+mapID+os.sep, depList)
    reps, nbRep = getRepList(resultFolder+mapID+'/',nums, depts)
    #print(reps)

    rows, n, nbLink = generateRows(resultFolder+mapID+'/', reps, nums, depts, phoList, pointList, outputFolder)
                        
    html = """
    <!doctype html>
    <html lang="en">

        <head>
            <title>OCR """+mapID+"""</title>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="Author" content="Christophe Rigaud">
          
            <style>
                table { 
                  border: 1px solid DarkOrange;
                  border-radius: 13px; 
                  border-spacing: 0;
                }
                table th { 
                  border-bottom: 1px solid DarkOrange;
                }
                table th,
                table td{
                    padding: 5px; 
                    font-family: "Liberation Serif", Symbola, FreeSerif, Arial, Helvetica, sans-serif;
                    font-size: 20px;
                }
                table td:hover{
                    font-family: "Symbola", "Liberation Serif", FreeSerif, Arial, Helvetica, sans-serif;
                }
                table tr:last-child > td {
                  border-bottom: none;
                }
                tr:nth-child(even) {
                    background-color: #eee;
                }
                tr:nth-child(odd) {
                    background-color: #fff;
                }            
                tr td img{
                    width:200px;
                }
                .title{
                    font-weight: bold;
                    color: red;
                }
            </style>

        </head>
        
        <body>
        
          <table>
               <thead>
                <tr>
                    <th> ID </th>
                    <th> Image </th>
                    <th> Image N&B </th>
                    <th> Transc. OCR</th>
                    <th> Localité </th>
                    <th> Réponse </th>
                    <th> Rétrotranscrip. </th>
                    <th> Transc. OCR</th>
                    <th> 1-CER sans ac.</th>
                    <th> 1-CER avec ac.</th>
                </tr>
              </thead>
              <tbody>
    """+rows+"""

                
              </tbody>
              <tfoot>
                <tr>
                    <td>Total</td>
                    <td> """+str(n)+""" images</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
              </tfoot>
           </table>
            
        </body>

    </html>

    """

    with open(resultFolder+mapID+'.html', 'w') as outfile:
        outfile.write(html)# and any(map(lambda v: v in word, 'aeiou'))))
        
    print('Found',nbLink,'links from',int((nbRep/(638))*100),'% ans.,',int((nbDep/(nbDep+len(dMiss)))*100),'% dep.,',int((nbNum/(nbNum+len(nMiss)))*100),'% num.','(=',n,'elements)')
    print('Unfound dep. :',dMiss)
    #print('Unfound num. :',nMiss) #WARNING: only check within range 0-999


if __name__ == "__main__":


    ################################################################
    # variables to set tasks performed

    mapID = "" # 0017 0026 0097 0318 0359 0859 1019 1334   0063
    exportGT = False


    inFolder = "/media/crigau02/My Passport/Datasets_input_output/CartoDialect/Cartes_ALF/" #"data/maps_batch/"
    resFolder = 'results_batch/'
    outFolder = 'output/'    

    ################################################################

    try:
        mapID = sys.argv[1]
    except:
        print('WRONG ARGS: python batch.py FOLDER')

    #Multi maps
    if mapID == "":
        for i,imageName in enumerate(sorted(os.listdir(inFolder))):
            
            #Skip
            if not 0 <= i < 500:
                print('SKIP',imageName)
                continue

            if imageName.split('.')[0][-1].isdigit():

                    mapID = imageName.split('.')[0][8:]
    
                    if not os.path.isdir(outFolder+mapID):

                        depList, phoList, pointList = process(mapID, inFolder, outFolder, resFolder)
                        if len(depList) > 0:
                            export(mapID, inFolder, outFolder, resFolder, depList, phoList, pointList)
                    else:
                        print('DIR already exist',outFolder,mapID)

            else:
                print('SKIP',imageName)

    #Single map
    else:
        depList, phoList, pointList = process(mapID, inFolder, outFolder, resFolder)
        if len(depList) > 0:
            export(mapID, inFolder, outFolder, resFolder, depList, phoList, pointList)
