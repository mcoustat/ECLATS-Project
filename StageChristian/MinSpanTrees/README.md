# Sujet de stage :
*Localisation, reconnaissance et extraction des données hétérogènes dans des documents cartographiques*

## Résumé du travail proposé :
L’objectif du stage est de savoir localiser, reconnaitre et extraire les différents types d’informations qui se trouvent dans l’Atlas linguistique de France. Ce travail sera un travail en lien avec une thèse effectuée au laboratoire L3i afin d’approfondir le travail commencé. Si la partie segmentation est terminée, le stagiaire pourra s’essayer à l’implémentation des méthodes de reconnaissance de l’état de l’art.

## Mots clés :
Traitement de l’image, Morphologie mathématique, Composantes connexes, Analyse de carte, Séparation texte/graphique, Atlas linguistique

## Tests :
Compiler :

g++ -DNDEBUG -O3 -I. main.cpp Hemst.h MaxTree.h MilenaTree.h MinTree.h Mmsdr.h Msdr.h Sdr.h Tree.h -o Clustering

Exécuter :

* **Hemst :**

Trois paramètres :

* la méthode souhaitée, ici hemst
* le chemin relatif de l'image au format pgm
* le nombre de clusters souhaités

Exemples :

./Clustering hemst img/input/test1.pgm 3

./Clustering hemst img/input/test2.pgm 2

./Clustering hemst img/input/test3.pgm 2

* **Msdr et Mmsdr :**

Deux paramètres :

* la méthode souhaitée, msdr ou mmsdr
* le chemin relatif de l'image au format pgm

Exemples :

./Clustering msdr img/input/test1.pgm

./Clustering mmsdr img/input/test1.pgm

## Ouputs :

Les arbres sont visibles dans la console, les images correspondant aux différents clusters sont écrites dans le dossier img/output/
