#ifndef CLUSTERING_MIN_TREE_H
#define CLUSTERING_MIN_TREE_H

/*!
 * \file MinTree.h
 * \brief Implementation of a MinTree rooted in the lightest pixel and for leaves the darker pixels.
 * \author Jordan Drapeau
 * \version 0.1
 */

#include <memory>

#include "MilenaTree.h"

namespace mln::my {
    std::vector<point2d> sort_decreasingly(const image2d <value::int_u8> &u) {
        // h
        std::vector<unsigned> h(256, 0);
        mln_piter(box2d) p(u.domain());
        for_all(p) ++h[u(p)];

        // index
        std::vector<unsigned> index(256);
        index[255] = 0;
        for (int i = 254; i >= 0; --i) {
            index[i] = index[i + 1] + h[i + 1];
        }

        // S
        std::vector<point2d> S(u.nsites());
        for_all(p) S[index[u(p)]++] = p;

        return S;
    }

    MilenaTree MinTree(const image2d <value::int_u8> &u) {
        MilenaTree t(u);
        t.S = sort_decreasingly(u);

        union_find(t);
        t.canonicalize();

        return t;
    }

    MilenaTree MinTree(MilenaTree const &mT, std::vector<std::unique_ptr<mln::point2d>> const &s) {
        MilenaTree t(mT, s);
        return t;
    }
}  // mln::my

#endif  // CLUSTERING_MIN_TREE_H
