#ifndef CLUSTERING_MILENA_TREE_H
#define CLUSTERING_MILENA_TREE_H

/*!
 * \file MilenaTree.h
 * \brief Implementation of the mother class of MaxTree and MinTree.
 * \author Jordan Drapeau
 * \version 0.1
 */

#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/value/int_u8.hh>
#include <vector>

namespace mln::my {
    /*!
     * \struct MilenaTree
     * \brief Object Related Component Tree.
     */
    struct MilenaTree {
        image2d <value::int_u8> u;
        image2d <point2d> parent;
        std::vector<point2d> S;

        explicit MilenaTree(const image2d <value::int_u8> &u) : u(u), parent(u.domain()) {
        }

        MilenaTree(MilenaTree const &mT, std::vector<std::unique_ptr<mln::point2d>> const &s) : u(mT.u),
                                                                                                parent(mT.parent) {
            for (auto const &point : s) {
                S.push_back(*point);
            }
        }

        bool is_root(const point2d &p) const {
            return parent(p) == p;
        }

        bool is_representative(const point2d &p) const {
            return is_root(p) || u(parent(p)) != u(p);
        }

        box2d domain() const {
            return u.domain();
        }

        // canonicalize

        void canonicalize() {
            const unsigned N = S.size();
            for (unsigned i = 0; i < N; ++i) {
                point2d p = S[i];  // p goes from root to leaves
                point2d q = parent(p);

                // q has always been processed so parent(q) is a
                // representative node:
                if (!is_representative(parent(q))) {
                    std::abort();
                }

                // if p does not point towards a representative node then
                // skip the node above p (i.e., p points to q's parent,
                // which is a representative node)
                if (u(parent(q)) == u(q)) {
                    parent(p) = parent(q);
                }
            }
        }

        // back-propagate "repr -> points having this repr"
        template<typename A>
        void back_propagate(image2d <A> &a) const {
            const unsigned N = S.size();
            for (unsigned i = 0; i < N; ++i) {  // from root to leaves
                point2d p = S[i];
                if (!is_representative(p)) {
                    a(p) = a(parent(p));
                }
            }
        }

        // repr -> sub-tree
        template<typename A>
        void back_propagate_subtree(image2d <A> &a) const {
            const unsigned N = S.size();
            for (unsigned i = 1; i < N; ++i) {  // from root to leaves
                point2d p = S[i], q = parent(p);
                if (is_representative(p)) {
                    if (a(p) == 0 and a(q) != 0) {
                        a(p) = a(q);
                    }
                } else {
                    a(p) = a(q);
                }
            }
        }

    }; // end of struct tree

    point2d find_root(image2d <point2d> &zpar, point2d x) {
        // modify zpar
        if (zpar(x) == x) {
            return x;
        } else {
            zpar(x) = find_root(zpar, zpar(x));
            return zpar(x);
        }
    }

    void do_union(point2d const &p_, point2d const &r_, image2d <point2d> &zpar, image2d<unsigned> &rank,
                  image2d<unsigned> &last) {
        // modify zpar, rank, and last
        if (rank(p_) > rank(r_)) {
            // new root is p_
            zpar(r_) = p_;
            if (last(r_) < last(p_)) {
                last(p_) = last(r_);
            }
        } else {
            // new root is r_
            zpar(p_) = r_;
            if (last(p_) < last(r_)) {
                last(r_) = last(p_);
            }
            if (rank(p_) == rank(r_)) {
                rank(r_) = rank(r_) + 1;
            }
        }
    }

    void union_find(MilenaTree &t) {
        const std::vector<point2d> &S = t.S;
        const image2d<value::int_u8> &u = t.u;

        box2d D = t.domain();
        if (S.size() != D.nsites()) {
            std::abort();
        }

        image2d<point2d> &parent = t.parent;

        // auxiliary data
        point2d undef = D.pmin() - dpoint2d(1, 1);

        image2d<point2d> zpar(D);
        data::fill(zpar, undef);

        image2d<unsigned> rank(D), last(D);
        data::fill(rank, 0);

        // end of auxiliary data
        image2d<bool> deja_vu(D);
        data::fill(deja_vu, false);

        point2d p;
        mln_niter(neighb2d) n(c4(), p);

        for (int i = S.size() - 1; i >= 0; --i) {
            p = S[i];  // p goes from leaves to root
            parent(p) = p;
            zpar(p) = p;
            last(p) = i;
            for_all(n) if (D.has(n) && zpar(n) != undef) {
                    point2d p_ = find_root(zpar, p), r_ = find_root(zpar, n);
                    if (r_ != p_) {
                        point2d r = S[last(r_)];
                        parent(r) = p;
                        do_union(p_, r_, zpar, rank, last);
                    }
                }
            deja_vu(p) = true;
        }
    }
}  // mln::my

#endif  // CLUSTERING_MILENA_TREE_H
