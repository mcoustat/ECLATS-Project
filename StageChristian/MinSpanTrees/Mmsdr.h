#ifndef CLUSTERING_MMSDR_H
#define CLUSTERING_MMSDR_H

/*!
 * \file Mmsdr.h
 * \brief Implementation of the Modified Maximum Standard Deviation Reduction algorithm.
 * \author Christian Marquay
 * \version 0.1
 * \date March 14, 2019
 */

#include <memory>
#include <vector>

#include "MaxTree.h"
#include "MinTree.h"
#include "Sdr.h"
#include "Tree.h"

/*!
 * \class Mmsdr
 * \brief Class representing the Modified Maximum Standard Deviation Reduction algorithm.
 */
class Mmsdr : public Sdr {
public:
    std::vector<std::unique_ptr<Tree>> run(mln::my::MilenaTree &mT) const override {
        // Let sK be the set of disjoint subtrees of t0
        std::vector<std::unique_ptr<Tree>> sK;

        // Let t0 the emst constructed emst from s
        sK.push_back(std::make_unique<Tree>(mT));

        std::vector<double> deltaSigmaSK;
        std::vector<std::vector<unsigned int>> branchesToPrune;

        subdivide(branchesToPrune, deltaSigmaSK, *sK[0]);

        unsigned int k(largestJump(deltaSigmaSK));

        createClusters(k, branchesToPrune, sK);

        return sK;
    }

private:
    unsigned int largestJump(std::vector<double> const &deltaSigmaSK) const {
        unsigned int counter(2);
        unsigned int index(1);
        double max(deltaSigmaSK[0] - deltaSigmaSK[1]);
        while (counter < deltaSigmaSK.size()) {
            double jump(deltaSigmaSK[counter - 1] - deltaSigmaSK[counter]);
            if (jump > max) {
                index = counter;
                max = jump;
            }
            ++counter;
        }
        if (max < 0) {
            index = 0;
        }
        return index;
    }
};

#endif  // CLUSTERING_MMSDR_H
