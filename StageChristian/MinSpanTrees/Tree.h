#ifndef CLUSTERING_TREE_H
#define CLUSTERING_TREE_H

/*!
 * \file Tree.h
 * \brief Implementation of a Tree composed of a point, a gray level, as well as its parent and children.
 * \author Christian Marquay
 * \version 0.1
 * \date March 11, 2019
 */

#include <algorithm>
#include <cmath>
#include <iostream>
#include <memory>
#include <vector>

#include "MaxTree.h"
#include "MinTree.h"

/*!
 * \class Tree
 * \brief Class representing a tree.
 */
class Tree {
public:
    explicit Tree(mln::my::MilenaTree &mT, const mln::point2d &point) : m_parent(nullptr), m_point(nullptr),
                                                                        m_weight(-1) {
        m_point = std::make_unique<mln::point2d>(point);
        if (mT.parent(*m_point) != *m_point) {
            m_parent = std::make_unique<mln::point2d>(mT.parent(*m_point));
            int weight(mT.u(*m_parent) - mT.u(*m_point));
            if (weight < 0) {
                weight *= -1;
            }
            m_weight = weight;
        }
    }

    explicit Tree(mln::my::MilenaTree &mT) : m_parent(nullptr), m_point(nullptr), m_weight(-1) {
        std::vector<mln::point2d> copy(mT.S);
        m_point = std::make_unique<mln::point2d>(copy[0]);
        if (mT.parent(*m_point) != *m_point) {
            m_parent = std::make_unique<mln::point2d>(mT.parent(*m_point));
            int weight(mT.u(*m_parent) - mT.u(*m_point));
            if (weight < 0) {
                weight *= -1;
            }
            m_weight = weight;
        }
        copy.erase(copy.begin());
        unsigned int counter(0);
        while (counter < copy.size()) {
            if (append(mT, std::make_unique<Tree>(mT, copy[counter]))) {
                copy.erase(copy.begin() + counter);
            } else {
                ++counter;
            }
            if (counter == copy.size()) {
                counter = 0;
            }
        }
    }

    Tree(Tree const &arbreACopier) : m_parent(nullptr), m_point(nullptr), m_weight(-1) {
        m_point = std::make_unique<mln::point2d>(*(arbreACopier.m_point));
        if (arbreACopier.m_parent) {
            m_parent = std::make_unique<mln::point2d>(*(arbreACopier.m_parent));
            m_weight = arbreACopier.m_weight;
        }
        m_children.clear();
        for (auto const &tree : arbreACopier.m_children) {
            m_children.push_back(std::make_unique<Tree>(*tree));
        }
    }

    bool append(mln::my::MilenaTree &mT, const std::unique_ptr<Tree> &tree) {
        bool found(false);
        if (mT.parent(*(*tree).m_point) != *(*tree).m_point) {
            if (*m_point == mT.parent(*(*tree).m_point)) {
                m_children.push_back(std::make_unique<Tree>(*tree));
                found = true;
            } else {
                unsigned int counter(0);
                while (!found && counter < m_children.size()) {
                    found = m_children[counter]->append(mT, tree);
                    ++counter;
                }
            }
        }
        return found;
    }

    std::vector<std::unique_ptr<Tree>> asVector() {
        std::vector<std::unique_ptr<Tree>> tree;
        tree.push_back(std::make_unique<Tree>(*this));
        for (auto const &child : m_children) {
            std::vector<std::unique_ptr<Tree>> childrenTree = child->asVector();
            tree.insert(tree.end(), make_move_iterator(childrenTree.begin()), make_move_iterator(childrenTree.end()));
        }
        return tree;
    }

    std::unique_ptr<mln::point2d> getCentroid(unsigned long const totalSize, int const parentSize) const {
        if (m_children.empty()) {
            return std::make_unique<mln::point2d>(*m_point);
        }

        std::vector<int> childrenSizes;
        for (auto const &child : m_children) {
            childrenSizes.push_back(child->getSize());
        }
        long index(distance(childrenSizes.begin(), max_element(childrenSizes.begin(), childrenSizes.end())));
        int maxSize(std::max(1 + parentSize, childrenSizes[index]));

        if (maxSize < totalSize / 2) {
            return std::make_unique<mln::point2d>(*m_point);
        } else {
            return m_children[index]->getCentroid(totalSize, parentSize + 1);
        }
    }

    int getHeaviestWeight() const {
        int maxWeight;
        std::vector<unsigned int> w = weights();
        if (w.empty()) {
            maxWeight = -1;
        } else {
            maxWeight = w[distance(w.begin(), max_element(w.begin(), w.end()))];
        }
        return maxWeight;
    }

    double getMean() const {
        unsigned int sum(0);
        std::vector<unsigned int> w(weights());
        for (auto const &weight : w) {
            sum += weight;
        }
        return sum / (double) w.size();
    }

    std::unique_ptr<mln::point2d> getPoint() const {
        return std::make_unique<mln::point2d>(*m_point);
    }

    unsigned int getSize() const {
        unsigned int size(1);
        for (auto const &child : m_children) {
            size += child->getSize();
        }
        return size;
    }

    double getStd(double const wBarre) const {
        double sum(0.0);
        std::vector<unsigned int> w = weights();
        for (auto const &weight : w) {
            sum += (weight - wBarre) * (weight - wBarre);
        }
        return sqrt(sum / w.size());
    }

    bool isLeave() const {
        return m_children.empty();
    }

    std::unique_ptr<Tree> removeBranch(double const threshold) {
        unsigned int counter(0);
        std::unique_ptr<Tree> pruned(nullptr);
        while (!pruned && counter < m_children.size()) {
            if (m_children[counter]->m_weight == threshold) {
                pruned = std::make_unique<Tree>(*m_children[counter]);
                pruned->m_weight = -1;
                m_children.erase(m_children.begin() + counter);
            }
            ++counter;
        }
        counter = 0;
        while (!pruned && counter < m_children.size()) {
            if (!m_children[counter]->isLeave()) {
                pruned = m_children[counter]->removeBranch(threshold);
            }
            ++counter;
        }
        return pruned;
    }

    std::vector<std::unique_ptr<Tree>> removeTooHeavyBranches(double const threshold) {
        std::vector<std::unique_ptr<Tree>> pruned;
        unsigned int counter(0);
        while (counter < m_children.size()) {
            if (!m_children[counter]->isLeave()) {
                std::vector<std::unique_ptr<Tree>> prunedChildren = m_children[counter]->removeTooHeavyBranches(
                        threshold);
                pruned.insert(pruned.end(), make_move_iterator(prunedChildren.begin()),
                              make_move_iterator(prunedChildren.end()));
            }
            if (m_children[counter]->m_weight > threshold) {
                m_children[counter]->m_weight = -1;
                pruned.push_back(std::make_unique<Tree>(*m_children[counter]));
                m_children.erase(m_children.begin() + counter);
            } else {
                ++counter;
            }
        }
        return pruned;
    }

    std::vector<unsigned int> weights() const {
        std::vector<unsigned int> weights;
        if (m_weight >= 0) {
            weights.push_back(static_cast<unsigned int>(m_weight));
        }
        for (auto const &child : m_children) {
            std::vector<unsigned int> childWeights = child->weights();
            if (!childWeights.empty()) {
                weights.insert(weights.end(), childWeights.begin(), childWeights.end());
            }
        }
        return weights;
    }

    Tree &operator=(Tree const &arbreACopier) {
        if (this != &arbreACopier) {  // On vérifie que l'objet n'est pas le même que celui reçu en argument
            if (arbreACopier.m_point) {  // On copie tous les champs
                m_point.reset(nullptr);
                m_point = std::make_unique<mln::point2d>(*(arbreACopier.m_point));
                if (arbreACopier.m_parent) {
                    m_parent = std::make_unique<mln::point2d>(*(arbreACopier.m_parent));
                    m_weight = arbreACopier.m_weight;
                }
                m_children.clear();
                for (auto const &tree : arbreACopier.m_children) {
                    m_children.push_back(std::make_unique<Tree>(*tree));
                }
            }
        }
        return *this;  // On renvoie l'objet lui-même
    }

private:
    void display(unsigned int tab, std::ostream &flux) const {
        for (unsigned int i(0); i < 4 * tab; ++i) {
            flux << " ";
        }
        flux << "- " << *m_point << " de poids " << m_weight << std::endl;
        for (auto const &child : m_children) {
            child->display(tab + 1, flux);
        }
    }

    std::vector<std::unique_ptr<Tree>> m_children;
    std::unique_ptr<mln::point2d> m_parent;
    std::unique_ptr<mln::point2d> m_point;
    int m_weight;

    friend std::ostream &operator<<(std::ostream &flux, Tree const &tree) {
        tree.display(0, flux);
        return flux;
    }
};

#endif  // CLUSTERING_TREE_H
