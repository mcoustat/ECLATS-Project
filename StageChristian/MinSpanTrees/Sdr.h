#ifndef CLUSTERING_SDR_H
#define CLUSTERING_SDR_H

/*!
 * \file Sdr.h
 * \brief Implementation of the mother class of Maximum Standard Deviation Reduction and Modified Maximum Standard Deviation Reduction algorithms.
 * \author Christian Marquay
 * \version 0.1
 * \date March 14, 2019
 */

#include <cmath>
#include <memory>
#include <numeric>
#include <vector>

#include "MaxTree.h"
#include "MinTree.h"
#include "Tree.h"

/*!
 * \class Sdr
 * \brief Class grouping methods common to Maximum Standard Deviation Reduction and Modified Maximum Standard Deviation Reduction algorithm.
 */
class Sdr {
public:
    virtual std::vector<std::unique_ptr<Tree>> run(mln::my::MilenaTree &mT) const = 0;

protected:
    void createClusters(unsigned int const k, std::vector<std::vector<unsigned int>> const &branchesToPrune,
                        std::vector<std::unique_ptr<Tree>> &sK) const {
        unsigned int counter(0);
        while (counter < k) {
            std::unique_ptr<Tree> subTree(sK[branchesToPrune[counter][0]]->removeBranch(branchesToPrune[counter][1]));
            if (subTree) {
                // T' is the new disjoint subtree
                sK.push_back(move(subTree));
            } else {
                std::cerr << "ERREUR : diminution de l'écart-type maximale par suppression d'une branche inconnue."
                          << std::endl;
                abort();
            }
            ++counter;
        }
    }

    void subdivide(std::vector<std::vector<unsigned int>> &branchesToPrune, std::vector<double> &deltaSigmaSK,
                   const Tree &t0) const {
        std::vector<std::unique_ptr<Tree>> sK;
        sK.push_back(std::make_unique<Tree>(t0));
        // Let sigmaSK be the overall StdDev of all edges in sK
        double wBarreSK = sK[0]->getMean();
        double sigmaSK = sK[0]->getStd(wBarreSK);
        deltaSigmaSK.push_back(0.0);
        double descent(0.0);
        double threshold(0.0);
        unsigned int i(0);
        do {
            ++i;
            double temp(sigmaSK);
            /* Choose an edge that leads to max StdDev reduction
            once it is removed from sK */
            std::vector<std::vector<unsigned int>> edgeWeights(sK.size());
            unsigned int counter(0);
            for (auto const &tree : sK) {
                edgeWeights[counter] = tree->weights();
                ++counter;
            }

            deltaSigmaSK.push_back(0.0);
            unsigned int indexRow(0);
            unsigned int weightCol(0);
            for (unsigned int counterRow(0); counterRow < edgeWeights.size(); ++counterRow) {
                for (unsigned int counterCol(0); counterCol < edgeWeights[counterRow].size(); ++counterCol) {
                    // Assume e is removed from sK thus sK = U(j = 1 --> i + 1)Tj
                    std::vector<std::vector<unsigned int>> copyWeights(edgeWeights);
                    copyWeights[counterRow].erase(copyWeights[counterRow].begin() + counterCol);

                    double numerator(0);
                    double denominator(0);
                    for (auto const &w : copyWeights) {
                        if (!w.empty()) {
                            numerator += (w.size() * stdDev(w));
                            denominator += w.size();
                        }
                    }
                    sigmaSK = numerator / denominator;

                    /* Compute StdDev reduction */
                    if ((counterRow == 0 && counterCol == 0) || (deltaSigmaSK[i] > sigmaSK - temp)) {
                        deltaSigmaSK[i] = sigmaSK - temp;
                        indexRow = counterRow;
                        weightCol = edgeWeights[counterRow][counterCol];
                    }
                }
            }

            // Remove e from sK that corresponds to deltaSigmaSK[i]
            std::unique_ptr<Tree> subTree(sK[indexRow]->removeBranch(weightCol));
            if (subTree) {
                std::vector<unsigned int> dataForFuturePruning;
                dataForFuturePruning.push_back(indexRow);
                dataForFuturePruning.push_back(weightCol);
                branchesToPrune.push_back(dataForFuturePruning);
                // T' is the new disjoint subtree
                sK.push_back(move(subTree));
            } else {
                std::cerr << "ERREUR : diminution de l'écart-type maximale par suppression d'une branche inconnue."
                          << std::endl;
                abort();
            }

            sigmaSK = temp + deltaSigmaSK[i];

            descent = deltaSigmaSK[i] - deltaSigmaSK[i - 1];
            if (descent < 0) {
                descent *= -1.0;
            }
            threshold = 0.0001 * (deltaSigmaSK[i] + 1);
            if (threshold < 0) {
                threshold *= -1.0;
            }
        } while (descent >= threshold);
    }

private:
    double stdDev(std::vector<unsigned int> const &data) const {
        unsigned int sumMean(0);
        unsigned int sumStd(0);
        for (auto const &d : data) {
            sumMean += d;
            sumStd += (d * d);
        }
        double meanWeight = sumMean / (data.size() * 1.0);
        return sqrt(sumStd / (data.size() * 1.0) - meanWeight * meanWeight);
    }
};

#endif  // CLUSTERING_SDR_H
