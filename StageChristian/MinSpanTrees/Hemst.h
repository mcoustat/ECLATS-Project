#ifndef CLUSTERING_HEMST_H
#define CLUSTERING_HEMST_H

/*!
 * \file Hemst.h
 * \brief Implementation of the Hierarchical Euclidean Minimum Spanning Tree algorithm.
 * \author Christian Marquay
 * \version 0.1
 * \date March 11, 2019
 */

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>

#include "MaxTree.h"
#include "MinTree.h"
#include "Tree.h"

/*!
 * \class Hemst
 * \brief Class representing the Hierarchical Euclidean Minimum Spanning Tree algorithm.
 */
class Hemst {
public:
    /*!
     * \brief Builder.
     * \param nbClusters : number of clusters desired
     */
    explicit Hemst(unsigned int nbClusters) : m_nbClusters(nbClusters) {
    }

    /*!
     * \brief Execute the algorithm.
     * \param mT : a related component tree
     * \return a vector of subtrees corresponding to the clusters
     */
    std::vector<std::unique_ptr<Tree>> run(mln::my::MilenaTree &mT) const {
        // Let sT = Ø be the set of disjoint subtrees of emst
        std::vector<std::unique_ptr<Tree>> sT;

        // Construct emst from s
        sT.push_back(std::make_unique<Tree>(mT));

        roughPruning(sT);

        /* If the number of clusters nC is less than k,
        remove nC - k longest edges so that nC = k */
        if (sT.size() < m_nbClusters) {
            finePruning(sT);
        }

        /* if the number of clusters nC is greater than k */
        if (sT.size() > m_nbClusters) {
            centroids(mT, sT);
        }

        return sT;
    }

private:
    void centroids(mln::my::MilenaTree &mT, std::vector<std::unique_ptr<Tree>> &dest) const {
        std::vector<std::unique_ptr<mln::point2d>> s;
        for (auto const &subTree : dest) {
            std::unique_ptr<mln::point2d> centroid(subTree->getCentroid(mT.S.size(), 0));
            s.push_back(move(centroid));
        }
        mln::my::MilenaTree mTS = MaxTree(mT, s);
        std::vector<std::unique_ptr<Tree>> newSubTrees = run(mTS);
        std::vector<long> toPrune;
        for (auto const &tree : newSubTrees) {
            if (tree->getSize() > 1) {
                unsigned int counter(1);
                std::vector<std::unique_ptr<Tree>> centroidsConcatenated = tree->asVector();
                while (counter < centroidsConcatenated.size()) {
                    std::unique_ptr<mln::point2d> srcP(
                            std::make_unique<mln::point2d>(*(centroidsConcatenated[0]->getPoint())));
                    auto srcIt = find_if(s.begin(), s.end(),
                                         [&srcP](const std::unique_ptr<mln::point2d> &object) -> bool {
                                             return *object == *srcP;
                                         });
                    long srcIndex = distance(s.begin(), srcIt);
                    std::unique_ptr<mln::point2d> destP(
                            std::make_unique<mln::point2d>(*(centroidsConcatenated[counter]->getPoint())));
                    auto destIt = find_if(dest.begin(), dest.end(),
                                          [&destP](const std::unique_ptr<Tree> &object) -> bool {
                                              return *object->getPoint() == *destP;
                                          });
                    long destIndex = distance(dest.begin(), destIt);
                    dest[srcIndex]->append(mT, dest[destIndex]);
                    toPrune.push_back(destIndex);
                    ++counter;
                }
            }
        }
        for (auto const &tP : toPrune) {
            dest.erase(dest.begin() + tP);
        }
    }

    void display(std::ostream &flux) const {
        flux << "Je recherche " << m_nbClusters << " clusters.";
    }

    void finePruning(std::vector<std::unique_ptr<Tree>> &dest) const {
        while (dest.size() != m_nbClusters) {
            unsigned int counter(0);
            std::vector<int> maxWeights(dest.size());
            for (auto const &subTree : dest) {
                maxWeights[counter] = subTree->getHeaviestWeight();
                ++counter;
            }
            auto index(distance(maxWeights.begin(), max_element(maxWeights.begin(), maxWeights.end())));
            // Remove the current longest edge
            std::unique_ptr<Tree> subTree(dest[index]->removeBranch(maxWeights[index]));
            if (subTree) {
                // T' is the new disjoint subtree
                dest.push_back(move(subTree));
            } else {
                std::cerr << "ERREUR : Nombre de clusters demandés trop important et inatteignable." << std::endl;
                abort();
            }
        }
    }

    void roughPruning(std::vector<std::unique_ptr<Tree>> &dest) const {
        // Compute the average weight wBarre of all the edges
        double wBarre = dest[0]->getMean();

        // Compute the standard deviation sigma of the edges
        double sigma = dest[0]->getStd(wBarre);

        // Remove e from emst
        std::vector<std::unique_ptr<Tree>> subTrees = dest[0]->removeTooHeavyBranches(wBarre + sigma);

        // T' is the new disjoint subtree
        dest.insert(dest.end(), make_move_iterator(subTrees.begin()), make_move_iterator(subTrees.end()));
    }

    unsigned int m_nbClusters; /*< Number of clusters desired */

    friend std::ostream &operator<<(std::ostream &flux, Hemst const &hemst) {
        hemst.display(flux);
        return flux;
    }
};

#endif  // CLUSTERING_HEMST_H
