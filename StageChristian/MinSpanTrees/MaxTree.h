#ifndef CLUSTERING_MAX_TREE_H
#define CLUSTERING_MAX_TREE_H

/*!
 * \file MaxTree.h
 * \brief Implementation of a MaxTree rooted in the darkest pixel and for leaves the lighter pixels.
 * \author Jordan Drapeau
 * \version 0.1
 */

#include <memory>

#include "MilenaTree.h"

namespace mln::my {
    /*!
     * \brief Sort the pixels in the tree to get the darkest at the root and the clearest in the leaves.
     * \param u : the domain of an image2d
     * \return a vector containing the sorted pixels
     */
    std::vector<point2d> sort_increasingly(const image2d<value::int_u8> &u) {
        // h
        std::vector<unsigned> h(256, 0);
        mln_piter(box2d) p(u.domain());
        for_all(p) ++h[u(p)];

        // index
        std::vector<unsigned> index(256);
        index[0] = 0;
        for (unsigned i = 1; i != 256; ++i) {
            index[i] = index[i - 1] + h[i - 1];
        }

        // S
        std::vector<point2d> S(u.nsites());
        for_all(p) S[index[u(p)]++] = p;

        return S;
    }

    /*!
     * \brief Builder.
     * \param u : the domain of an image2d
     * \return a MilenaTree composed of a sorted vector
     */
    MilenaTree MaxTree(const image2d<value::int_u8> &u) {
        MilenaTree t(u);
        t.S = sort_increasingly(u);

        union_find(t);
        t.canonicalize();

        return t;
    }

    /*!
     * \brief Builder.
     * \param mT : the MilenaTree you want copied
     * \param s : the vector of the MilenaTree that you wish to copied
     * \return a MilenaTree composed of a sorted vector
     */
    MilenaTree MaxTree(MilenaTree const &mT, std::vector<std::unique_ptr<mln::point2d>> const &s) {
        MilenaTree t(mT, s);
        return t;
    }
}  // mln::my

#endif  // CLUSTERING_MAX_TREE_H

