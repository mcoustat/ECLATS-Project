/*!
 * \file main.cpp
 * \brief Separation of layers of information located on different gray levels.
 * \author Christian Marquay
 * \version 0.1
 * \date March 11, 2019
 */

#include <iostream>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/ppm/save.hh>
#include <mln/io/pgm/load.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "Hemst.h"
#include "MaxTree.h"
#include "MinTree.h"
#include "Mmsdr.h"
#include "Msdr.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

void usage(char *argv[]);

void usageHemst(char *argv[]);

int main(int argc, char *argv[]) {
    // argv [2] must match the path to a pgm image
    string img_path = argv[2];

    // If the condition is true, the parameter is not the path of an image in pgm format
    if (img_path.substr(img_path.rfind('.') + 1, img_path.length()) != "pgm") {
        cerr << "ERREUR : chemin de l'image non valide." << endl;
        usage(argv);
    }

    // Let s be the point set
    image2d<int_u8> pointsImg;
    io::pgm::load(pointsImg, img_path);
    MilenaTree mT = MaxTree(pointsImg);

    string method(argv[1]);
    transform(method.begin(), method.end(), method.begin(), [](unsigned char c) { return tolower(c); });

    vector<unique_ptr<Tree>> clusters;

    if (method == "hemst") {
        // If the condition is true, the number of arguments passed in parameters is wrong
        if (argc != 4) {
            cerr << "ERREUR : mauvais nombre d'arguments." << endl;
            usageHemst(argv);
        }

        // Conversion of the char *argv [3] to int
        int arg3(atoi(argv[3]));

        // If the condition is true, the parameter is not a positive number
        if (arg3 <= 0) {
            cerr << "ERREUR : nombre de clusters souhaité strictement positif." << endl;
            usageHemst(argv);
        }
        // If the condition is true, the parameter is a too big positive number
        if (arg3 > mT.S.size()) {
            cerr << "ERREUR : Nombre de clusters demandés trop important et inatteignable." << endl;
            usageHemst(argv);
        }
        auto k(static_cast<unsigned int>(arg3));

        Hemst hemst(k);
        clusters = hemst.run(mT);
    } else if (method == "msdr") {
        // If the condition is true, the number of arguments passed in parameters is wrong
        if (argc != 3) {
            cerr << "ERREUR : mauvais nombre d'arguments." << endl;
            usage(argv);
        }
        Msdr msdr;
        clusters = msdr.run(mT);
    } else if (method == "mmsdr") {
        // If the condition is true, the number of arguments passed in parameters is wrong
        if (argc != 3) {
            cerr << "ERREUR : mauvais nombre d'arguments." << endl;
            usage(argv);
        }
        Mmsdr mmsdr;
        clusters = mmsdr.run(mT);
    } else {
        cerr << "Méthode non encore implémentée." << endl;
    }

    image2d<value::rgb8> ima(pointsImg.nrows(), pointsImg.ncols());

    unsigned int counter(0);
    for (auto const &cluster : clusters) {
        vector<unique_ptr<Tree>> cl = cluster->asVector();
        for (auto const &point : cl) {
            switch (counter) {
                case 1 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::blue;
                    break;
                case 2 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::brown;
                    break;
                case 3 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::cyan;
                    break;
                case 4 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::dark_gray;
                    break;
                case 5 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::green;
                    break;
                case 6 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::light_gray;
                    break;
                case 7 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::lime;
                    break;
                case 8 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::magenta;
                    break;
                case 9 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::medium_gray;
                    break;
                case 10 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::olive;
                    break;
                case 11 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::orange;
                    break;
                case 12 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::pink;
                    break;
                case 13 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::purple;
                    break;
                case 14 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::red;
                    break;
                case 15 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::teal;
                    break;
                case 16 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::violet;
                    break;
                case 17 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::white;
                    break;
                case 18 :
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::yellow;
                    break;
                default:
                    ima(point2d(point->getPoint()->row(), point->getPoint()->col())) = literal::black;
                    break;
            }
        }
        ++counter;
    }

    unsigned long start = img_path.find_last_of('/');
    unsigned long end = img_path.find_last_of('.');
    std::stringstream ss;
    ss << "img/output/" << method << "-" << img_path.substr(start + 1, end - start - 1);
    if (method == "hemst") {
        ss << "-" << argv[3] << "clusters";
    }
    ss << ".ppm";
    io::ppm::save(ima, ss.str());

    cout << "Nombre de clusters : " << clusters.size() << endl;
    for (auto const &cluster : clusters) {
        cout << "Cluster de " << cluster->getSize() << " points." << std::endl;
        cout << *cluster << endl;
    }

    return 0;
}

void usage(char *argv[]) {
    cerr << "usage : " << argv[0] << " method path/to/img.pgm" << endl;
    abort();
}

void usageHemst(char *argv[]) {
    cerr << "usage : " << argv[0] << "method path/to/img.pgm positive_integer" << endl;
    abort();
}
