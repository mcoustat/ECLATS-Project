#ifndef CLUSTERING_MSDR_H
#define CLUSTERING_MSDR_H

/*!
 * \file Msdr.h
 * \brief Implementation of the Maximum Standard Deviation Reduction algorithm.
 * \author Christian Marquay
 * \version 0.1
 * \date March 21, 2019
 */

#include <iostream>
#include <memory>
#include <vector>

#include "MaxTree.h"
#include "MinTree.h"
#include "Sdr.h"
#include "Tree.h"

/*!
 * \class Msdr
 * \brief Class representing the Maximum Standard Deviation Reduction algorithm.
 */
class Msdr : public Sdr {
public:
    std::vector<std::unique_ptr<Tree>> run(mln::my::MilenaTree &mT) const override {
        // Let sK be the set of disjoint subtrees of t0
        std::vector<std::unique_ptr<Tree>> sK;

        // Let t0 the emst constructed emst from s
        sK.push_back(std::make_unique<Tree>(mT));

        std::vector<double> deltaSigmaSK;
        std::vector<std::vector<unsigned int>> branchesToPrune;

        subdivide(branchesToPrune, deltaSigmaSK, *sK[0]);

        /* No of clusters corresponds to the 1st local minimum */
        unsigned int k(polyReg(deltaSigmaSK));

        createClusters(k, branchesToPrune, sK);

        return sK;
    }

private:
    unsigned int polyReg(std::vector<double> const &deltaSigmaSK) const {
        unsigned int k(0);
        do {
            ++k;
        } while ((k < deltaSigmaSK.size() - 1) &&
                 ((deltaSigmaSK[k - 1] - deltaSigmaSK[k] <= 0) || (deltaSigmaSK[k] - deltaSigmaSK[k + 1] >= 0)));
        return k;
    };
};

#endif  // CLUSTERING_MSDR_H
