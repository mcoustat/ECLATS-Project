//
// Created by Christian Marquay on 19/06/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Tree.h"

using namespace cv;
using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

void pushRectangle(vector<vector<point2d>> &rectangles, point2d const &point, Tree const &tree);

vector<unsigned int> readClusters(string line, unsigned int &row, unsigned int &col, unsigned long &sep);

int main(int argc, char *argv[]) {
    string imgName = argv[1];

    std::stringstream ssImgBorders;
    std::stringstream ssImgOther;
    ssImgBorders << "img/test/borders/" << imgName << ".pgm";
    ssImgOther << "img/test/other/" << imgName << ".pgm";
    image2d<int_u8> pointsImgBorders;
    image2d<int_u8> pointsImgOther;
    io::pgm::load(pointsImgBorders, ssImgBorders.str());
    io::pgm::load(pointsImgOther, ssImgOther.str());
    Tree treeBorders = MinTree(pointsImgBorders);
    Tree treeOther = MinTree(pointsImgOther);

    image2d<value::int_u8> imaBorders(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<value::int_u8> imaDepartments(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<value::int_u8> imaInvestigations(pointsImgBorders.nrows(), pointsImgBorders.ncols());

    vector<image2d<value::int_u8>> imas;
    imas.push_back(imaBorders);
    imas.push_back(imaDepartments);
    imas.push_back(imaInvestigations);

    image2d<box2d> bbsBorders(computeBboxImage<Bbox>(treeBorders));
    image2d<box2d> bbsOther(computeBboxImage<Bbox>(treeOther));
    vector<vector<point2d>> rectangles;

    std::stringstream ssTsvBorders;
    std::stringstream ssTsvOther;
    ssTsvBorders << "data/clusters/" << imgName << "-borders.tsv";
    ssTsvOther << "data/clusters/" << imgName << "-other.tsv";
    ifstream fileTsvBorders(ssTsvBorders.str(), ios::in);  // on ouvre en lecture
    ifstream fileTsvOther(ssTsvOther.str(), ios::in);  // on ouvre en lecture
    if (fileTsvBorders) {  // si l'ouverture a fonctionné
        string lineBorders;
        getline(fileTsvBorders, lineBorders);
        if (getline(fileTsvBorders, lineBorders)) {
            unsigned long sep;
            unsigned int row;
            unsigned int col;
            vector<unsigned int> clusters(readClusters(lineBorders, row, col, sep));
            for (auto const &point : treeBorders.S) {
                unsigned int counter(0);
                for (auto &ima : imas) {
                    ima(point2d(point.row(), point.col())) = 255;
                    if (ima(point2d(treeBorders.parent(point).row(), treeBorders.parent(point).col())) != 255) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeBorders.parent(point).row(), treeBorders.parent(point).col()));
                        if (counter == 1) {
                            pushRectangle(rectangles, point, treeBorders);
                        }
                    }
                    ++counter;
                }
                if (treeBorders.parent(point) != point && bbsBorders(point).min_row() == row &&
                    bbsBorders(point).min_col() == col) {
                    if (bbsBorders(point).nrows() > 10 && bbsBorders(point).nrows() < 50 &&
                        bbsBorders(point).ncols() > 5 && bbsBorders(point).ncols() < 50) {
                        if (clusters[0] != 8 && clusters[0] != 10 && clusters[1] != 10 && clusters[2] != 0 &&
                            clusters[2] != 9 && clusters[0] != 0 && clusters[0] != 2) {
                            imas[0](point2d(point.row(), point.col())) = 0;
                        } else {
                            imas[2](point2d(point.row(), point.col())) = 0;
                        }
                    } else {
                        imas[0](point2d(point.row(), point.col())) = 0;
                    }
                    if (getline(fileTsvBorders, lineBorders)) {
                        istringstream newIss(lineBorders);
                        vector<string> newTokens{istream_iterator < string > {newIss}, istream_iterator < string > {}};
                        sep = newTokens[0].find(',');
                        row = stoul(newTokens[0].substr(1, sep - 1));
                        col = stoul(newTokens[0].substr(sep + 1, newTokens[0].length() - sep - 2));
                        for (unsigned int i(0); i < 5; ++i) {
                            clusters[i] = stoul(newTokens[i + 1]);
                        }
                    }

                }
            }
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier " << ssTsvBorders.str() << " !" << endl;
    }

    if (fileTsvOther) {  // si l'ouverture a fonctionné
        string lineOther;
        getline(fileTsvOther, lineOther);
        if (getline(fileTsvOther, lineOther)) {
            unsigned long sep;
            unsigned int row;
            unsigned int col;
            vector<unsigned int> clusters(readClusters(lineOther, row, col, sep));
            for (auto const &point : treeOther.S) {
                unsigned int counter(0);
                for (auto &ima : imas) {
                    if (ima(point2d(treeOther.parent(point).row(), treeOther.parent(point).col())) != 255) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeOther.parent(point).row(), treeOther.parent(point).col()));
                        if (counter == 1) {
                            pushRectangle(rectangles, point, treeOther);
                        }
                    }
                    ++counter;
                }
                if (treeOther.parent(point) != point && bbsOther(point).min_row() == row &&
                    bbsOther(point).min_col() == col) {
                    if ((bbsOther(point).min_row() > 1250 || bbsOther(point).min_col() > 2000) &&
                        (bbsOther(point).min_row() > 2000 ||
                         bbsOther(point).min_col() < pointsImgOther.ncols() - 1250) &&
                        (bbsOther(point).min_row() < pointsImgOther.nrows() - 1000 ||
                         bbsOther(point).min_col() < pointsImgOther.ncols() - 2750) &&
                        bbsOther(point).min_row() > 500 && bbsOther(point).min_row() < pointsImgOther.nrows() - 400) {
                        if (clusters[0] == 4 || clusters[0] == 9 || clusters[0] == 11 || clusters[0] == 15) {
                            vector<point2d> rectangle;
                            rectangle.push_back(point);
                            rectangles.push_back(rectangle);
                            imas[1](point2d(point.row(), point.col())) = 0;
                        } else if ((bbsOther(point).nrows() > 10 && bbsOther(point).nrows() < 50 &&
                                    bbsOther(point).ncols() > 5 && bbsOther(point).ncols() < 50) &&
                                   (clusters[0] == 5 || clusters[0] == 7 || clusters[0] == 8 || clusters[0] == 16) &&
                                   (clusters[1] == 0 || clusters[1] == 3 || clusters[1] == 7 ||
                                    clusters[1] == 10 || clusters[1] == 16) &&
                                   (clusters[2] == 2 || clusters[2] == 5 || clusters[2] == 11) &&
                                   (clusters[4] == 5 || clusters[4] == 7 || clusters[4] == 15)) {
                            imas[2](point2d(point.row(), point.col())) = 0;
                        }
                    }
                    if (getline(fileTsvOther, lineOther)) {
                        istringstream newIss(lineOther);
                        vector<string> newTokens{istream_iterator < string > {newIss}, istream_iterator < string > {}};
                        sep = newTokens[0].find(',');
                        row = stoul(newTokens[0].substr(1, sep - 1));
                        col = stoul(newTokens[0].substr(sep + 1, newTokens[0].length() - sep - 2));
                        for (unsigned int i(0); i < 5; ++i) {
                            clusters[i] = stoul(newTokens[i + 1]);
                        }
                    }
                }
            }
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier " << ssTsvOther.str() << " !" << endl;
    }
    fileTsvBorders.close();
    fileTsvOther.close();

    for (auto const &rect : rectangles) {
        for (unsigned int i(bbsOther(rect[0]).min_row()); i <= bbsOther(rect[0]).max_row(); ++i) {
            for (unsigned int j(bbsOther(rect[0]).min_col()); j <= bbsOther(rect[0]).max_col(); ++j) {
                if (imas[0](point2d(i, j)) != 255 || imas[2](point2d(i, j)) != 255) {
                    imas[0](point2d(i, j)) = 255;
                    imas[1](point2d(i, j)) = 0;
                    imas[2](point2d(i, j)) = 255;
                }
            }
        }
    }

    vector<string> layers({"Borders", "Departments", "Investigations"});
    for (unsigned int layerNumber(0); layerNumber < layers.size(); ++layerNumber) {
        std::stringstream ss;
        ss << "img/post_treatment/" << imgName << "-" << layers[layerNumber] << ".pgm";
        io::pgm::save(imas[layerNumber], ss.str());
    }

    // Apply blur to smooth edges and use adaptive thresholding
    std::stringstream ss;
    ss << "img/post_treatment/" << imgName << "-" << layers[2] << ".pgm";
    Mat src = imread(ss.str());
    Mat src_gray;
    GaussianBlur(src, src, Size(37, 37), 0);

    cvtColor(src, src_gray, COLOR_BGR2GRAY);

    std::vector<int> compression_params;  // Stores the compression parameters
    compression_params.push_back(IMWRITE_PXM_BINARY);  // Set to PXM compression
    compression_params.push_back(0);  // Set type of PXM in our case PGM

    imwrite(ss.str(), src_gray, compression_params);

    image2d<int_u8> pointsImgBlurred;
    io::pgm::load(pointsImgBlurred, ss.str());
    Tree treeBlurred = MinTree(pointsImgBlurred);
    image2d<int_u8> imaBlurred(pointsImgBlurred.nrows(), pointsImgBlurred.ncols());

    for (auto const &point : treeBlurred.S) {
        if (treeBlurred.u(point) > 229) {
            treeBlurred.u(point) = 255;
            imaBlurred(point2d(point.row(), point.col())) = 255;
        } else {
            treeBlurred.u(point) = 0;
            imaBlurred(point2d(point.row(), point.col())) = 0;
        }
    }

    io::pgm::save(imaBlurred, ss.str());

    cout << "Blocks OK" << endl;

    src = imread(ss.str());
    cvtColor(src, src_gray, COLOR_BGR2GRAY);

    Mat threshold_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    /// Detect edges using Threshold
    int thresh = 100;
    threshold(src_gray, threshold_output, thresh, 255, THRESH_BINARY);
    /// Find contours
    findContours(threshold_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

    /// Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly(contours.size());
    vector<Rect> boundRect(contours.size());
    vector<Point2f> center(contours.size());
    vector<float> radius(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
    }

    std::stringstream ssBinary;
    ssBinary << "img/binary/" << imgName << ".ppm";

    image2d<int_u8> imaBinary(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<rgb8> imaFinalInvestigations(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    io::pgm::load(imaBinary, ssBinary.str());
    data::fill(imaFinalInvestigations, literal::white);
    Tree treeBinary = MinTree(imaBinary);


    std::stringstream ssInvestigations;
    ssInvestigations << "investigations-" << imgName << ".tsv";
    std::ofstream fileInvestigations;
    fileInvestigations.open(ssInvestigations.str());
    fileInvestigations << "x\ty\twidth\theight\n";

    for (unsigned int i(0); i < contours.size(); i++) {
        unsigned int height(boundRect[i].br().y - boundRect[i].tl().y);
        unsigned int width(boundRect[i].br().x - boundRect[i].tl().x);
        for (unsigned int j(boundRect[i].tl().x); j < boundRect[i].br().x; ++j) {
            for (unsigned int k(boundRect[i].tl().y); k < boundRect[i].br().y; ++k) {
                point2d point = point2d(k, j);
                if (treeBinary.u(point) < 127) {
                    if (height > 20 && height < 200 && width > 5 && width < 125) {
                        imaFinalInvestigations(point) = literal::black;
                        fileInvestigations << boundRect[i].tl().x << "\t" << boundRect[i].tl().y << "\t" << width
                                           << "\t" << height << "\n";
                    }
                }
            }
        }
    }
    fileInvestigations.close();

    std::stringstream ssFinal;
    ssFinal << "img/post_treatment/" << imgName << "-" << layers[2] << ".ppm";

    io::ppm::save(imaFinalInvestigations, ssFinal.str());

    cout << "Rectangles OK" << endl;

    return 0;
}

void pushRectangle(vector<vector<point2d>> &rectangles, point2d const &point, Tree const &tree) {
    bool found(false);
    unsigned int rectNumber(0);
    while (rectNumber < rectangles.size() && !found) {
        if (rectangles[rectNumber][0] == tree.parent(point)) {
            rectangles[rectNumber].push_back(point);
            found = true;
        }
        ++rectNumber;
    }
}

vector<unsigned int> readClusters(string line, unsigned int &row, unsigned int &col, unsigned long &sep) {
    istringstream iss(line);
    vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
    sep = tokens[0].find(',');
    row = stoul(tokens[0].substr(1, sep - 1));
    col = stoul(tokens[0].substr(sep + 1, tokens[0].length() - sep - 2));
    unsigned int agglomerativeClusteringCluster = stoul(tokens[1]);
    unsigned int birchCluster = stoul(tokens[2]);
    unsigned int kMeansCluster = stoul(tokens[3]);
    unsigned int meanShiftCluster = stoul(tokens[4]);
    unsigned int miniBatchKMeansCluster = stoul(tokens[5]);
    vector<unsigned int> clusters;
    clusters.push_back(agglomerativeClusteringCluster);
    clusters.push_back(birchCluster);
    clusters.push_back(kMeansCluster);
    clusters.push_back(meanShiftCluster);
    clusters.push_back(miniBatchKMeansCluster);
    return clusters;
}
