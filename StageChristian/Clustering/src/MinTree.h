//
// Created by Jordan Drapeau, modified by Christian Marquay on 01/04/2019.
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#ifndef CLUSTERING_MIN_TREE_H
#define CLUSTERING_MIN_TREE_H

#include <memory>

#include "Tree.h"

namespace mln::my {
    std::vector<point2d> sort_decreasingly(const image2d <value::int_u8> &u) {
        // h
        std::vector<unsigned> h(256, 0);
        mln_piter(box2d) p(u.domain());
        for_all(p) ++h[u(p)];

        // index
        std::vector<unsigned> index(256);
        index[255] = 0;
        for (int i = 254; i >= 0; --i) {
            index[i] = index[i + 1] + h[i + 1];
        }

        // S
        std::vector<point2d> S(u.nsites());
        for_all(p) S[index[u(p)]++] = p;

        return S;
    }

    Tree MinTree(const image2d <value::int_u8> &u) {
        Tree t(u);
        t.S = sort_decreasingly(u);

        union_find(t);
        t.canonicalize();

        return t;
    }

    Tree MinTree(Tree const &mT, std::vector<std::unique_ptr<mln::point2d>> const &s) {
        Tree t(mT, s);
        return t;
    }
}  // mln::my

#endif  // CLUSTERING_MIN_TREE_H
