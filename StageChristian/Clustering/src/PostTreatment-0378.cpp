//
// Created by Christian Marquay on 23/05/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

int main(int argc, char *argv[]) {
    string tsv_path = argv[1];

    unsigned long start = tsv_path.find_last_of('/');
    unsigned long end = tsv_path.find_last_of('.');
    string imgName = tsv_path.substr(start + 1, end - start - 1);

    std::stringstream ssBinary;
    ssBinary << "img/binary/" << imgName << ".ppm";
    image2d<int_u8> pointsImgBinary;
    io::pgm::load(pointsImgBinary, ssBinary.str());
    Tree treeBinary = MinTree(pointsImgBinary);

    image2d<value::rgb8> imaDefault(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaDepartments(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaInvestigations(pointsImgBinary.nrows(), pointsImgBinary.ncols());

    vector<image2d<value::rgb8>> imas;
    imas.push_back(imaDefault);
    imas.push_back(imaDepartments);
    imas.push_back(imaInvestigations);

    image2d<box2d> bbs(computeBboxImage<Bbox>(treeBinary));
    image2d<double> lineThick(computeLineThicknessImage(treeBinary));

    vector<vector<point2d>> rectangles;

    std::ofstream fileDepartments;
    std::ofstream fileInvestigations;
    std::stringstream ssDepartments;
    std::stringstream ssInvestigations;
    ssDepartments << "departements-" << imgName << ".tsv";
    ssInvestigations << "investigations-" << imgName << ".tsv";
    fileDepartments.open(ssDepartments.str());
    fileInvestigations.open(ssInvestigations.str());
    fileDepartments << "x\ty\twidth\theight\n";
    fileInvestigations << "x\ty\twidth\theight\tthick\n";

    ifstream file(tsv_path, ios::in);  // on ouvre en lecture
    if (file) {  // si l'ouverture a fonctionné
        string line;
        getline(file, line);
        if (getline(file, line)) {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
            unsigned long sep = tokens[0].find(',');
            unsigned int row = stoul(tokens[0].substr(1, sep - 1));
            unsigned int col = stoul(tokens[0].substr(sep + 1, tokens[0].length() - sep - 2));
            unsigned int agglomerativeClusteringCluster = stoul(tokens[1]);
            unsigned int birchCluster = stoul(tokens[2]);
            unsigned int kMeansCluster = stoul(tokens[3]);
            unsigned int meanShiftCluster = stoul(tokens[4]);
            unsigned int miniBatchKMeansCluster = stoul(tokens[5]);
            vector<unsigned int> clusters;
            clusters.push_back(agglomerativeClusteringCluster);
            clusters.push_back(birchCluster);
            clusters.push_back(kMeansCluster);
            clusters.push_back(meanShiftCluster);
            clusters.push_back(miniBatchKMeansCluster);
            for (auto const &point : treeBinary.S) {
                unsigned int counter(0);
                for (auto &ima : imas) {
                    ima(point2d(point.row(), point.col())) = literal::black;
                    if (ima(point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col())) !=
                        literal::black) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col()));
                        if (counter == 1) {
                            bool found(false);
                            unsigned int rectNumber(0);
                            while (rectNumber < rectangles.size() && !found) {
                                if (rectangles[rectNumber][0] == treeBinary.parent(point)) {
                                    rectangles[rectNumber].push_back(point);
                                    found = true;
                                }
                                ++rectNumber;
                            }
                        }
                    }
                    ++counter;
                }
                if (treeBinary.parent(point) != point && bbs(point).min_row() == row && bbs(point).min_col() == col) {
                    if (bbs(point).min_row() > 250 && bbs(point).min_col() < 9000 &&
                        (bbs(point).min_col() > 1000 || (bbs(point).min_row() > 3000 && bbs(point).min_row() < 5000)) &&
                        ((bbs(point).min_row() < 11000 || bbs(point).min_col() > 4000) &&
                         (bbs(point).min_row() < 10000 || bbs(point).min_col() < 7500)) &&
                        (clusters[0] == 1 || clusters[0] == 13) && (clusters[1] == 2 || clusters[1] == 6) &&
                        (clusters[2] == 8 || clusters[2] == 9) && (clusters[3] == 2 || clusters[3] == 15) &&
                        (clusters[4] == 11 || clusters[4] == 15)) {
                        vector<point2d> rectangle;
                        rectangle.push_back(point);
                        rectangles.push_back(rectangle);
                        imas[1](point2d(point.row(), point.col())) = literal::white;
                        fileDepartments << bbs(point).min_col() << "\t" << bbs(point).min_row() << "\t"
                                        << bbs(point).ncols() << "\t" << bbs(point).nrows() << "\n";
                    } else if (((bbs(point).min_row() > 2000 || bbs(point).min_col() > 2000) &&
                                (bbs(point).min_row() > 3350 ||
                                 bbs(point).min_col() < pointsImgBinary.ncols() - 2000) &&
                                (bbs(point).min_row() < pointsImgBinary.nrows() - 1250 ||
                                 bbs(point).min_col() < pointsImgBinary.ncols() - 2500)) &&
                               (bbs(point).nrows() > 20 && bbs(point).nrows() < 60 && bbs(point).ncols() > 15 &&
                                lineThick(point) > 4 && lineThick(point) < 10) &&
                               (clusters[0] == 2 || clusters[0] == 5 || clusters[0] == 6 || clusters[0] == 7 ||
                                clusters[0] == 12 || clusters[0] == 14 || clusters[0] == 16) &&
                               (clusters[1] == 1 || clusters[1] == 3) &&
                               (clusters[2] == 0 || clusters[2] == 5 || clusters[2] == 9 || clusters[2] == 14 ||
                                clusters[2] == 15) && (clusters[3] == 1 || clusters[3] == 2) &&
                               (clusters[4] == 5 || clusters[4] == 7 || clusters[4] == 10 || clusters[4] == 12 ||
                                clusters[4] == 13 || clusters[4] == 15 || clusters[4] == 16)) {
                        imas[2](point2d(point.row(), point.col())) = literal::white;
                        fileInvestigations << bbs(point).min_col() << "\t" << bbs(point).min_row() << "\t"
                                           << bbs(point).ncols() << "\t" << bbs(point).nrows() << "\t"
                                           << lineThick(point) << "\n";
                    } else {
                        imas[0](point2d(point.row(), point.col())) = literal::white;
                    }
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[0].find(',');
                        row = stoul(newTokens[0].substr(1, sep - 1));
                        col = stoul(newTokens[0].substr(sep + 1, newTokens[0].length() - sep - 2));
                        for (unsigned int i(0); i < 5; ++i) {
                            clusters[i] = stoul(newTokens[i + 1]);
                        }
                    }
                }
            }
            fileDepartments.close();
            fileInvestigations.close();
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
    file.close();

    for (auto const &rect : rectangles) {
        for (unsigned int i(bbs(rect[0]).min_row()); i <= bbs(rect[0]).max_row(); ++i) {
            for (unsigned int j(bbs(rect[0]).min_col()); j <= bbs(rect[0]).max_col(); ++j) {
                if (imas[0](point2d(i, j)) != literal::black || imas[2](point2d(i, j)) != literal::black) {
                    imas[0](point2d(i, j)) = literal::black;
                    imas[1](point2d(i, j)) = literal::white;
                    imas[2](point2d(i, j)) = literal::black;
                }
            }
        }
    }

    vector<string> layers({"default", "Departments", "Investigations"});
    for (unsigned int layerNumber(0); layerNumber < layers.size(); ++layerNumber) {
        std::stringstream ss;
        ss << "img/post_treatment/" << imgName << "-" << layers[layerNumber] << ".ppm";
        io::ppm::save(imas[layerNumber], ss.str());
    }

    return 0;
}
