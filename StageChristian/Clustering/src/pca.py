#! /usr/bin/env python3
# coding: utf-8

"""
Module for performing a PCA on raw data.
param -d the file containing the data
returns the file containing the principal components.
"""

import argparse

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
from sklearn import decomposition, preprocessing


def parse_arguments():
    """
    Function to read the parameters passed in arguments at the execution of the module.
    return a parser containing the parameters
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--datafile", help="""TSV file containing the data""")
    return parser.parse_args()


def launch_pca(data_file, name):
    """
    Function to perform a PCA on the raw data, saves the principal components in a new file.
    param data_file the file containing the data
    param name the generic name to perform backups
    """
    # load the data
    data = pd.read_csv(data_file, sep="\t")
    # drop the columns we will not use
    my_data = data.drop(['point'], axis=1)

    # transform data into numpy array
    my_values = my_data.values

    std_scale = preprocessing.StandardScaler().fit(my_values.astype(float))
    x_scaled = std_scale.transform(my_values.astype(float))

    launch_pca2(my_data, name, x_scaled)
    launch_pca3(my_data, name, x_scaled)

    pca = decomposition.PCA(n_components=7)
    pca.fit(x_scaled)

    # project x on the principal components
    x_projected = pca.transform(x_scaled)

    print(pca.explained_variance_ratio_)
    print(pca.explained_variance_ratio_.cumsum())

    df_data = pd.DataFrame({'point': data['point'],
                            'PC_1': x_projected[:, 0],
                            'PC_2': x_projected[:, 1],
                            'PC_3': x_projected[:, 2],
                            'PC_4': x_projected[:, 3],
                            'PC_5': x_projected[:, 4],
                            'PC_6': x_projected[:, 5],
                            'PC_7': x_projected[:, 6]})
    df_data.to_csv('data/pca/' + name + '.tsv', index=False, sep="\t")


def launch_pca2(my_data, name, x_scaled):
    """
    Function to perform a PCA on the raw data with two principles components,
    saves graphical representations of data and components.
    param my_data useful columns containing the data
    param name the generic name to perform backups
    param x_scaled the scaled data
    """
    pca = decomposition.PCA(n_components=2)
    pca.fit(x_scaled)

    # project x on the principal components
    x_projected = pca.transform(x_scaled)

    plt.figure(figsize=(8, 8))
    for i, (abscissa, ordinate) in enumerate(zip(pca.components_[0, :], pca.components_[1, :])):
        plt.plot([0, abscissa], [0, ordinate], color='k')
        plt.text(abscissa, ordinate, my_data.columns[i], fontsize='14')
    plt.xlabel('PC 1')
    plt.ylabel('PC 2')
    plt.savefig('img/data/2components/' + name + '.png')
    plt.close()
    plt.clf()

    # display each observation
    plt.figure(figsize=(8, 8))
    plt.scatter(x_projected[:, 0], x_projected[:, 1])
    plt.savefig('img/data/2data/' + name + '.png')
    plt.clf()


def launch_pca3(my_data, name, x_scaled):
    """
    Function to perform a PCA on the raw data with three principles components,
    saves graphical representations of data and components.
    param my_data useful columns containing the data
    param name the generic name to perform backups
    param x_scaled the scaled data
    """
    pca = decomposition.PCA(n_components=3)
    pca.fit(x_scaled)

    # project x on the principal components
    x_projected = pca.transform(x_scaled)

    fig = plt.figure(figsize=(8, 8))
    axes = Axes3D(fig)
    for i, (pc1, pc2, pc3) in enumerate(zip(pca.components_[0, :],
                                            pca.components_[1, :],
                                            pca.components_[2, :])):
        axes.plot([0, pc1], [0, pc2], [0, pc3], color='k')
        axes.text(pc1, pc2, pc3, my_data.columns[i], fontsize='14')
    axes.set_xlabel('PC 1')
    axes.set_ylabel('PC 2')
    axes.set_zlabel('PC 3')
    plt.savefig('img/data/3components/' + name + '.png')
    plt.close()
    plt.clf()

    # display each observation
    fig = plt.figure(figsize=(8, 8))
    axes = Axes3D(fig)
    axes.scatter(x_projected[:, 0], x_projected[:, 1], x_projected[:, 2])
    plt.savefig('img/data/3data/' + name + '.png')
    plt.clf()


if __name__ == "__main__":
    ARGS = parse_arguments()
    FILE_NAME = ARGS.datafile
    START = FILE_NAME.rfind('/')
    END = FILE_NAME.rfind('.')
    RADICAL_NAME = FILE_NAME[START+1:END]
    launch_pca(FILE_NAME, RADICAL_NAME)
