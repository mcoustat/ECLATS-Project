//
// Created by Christian Marquay on 13/05/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#ifndef CLUSTERING_COMPUTEATTRIBUTES_H
#define CLUSTERING_COMPUTEATTRIBUTES_H

#include "Tree.h"

namespace mln::my {
    struct Bbox {
        typedef box2d value_type;

        box2d bb;

        void init(const point2d &p) {
            bb.pmin() = p;
            bb.pmax() = p;
        }

        void take(const point2d &p) {
            bb.merge(box2d(p, p));
        }

        void take(const Bbox &other) {
            bb.merge(other.bb);
        }

        box2d value() const {
            return bb;
        }
    };

    // compute an image of the area attribute
    image2d<unsigned> computeAreaImage(Tree const &t) {
        image2d<unsigned> area(t.domain());
        data::fill(area, 1);

        for (unsigned i(t.S.size() - 1); i != 0; --i) {
            point2d p(t.S[i]);  // p goes from leaves to root
            area(t.parent(p)) += area(p);
        }

        t.back_propagate(area);
        return area;
    }

    template<typename A>
    image2d<typename A::value_type> computeBboxImage(Tree const &t) {
        image2d<A> acc(t.domain());

        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                acc(p).init(p);  // initialization for each node
            }
        }

        image2d<typename A::value_type> attr(t.domain());
        // p goes from leaves to root
        for (int i(t.S.size() - 1); i >= 0; --i) {
            point2d p(t.S[i]);
            point2d q(t.parent(p));

            if (t.is_representative(p)) {
                // done with this node so store the result
                attr(p) = acc(p).value();
                // and push info towards the parent node
                if (!t.is_root(p)) {
                    acc(q).take(acc(p));
                }
            } else {
                // take into account p
                acc(q).take(p);
            }
        }
        t.back_propagate(attr);
        return attr;
    }

    // compute an image of the density attribute
    image2d<double> computeDensityImage(Tree const &t) {
        image2d<double> dens(t.domain());
        image2d<unsigned int> area(computeAreaImage(t));
        image2d<box2d> bbs(computeBboxImage<Bbox>(t));
        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                dens(p) = (1.0 * area(p)) / (1.0 * bbs(p).nrows() * bbs(p).ncols());
            }
        }
        t.back_propagate(dens);
        return dens;
    }

    // compute an image of the eccentricity attribute
    image2d<double> computeEccentricityImage(Tree const &t) {
        // e = (1-b^2/a^2)^(1/2) since a is the length of the semi-major axis, a >= b
        image2d<double> ecc(t.domain());
        image2d<box2d> bbs(computeBboxImage<Bbox>(t));
        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                unsigned int a;
                unsigned int b;
                if (bbs(p).nrows() >= bbs(p).ncols()) {
                    a = bbs(p).nrows();
                    b = bbs(p).ncols();
                } else {
                    a = bbs(p).ncols();
                    b = bbs(p).nrows();
                }
                ecc(p) = sqrt(1.0 - (1.0 * b * b) / (1.0 * a * a));
            }
        }
        t.back_propagate(ecc);
        return ecc;
    }

    // compute an image of the perimeter attribute
    image2d<double> computeLineThicknessImage(Tree const &t) {
        image2d<double> lineThick(t.domain());
        image2d<box2d> bbs(computeBboxImage<Bbox>(t));
        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                std::vector<unsigned int> heights;
                std::vector<unsigned int> widths;
                for (unsigned int i(bbs(p).min_row()); i <= bbs(p).max_row(); ++i) {
                    for (unsigned int j(bbs(p).min_col()); j <= bbs(p).max_col(); ++j) {
                        point2d current(point2d(i, j));
                        if ((p == t.parent(current) && t.u(p) == t.u(current)) || p == current) {
                            unsigned int height(1);
                            if (i == 0 || t.u(point2d(i - 1, j)) != t.u(current)) {
                                unsigned int tmpY(i);
                                while (tmpY != t.u.nrows() - 1 && t.u(point2d(tmpY + 1, j)) == t.u(current)) {
                                    ++height;
                                    ++tmpY;
                                }
                                heights.push_back(height);
                            }
                            unsigned int width(1);
                            if (j == 0 || t.u(point2d(i, j - 1)) != t.u(current)) {
                                unsigned int tmpX(j);
                                while (tmpX != t.u.ncols() - 1 && t.u(point2d(i, tmpX + 1)) == t.u(current)) {
                                    ++width;
                                    ++tmpX;
                                }
                                widths.push_back(width);
                            }
                        }
                    }
                }
                sort(heights.begin(), heights.end());
                double medianY(0);
                if (heights.size() % 2 == 0) {
                    medianY = (1.0 * (heights[heights.size() / 2 - 1] + heights[heights.size() / 2])) / 2.0;
                } else {
                    medianY = 1.0 * heights[heights.size() / 2];
                }
                sort(widths.begin(), widths.end());
                double medianX(0);
                if (widths.size() % 2 == 0) {
                    medianX = (1.0 * (widths[widths.size() / 2 - 1] + widths[widths.size() / 2])) / 2.0;
                } else {
                    medianX = 1.0 * widths[widths.size() / 2];
                }
                lineThick(p) = std::min(medianY, medianX);
            }
        }
        t.back_propagate(lineThick);
        return lineThick;
    }

    // compute an image of the perimeter attribute
    image2d<unsigned int> computePerimeterImage(Tree const &t) {
        image2d<unsigned int> per(t.domain());
        image2d<box2d> bbs(computeBboxImage<Bbox>(t));
        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                unsigned int perimeter(0);
                for (unsigned int i(bbs(p).min_row()); i <= bbs(p).max_row(); ++i) {
                    for (unsigned int j(bbs(p).min_col()); j <= bbs(p).max_col(); ++j) {
                        point2d current(point2d(i, j));
                        if ((p == t.parent(current) && t.u(p) == t.u(current)) || p == current) {
                            if (i == 0 || t.u(point2d(i - 1, j)) != t.u(current)) {
                                ++perimeter;
                            }
                            if (j == t.u.ncols() - 1 || t.u(point2d(i, j + 1)) != t.u(current)) {
                                ++perimeter;
                            }
                            if (i == t.u.nrows() - 1 || t.u(point2d(i + 1, j)) != t.u(current)) {
                                ++perimeter;
                            }
                            if (j == 0 || t.u(point2d(i, j - 1)) != t.u(current)) {
                                ++perimeter;
                            }
                        }
                    }
                }
                per(p) = perimeter;
            }
        }
        t.back_propagate(per);
        return per;
    }

    image2d<double> computeCircularityImage(Tree const &t) {
        // C = 4_pi_A/P^2 ~ 12.57_A/P^2, where C is the circularity, A is the area and P is the perimeter
        image2d<unsigned int> area(computeAreaImage(t));
        image2d<unsigned int> per(computePerimeterImage(t));
        image2d<double> circ(t.domain());
        for (auto const &p : t.S) {
            if (t.is_representative(p)) {
                circ(p) = (4.0 * M_PI * area(p)) / (1.0 * per(p) * per(p));
            }
        }
        t.back_propagate(circ);
        return circ;
    }
}  // mln::my

#endif  // CLUSTERING_COMPUTEATTRIBUTES_H

