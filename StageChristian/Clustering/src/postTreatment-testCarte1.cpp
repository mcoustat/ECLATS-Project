//
// Created by Christian Marquay on 18/04/2019.
// Bbox, ComputeAreaImage, ComputeAttribute, MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "Bbox.h"
#include "ComputeAreaImage.h"
#include "ComputeAttribute.h"
#include "MinTree.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

int main(int argc, char *argv[]) {
    string tsv_path = argv[1];

    unsigned long start = tsv_path.find_last_of('/');
    unsigned long end = tsv_path.find_last_of('.');
    string imgName = tsv_path.substr(start + 1, end - start - 1);

    std::stringstream ssBinary;
    ssBinary << "img/binary/" << imgName << ".ppm";
    image2d<int_u8> pointsImgBinary;
    io::pgm::load(pointsImgBinary, ssBinary.str());
    Tree treeBinary = MinTree(pointsImgBinary);

    image2d<value::rgb8> imaDefault(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaDepartments(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaInvestigations(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaBorders(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaPhonetics(pointsImgBinary.nrows(), pointsImgBinary.ncols());

    vector<image2d<value::rgb8>> imas;
    imas.push_back(imaDefault);
    imas.push_back(imaDepartments);
    imas.push_back(imaInvestigations);
    imas.push_back(imaBorders);
    imas.push_back(imaPhonetics);

    vector<vector<point2d>> rectangles;

    ifstream file(tsv_path, ios::in);  // on ouvre en lecture
    if (file) {  // si l'ouverture a fonctionné
        string line;
        getline(file, line);
        if (getline(file, line)) {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
            unsigned long sep = tokens[1].find(',');
            unsigned int row = stoul(tokens[1].substr(1, sep - 1));
            unsigned int col = stoul(tokens[1].substr(sep + 1, tokens[1].length() - sep - 2));
            unsigned int cluster = stoul(tokens[5]);
            for (auto const &point : treeBinary.S) {
                unsigned int counter(0);
                for (auto ima : imas) {
                    ima(point2d(point.row(), point.col())) = literal::black;
                    if (ima(point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col())) !=
                        literal::black) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col()));
                        if (counter == 1) {
                            bool found(false);
                            unsigned int rectNumber(0);
                            while (rectNumber < rectangles.size() && !found) {
                                if (rectangles[rectNumber][0] == treeBinary.parent(point)) {
                                    rectangles[rectNumber].push_back(point);
                                    found = true;
                                }
                                ++rectNumber;
                            }
                        }
                    }
                    ++counter;
                }
                if (point.row() == row && point.col() == col) {
                    switch (cluster) {
                        case 1:
                            imas[0](point2d(point.row(), point.col())) = literal::blue;
                            break;
                        case 2:
                            imas[0](point2d(point.row(), point.col())) = literal::brown;
                            break;
                        case 3:
                            imas[0](point2d(point.row(), point.col())) = literal::cyan;
                            break;
                        case 4:
                            imas[0](point2d(point.row(), point.col())) = literal::dark_gray;
                            break;
                        case 5:
                            imas[0](point2d(point.row(), point.col())) = literal::green;
                            break;
                        case 6:
                            imas[0](point2d(point.row(), point.col())) = literal::light_gray;
                            break;
                        case 7:
                            imas[0](point2d(point.row(), point.col())) = literal::lime;
                            break;
                        case 11:
                            imas[2](point2d(point.row(), point.col())) = literal::white;
                            break;
                        case 13:
                            imas[0](point2d(point.row(), point.col())) = literal::red;
                            break;
                        case 14:
                            imas[0](point2d(point.row(), point.col())) = literal::teal;
                            break;
                        case 9:
                        case 12:
                        case 15:
                            imas[3](point2d(point.row(), point.col())) = literal::white;
                            break;
                        case 16:
                            imas[0](point2d(point.row(), point.col())) = literal::yellow;
                            break;
                        case 8:
                        case 10: {
                            vector<point2d> rectangle;
                            rectangle.push_back(point);
                            rectangles.push_back(rectangle);
                            imas[1](point2d(point.row(), point.col())) = literal::white;
                            break;
                        }
                        default:
                            imas[0](point2d(point.row(), point.col())) = literal::white;
                            break;
                    }
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[1].find(',');
                        row = stoul(newTokens[1].substr(1, sep - 1));
                        col = stoul(newTokens[1].substr(sep + 1, newTokens[1].length() - sep - 2));
                        cluster = stoul(newTokens[5]);
                    }
                }
            }
        }
        file.clear();
        file.seekg(0, ios::beg);
        getline(file, line);
        if (getline(file, line)) {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
            unsigned long sep = tokens[1].find(',');
            unsigned int row = stoul(tokens[1].substr(1, sep - 1));
            unsigned int col = stoul(tokens[1].substr(sep + 1, tokens[1].length() - sep - 2));
            unsigned int cluster = stoul(tokens[5]);
            for (auto const &point : treeBinary.S) {
                vector<unsigned int> others = {8, 9, 10, 11, 12, 15};
                if (find(others.begin(), others.end(), cluster) != others.end()) {
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[1].find(',');
                        row = stoul(newTokens[1].substr(1, sep - 1));
                        col = stoul(newTokens[1].substr(sep + 1, newTokens[1].length() - sep - 2));
                        cluster = stoul(newTokens[5]);
                    }
                    continue;
                }
                    if (imas[0](point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col())) !=
                        literal::black) {
                        imas[0](point2d(point.row(), point.col())) = imas[0](
                                point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col()));
                    }
                if (point.row() == row && point.col() == col) {
                    bool found(false);
                    int x(20);
                    int y(40);
                    while (!found && x >= -20) {
                        int X(point.row() - x);
                        if (X < 0) {
                            X = 0;
                        }
                        if (X >= pointsImgBinary.nrows()) {
                            X = pointsImgBinary.nrows() - 1;
                        }
                        int Y(point.col() - y);
                        if (Y < 0) {
                            Y = 0;
                        }
                        if (Y >= pointsImgBinary.ncols()) {
                            Y = pointsImgBinary.ncols() - 1;
                        }
                        if (imas[0](point2d(X, Y)) != literal::black) {
                            imas[0](point2d(point.row(), point.col())) = imas[0](point2d(X, Y));
//                            if (imas[0](point2d(point.row(), point.col())) == literal::white) {
//                                imas[3](point2d(point.row(), point.col())) = literal::white;
//                            } else {
//                                imas[4](point2d(point.row(), point.col())) = literal::white;
//                            }
                            found = true;
                        }
                        --y;
                        if (y < -40) {
                            y = 40;
                            --x;
                        }
                    }
                    if (!found) {
                        switch (cluster) {
                            case 0:
                                imas[0](point2d(point.row(), point.col())) = literal::white;
                                break;
                            case 1:
                                imas[0](point2d(point.row(), point.col())) = literal::blue;
                                break;
                            case 2:
                                imas[0](point2d(point.row(), point.col())) = literal::brown;
                                break;
                            case 3:
                                imas[0](point2d(point.row(), point.col())) = literal::cyan;
                                break;
                            case 4:
                                imas[0](point2d(point.row(), point.col())) = literal::dark_gray;
                                break;
                            case 5:
                                imas[0](point2d(point.row(), point.col())) = literal::green;
                                break;
                            case 6:
                                imas[0](point2d(point.row(), point.col())) = literal::light_gray;
                                break;
                            case 7:
                                imas[0](point2d(point.row(), point.col())) = literal::lime;
                                break;
                            case 13:
                                imas[0](point2d(point.row(), point.col())) = literal::red;
                                break;
                            case 14:
                                imas[0](point2d(point.row(), point.col())) = literal::teal;
                                break;
                            case 16:
                                imas[0](point2d(point.row(), point.col())) = literal::yellow;
                                break;
                            default:
                                imas[0](point2d(point.row(), point.col())) = literal::black;
                                break;
                        }
                    }
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[1].find(',');
                        row = stoul(newTokens[1].substr(1, sep - 1));
                        col = stoul(newTokens[1].substr(sep + 1, newTokens[1].length() - sep - 2));
                        cluster = stoul(newTokens[5]);
                    }
                }
            }
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
    file.close();

    image2d<box2d> bbs = ComputeAttribute<Bbox>(treeBinary);
    vector<vector<point2d>> bounds;

    for (auto const &rect : rectangles) {
        vector<point2d> points;
        points.push_back(rect[0]);
        points.push_back(rect[0]);
        for (auto const &r : rect) {
            if (r.col() < points[0].col() || (r.col() == points[0].col() && r.row() < points[0].row())) {
                points[0] = r;
            }
            if (r.col() > points[1].col() || (r.col() == points[1].col() && r.row() > points[1].row())) {
                points[1] = r;
            }
        }
        bounds.push_back(points);
    }

    for (auto const &b : bounds) {
        for (unsigned int i(b[0].row()); i < b[1].row(); ++i) {
            for (unsigned int j(b[0].col()); j < b[1].col(); ++j) {
                for (unsigned int k(0); k < imas.size(); ++k) {
                    if (k == 1) {
                        ++k;
                    }
                    if (imas[k](point2d(i, j)) != literal::black) {
                        imas[k](point2d(i, j)) = literal::black;
                        imas[1](point2d(i, j)) = literal::white;
                    }
                }
            }
        }
    }

    std::stringstream ssDefault;
    ssDefault << "img/post_treatment/" << imgName << "-hierarchical-default.ppm";
    io::ppm::save(imas[0], ssDefault.str());
    std::stringstream ssDepartments;
    ssDepartments << "img/post_treatment/" << imgName << "-hierarchical-departments.ppm";
    io::ppm::save(imas[1], ssDepartments.str());
    std::stringstream ssInvestigations;
    ssInvestigations << "img/post_treatment/" << imgName << "-hierarchical-investigations.ppm";
    io::ppm::save(imas[2], ssInvestigations.str());
    std::stringstream ssBorders;
    ssBorders << "img/post_treatment/" << imgName << "-hierarchical-borders.ppm";
    io::ppm::save(imas[3], ssBorders.str());
    std::stringstream ssPhonetics;
    ssPhonetics << "img/post_treatment/" << imgName << "-hierarchical-phonetics.ppm";
    io::ppm::save(imas[4], ssPhonetics.str());

    return 0;
}
