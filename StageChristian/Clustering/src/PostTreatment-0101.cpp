//
// Created by Christian Marquay on 19/06/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

int main(int argc, char *argv[]) {
    string imgName = argv[1];

    std::stringstream ssImgBorders;
    std::stringstream ssImgOther;
    ssImgBorders << "img/test/borders/" << imgName << ".pgm";
    ssImgOther << "img/test/other/" << imgName << ".pgm";
    image2d<int_u8> pointsImgBorders;
    image2d<int_u8> pointsImgOther;
    io::pgm::load(pointsImgBorders, ssImgBorders.str());
    io::pgm::load(pointsImgOther, ssImgOther.str());
    Tree treeBorders = MinTree(pointsImgBorders);
    Tree treeOther = MinTree(pointsImgOther);

    image2d<value::rgb8> imaBorders(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<value::rgb8> imaDepartments(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<value::rgb8> imaInvestigations(pointsImgBorders.nrows(), pointsImgBorders.ncols());
    image2d<value::rgb8> imaPhonetics(pointsImgBorders.nrows(), pointsImgBorders.ncols());

    vector<image2d<value::rgb8>> imas;
    imas.push_back(imaBorders);
    imas.push_back(imaDepartments);
    imas.push_back(imaInvestigations);
    imas.push_back(imaPhonetics);

    image2d<box2d> bbs(computeBboxImage<Bbox>(treeOther));
    vector<vector<point2d>> rectangles;

    std::ofstream fileBorders;
    std::ofstream fileDepartments;
    std::ofstream fileInvestigations;
    std::ofstream filePhonetics;
    std::stringstream ssBorders;
    std::stringstream ssDepartments;
    std::stringstream ssInvestigations;
    std::stringstream ssPhonetics;
    ssBorders << "borders-" << imgName << ".tsv";
    ssDepartments << "departements-" << imgName << ".tsv";
    ssInvestigations << "investigations-" << imgName << ".tsv";
    ssPhonetics << "phonetics-" << imgName << ".tsv";
    fileBorders.open(ssBorders.str());
    fileDepartments.open(ssDepartments.str());
    fileInvestigations.open(ssInvestigations.str());
    filePhonetics.open(ssPhonetics.str());
    fileBorders << "x\ty\twidth\theight\n";
    fileDepartments << "x\ty\twidth\theight\n";
    fileInvestigations << "x\ty\twidth\theight\n";
    filePhonetics << "x\ty\twidth\theight\n";

    std::stringstream ssTsvBorders;
    std::stringstream ssTsvOther;
    ssTsvBorders << "data/clusters/" << imgName << "-borders.tsv";
    ssTsvOther << "data/clusters/" << imgName << "-other.tsv";
    ifstream file(ssTsvOther.str(), ios::in);  // on ouvre en lecture
    if (file) {  // si l'ouverture a fonctionné
        string line;
        getline(file, line);
        if (getline(file, line)) {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
            unsigned long sep = tokens[0].find(',');
            unsigned int row = stoul(tokens[0].substr(1, sep - 1));
            unsigned int col = stoul(tokens[0].substr(sep + 1, tokens[0].length() - sep - 2));
            unsigned int agglomerativeClusteringCluster = stoul(tokens[1]);
            unsigned int birchCluster = stoul(tokens[2]);
            unsigned int kMeansCluster = stoul(tokens[3]);
            unsigned int meanShiftCluster = stoul(tokens[4]);
            unsigned int miniBatchKMeansCluster = stoul(tokens[5]);
            vector<unsigned int> clusters;
            clusters.push_back(agglomerativeClusteringCluster);
            clusters.push_back(birchCluster);
            clusters.push_back(kMeansCluster);
            clusters.push_back(meanShiftCluster);
            clusters.push_back(miniBatchKMeansCluster);
            for (auto const &point : treeOther.S) {
                unsigned int counter(0);
                for (auto &ima : imas) {
                    ima(point2d(point.row(), point.col())) = literal::black;
                    if (ima(point2d(treeOther.parent(point).row(), treeOther.parent(point).col())) !=
                        literal::black) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeOther.parent(point).row(), treeOther.parent(point).col()));
                        if (counter == 1) {
                            bool found(false);
                            unsigned int rectNumber(0);
                            while (rectNumber < rectangles.size() && !found) {
                                if (rectangles[rectNumber][0] == treeOther.parent(point)) {
                                    rectangles[rectNumber].push_back(point);
                                    found = true;
                                }
                                ++rectNumber;
                            }
                        }
                    }
                    ++counter;
                }
                if (treeOther.parent(point) != point && bbs(point).min_row() == row && bbs(point).min_col() == col) {
                    if ((bbs(point).min_row() > 1500 || bbs(point).min_col() > 2500) &&
                        (bbs(point).min_row() > 1500 || bbs(point).min_col() < pointsImgOther.ncols() - 2000) &&
                        bbs(point).min_row() > 500 && bbs(point).min_row() < pointsImgOther.nrows() - 500) {
                        if ((clusters[0] == 4 || clusters[0] == 6 || clusters[0] == 13) &&
                            (clusters[1] == 3 || clusters[1] == 4 /* || clusters[1] == 7 */ || clusters[1] == 8 ||
                             clusters[1] == 12 || clusters[1] == 13) /* &&
                        (clusters[2] == 3 || clusters[2] == 4 || clusters[2] == 10 || clusters[2] == 15) &&
                        (clusters[3] == 2 || clusters[3] == 3 || clusters[3] == 4 || clusters[3] == 6 ||
                         clusters[3] == 7 || clusters[3] == 9 || clusters[3] == 12) &&
                        (clusters[4] == 3 || clusters[4] == 6 || clusters[4] == 12) */) {
                            vector<point2d> rectangle;
                            rectangle.push_back(point);
                            rectangles.push_back(rectangle);
                            imas[1](point2d(point.row(), point.col())) = literal::white;
                            fileDepartments << bbs(point).min_col() << "\t" << bbs(point).min_row() << "\t"
                                            << bbs(point).ncols() << "\t" << bbs(point).nrows() << "\n";
                        } else if (
                                (/* clusters[0] == 0 || clusters[0] == 2 || */clusters[0] == 6 || clusters[0] == 11) &&
                                (clusters[1] == 0 || clusters[1] == 6 || clusters[1] == 7 || clusters[1] == 14) &&
                                (clusters[2] == 0 || clusters[2] == 5 || clusters[2] == 11 || clusters[2] == 15) &&
                                (clusters[3] == 0 || clusters[3] == 4 || clusters[3] == 5) &&
                                (clusters[4] == 3 || clusters[4] == 7 || clusters[4] == 10)) {
                            imas[2](point2d(point.row(), point.col())) = literal::white;
                            fileInvestigations << bbs(point).min_col() << "\t" << bbs(point).min_row() << "\t"
                                               << bbs(point).ncols() << "\t" << bbs(point).nrows() << "\n";
                        } else {
                            imas[3](point2d(point.row(), point.col())) = literal::white;
                        }
                    }
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[0].find(',');
                        row = stoul(newTokens[0].substr(1, sep - 1));
                        col = stoul(newTokens[0].substr(sep + 1, newTokens[0].length() - sep - 2));
                        for (unsigned int i(0); i < 5; ++i) {
                            clusters[i] = stoul(newTokens[i + 1]);
                        }
                    }
                }
            }
            fileDepartments.close();
            fileInvestigations.close();
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
    file.close();

    for (auto const &rect : rectangles) {
        for (unsigned int i(bbs(rect[0]).min_row()); i <= bbs(rect[0]).max_row(); ++i) {
            for (unsigned int j(bbs(rect[0]).min_col()); j <= bbs(rect[0]).max_col(); ++j) {
                if (imas[0](point2d(i, j)) != literal::black || imas[2](point2d(i, j)) != literal::black || imas[3](point2d(i, j)) != literal::black) {
                    imas[0](point2d(i, j)) = literal::black;
                    imas[1](point2d(i, j)) = literal::white;
                    imas[2](point2d(i, j)) = literal::black;
                    imas[3](point2d(i, j)) = literal::black;
                }
            }
        }
    }

    vector<string> layers({"Borders", "Departments", "Investigations", "Phonetics"});
    for (unsigned int layerNumber(0); layerNumber < layers.size(); ++layerNumber) {
        std::stringstream ss;
        ss << "img/post_treatment/" << imgName << "-" << layers[layerNumber] << ".ppm";
        io::ppm::save(imas[layerNumber], ss.str());
    }

    return 0;
}

