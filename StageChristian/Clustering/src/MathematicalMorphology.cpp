//
// Created by Christian Marquay on 03/06/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "Tree.h"

using namespace cv;
using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

void usage(char *argv[]);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cerr << "ERREUR : mauvais nombre d'arguments." << endl;
        usage(argv);
    }

    // argv[1] must match the path to a pgm image
    string imgPath = argv[1];

    // If the condition is true, the parameter is not the path of an image in pgm format
    if (imgPath.substr(imgPath.rfind('.') + 1, imgPath.length()) != "pgm") {
        cerr << "ERREUR : chemin de l'image non valide." << endl;
        usage(argv);
    }

    unsigned long start = imgPath.find_last_of('/');
    unsigned long end = imgPath.find_last_of('.');
    string imgName = imgPath.substr(start + 1, end - start - 1);

    // Let s be the point set
    image2d<int_u8> pointsImgGrayScale;
    io::pgm::load(pointsImgGrayScale, imgPath);
    Tree treeGrayScale = MinTree(pointsImgGrayScale);
    image2d<int_u8> imaGrayScale(pointsImgGrayScale.nrows(), pointsImgGrayScale.ncols());

    for (auto const &point : treeGrayScale.S) {
        if (treeGrayScale.u(point) > 127) {
            treeGrayScale.u(point) = 255;
            imaGrayScale(point2d(point.row(), point.col())) = 255;
        } else {
            treeGrayScale.u(point) = 0;
            imaGrayScale(point2d(point.row(), point.col())) = 0;
        }
    }

    image2d<box2d> bbsGrayScale(computeBboxImage<Bbox>(treeGrayScale));

    std::stringstream ssGrayScale;
    ssGrayScale << "img/binary/" << imgName << ".ppm";

    io::pgm::save(imaGrayScale, ssGrayScale.str());

    cout << "Binarization OK" << endl;

    // Apply blur to smooth edges and use adaptive thresholding
    Mat src = imread(ssGrayScale.str());
    Mat src_gray;
    GaussianBlur(src, src, Size(37, 37), 0);

    std::stringstream ssBlurred;
    ssBlurred << "img/filtered/blocks/" << imgName << ".pgm";

    cvtColor(src, src_gray, COLOR_BGR2GRAY);

    std::vector<int> compression_params;  // Stores the compression parameters
    compression_params.push_back(IMWRITE_PXM_BINARY);  // Set to PXM compression
    compression_params.push_back(0);  // Set type of PXM in our case PGM

    imwrite(ssBlurred.str(), src_gray, compression_params);

    cout << "Gaussian Blur OK" << endl;

    image2d<int_u8> pointsImgBlurred;
    io::pgm::load(pointsImgBlurred, ssBlurred.str());
    Tree treeBlurred = MinTree(pointsImgBlurred);
    image2d<int_u8> imaBlurred(pointsImgBlurred.nrows(), pointsImgBlurred.ncols());

    for (auto const &point : treeBlurred.S) {
        if (treeBlurred.u(point) > 229) {
            treeBlurred.u(point) = 255;
            imaBlurred(point2d(point.row(), point.col())) = 255;
        } else {
            treeBlurred.u(point) = 0;
            imaBlurred(point2d(point.row(), point.col())) = 0;
        }
    }

    io::pgm::save(imaBlurred, ssBlurred.str());

    cout << "Blocks OK" << endl;

    src = imread(ssBlurred.str());
    cvtColor(src, src_gray, COLOR_BGR2GRAY);

    Mat threshold_output;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    /// Detect edges using Threshold
    int thresh = 100;
    threshold(src_gray, threshold_output, thresh, 255, THRESH_BINARY);
    /// Find contours
    findContours(threshold_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

    /// Approximate contours to polygons + get bounding rects and circles
    vector<vector<Point> > contours_poly(contours.size());
    vector<Rect> boundRect(contours.size());
    vector<Point2f> center(contours.size());
    vector<float> radius(contours.size());

    for (int i = 0; i < contours.size(); i++) {
        approxPolyDP(Mat(contours[i]), contours_poly[i], 3, true);
        boundRect[i] = boundingRect(Mat(contours_poly[i]));
    }

    /// Draw polygonal contour + bonding rects + circles
    Mat drawing = Mat::zeros(threshold_output.size(), CV_8UC3);
    RNG rng(12345);

    image2d<int_u8> imaBorders(pointsImgGrayScale.nrows(), pointsImgGrayScale.ncols());
    image2d<int_u8> imaOther(pointsImgGrayScale.nrows(), pointsImgGrayScale.ncols());
    io::pgm::load(imaBorders, ssGrayScale.str());
    data::fill(imaOther, 255);
    Tree treeBorders = MinTree(imaBorders);

    for (unsigned int i(0); i < contours.size(); i++) {
        unsigned int height(boundRect[i].br().y - boundRect[i].tl().y);
        unsigned int width(boundRect[i].br().x - boundRect[i].tl().x);
        if (height > 5 && height < 250) {
            Scalar color = Scalar(255, 255, 255);
            rectangle(drawing, boundRect[i].tl(), boundRect[i].br(), color, 2, 8, 0);

            for (unsigned int j(boundRect[i].tl().x); j < boundRect[i].br().x; ++j) {
                for (unsigned int k(boundRect[i].tl().y); k < boundRect[i].br().y; ++k) {
                    point2d point = point2d(k, j);
                    if (treeBorders.u(point) < 127) {
                        imaBorders(point) = 255;
                        imaOther(point) = 0;
                    }
                }
            }
        }
    }

    std::stringstream ssRectangles;
    ssRectangles << "img/filtered/rectangles/" << imgName << ".pgm";

    cvtColor(drawing, drawing, COLOR_BGR2GRAY);

    imwrite(ssRectangles.str(), drawing, compression_params);

    std::stringstream ssBorders;
    ssBorders << "img/test/borders/" << imgName << ".pgm";

    io::pgm::save(imaBorders, ssBorders.str());

    std::stringstream ssOther;
    ssOther << "img/test/other/" << imgName << ".pgm";

    io::pgm::save(imaOther, ssOther.str());

    cout << "Rectangles OK" << endl;

    return 0;
}

void usage(char *argv[]) {
    cerr << "usage : " << argv[0] << " path/to/img.pgm" << endl;
    abort();
}

