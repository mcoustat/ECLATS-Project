//
// Created by Christian Marquay on 11/03/2019.
// MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <math.h>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

void usage(char *argv[]);

int main(int argc, char *argv[]) {
    if (argc != 2) {
        cerr << "ERREUR : mauvais nombre d'arguments." << endl;
        usage(argv);
    }

    // argv[1] must match the path to a pgm image
    string imgPath = argv[1];

    // If the condition is true, the parameter is not the path of an image in pgm format
    if (imgPath.substr(imgPath.rfind('.') + 1, imgPath.length()) != "pgm") {
        cerr << "ERREUR : chemin de l'image non valide." << endl;
        usage(argv);
    }

    unsigned long start = imgPath.find_last_of('/');
    unsigned long end = imgPath.find_last_of('.');
    string imgName = imgPath.substr(start + 1, end - start - 1);

    image2d<int_u8> pointsImgBinary;
    io::pgm::load(pointsImgBinary, imgPath);
    Tree treeBinary = MinTree(pointsImgBinary);

    image2d<unsigned int> area(computeAreaImage(treeBinary));
    image2d<box2d> bbs(computeBboxImage<Bbox>(treeBinary));
    image2d<double> ecc(computeEccentricityImage(treeBinary));
    image2d<double> dens(computeDensityImage(treeBinary));
    image2d<unsigned int> per(computePerimeterImage(treeBinary));
    image2d<double> lineThick(computeLineThicknessImage(treeBinary));
    image2d<double> circ(computeCircularityImage(treeBinary));

    std::ofstream myfile;
    std::stringstream ssData;
    ssData << "data/raw/" << imgName << ".tsv";
    myfile.open(ssData.str());
    myfile << "point\theight\twidth\teccentricity\tdensity\tperimeter\tline_thickness\tcircularity\n";
    for (auto const &point : treeBinary.S) {
        if (treeBinary.u(point) < 127 && treeBinary.u(treeBinary.parent(point)) > 127) {
            myfile << point2d(bbs(point).min_row(), bbs(point).min_col()) << "\t" << bbs(point).nrows() << "\t"
                   << bbs(point).ncols() << "\t" << ecc(point) << "\t" << dens(point) << "\t" << per(point) << "\t"
                   << lineThick(point) << "\t" << circ(point) << "\n";
        }
    }
    myfile.close();

    cout << "Data OK" << endl;


    return 0;
}

void usage(char *argv[]) {
    cerr << "usage : " << argv[0] << " path/to/img.pgm" << endl;
    abort();
}
