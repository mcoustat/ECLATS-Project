#! /usr/bin/env python3
# coding: utf-8

"""
Module for grouping data contained in a file using AgglomerativeClustering and KMeans methods.
param -d the file containing the data
param -c the number of desired clusters
return the file containing the data and clusters to which they were added
"""

import argparse
import time

import pandas as pd
from sklearn.cluster import (AgglomerativeClustering,
                             Birch,
                             KMeans,
                             MiniBatchKMeans,
                             MeanShift)


def parse_arguments():
    """
    Function to read the parameters passed in arguments at the execution of the module.
    return a parser containing the parameters
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--clusters", help="""Number of clusters desired""")
    parser.add_argument("-d", "--datafile", help="""TSV file containing the data""")
    return parser.parse_args()


def launch_analysis(data_file, name, nb_clusters):
    """
    Function to group data in clusters, back up the clusters in the original given file.
    param data_file the file containing the data
    param nb_clusters the number of desired clusters
    """
    # We load the data.
    data = pd.read_csv(data_file, sep="\t")

    # We eliminate the columns we will not use.
    my_data = data.drop(['point'], axis=1)

    # We transform data into numpy array.
    my_x = my_data.values

    # We create cluster objects.
    clustering_algorithms = (
        ('agglomerative_clustering', AgglomerativeClustering(n_clusters=nb_clusters)),
        ('birch', Birch(n_clusters=nb_clusters)),
        ('k_means', KMeans(n_clusters=nb_clusters)),
        ('mean_shift', MeanShift()),
        ('mini_batch_k_means', MiniBatchKMeans(n_clusters=nb_clusters))
    )

    df_data = pd.DataFrame({'point': data['point']})

    for title, algorithm in clustering_algorithms:
        start = time.time()

        algorithm.fit(my_x)

        end = time.time()

        df_data[title] = algorithm.labels_

        print('Running time of ' + title + ' : ' + str(end - start))

    df_data.to_csv('data/clusters/' + name + '.tsv', index=False, sep="\t")


if __name__ == "__main__":
    ARGS = parse_arguments()
    FILE_NAME = ARGS.datafile
    START = FILE_NAME.rfind('/')
    END = FILE_NAME.rfind('.')
    RADICAL_NAME = FILE_NAME[START+1:END]
    launch_analysis(FILE_NAME, RADICAL_NAME, int(ARGS.clusters))
