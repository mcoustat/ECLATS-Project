//
// Created by Christian Marquay on 18/04/2019.
// Bbox, ComputeAreaImage, ComputeAttribute, MaxTree, MinTree, and Tree created by Jordan Drapeau
// Milena library created by Thierry Géraud - https://www.lrde.epita.fr/wiki/Olena/Milena
//

#include <fstream>
#include <iostream>
#include <memory>
#include <mln/core/alias/neighb2d.hh>
#include <mln/core/image/dmorph/image_if.hh>
#include <mln/core/image/image2d.hh>
#include <mln/data/fill.hh>
#include <mln/debug/println.hh>
#include <mln/io/pgm/load.hh>
#include <mln/io/pgm/save.hh>
#include <mln/io/ppm/save.hh>
#include <mln/value/int_u8.hh>
#include <mln/value/rgb8.hh>
#include <sstream>
#include <string>
#include <vector>

#include "ComputeAttributes.h"
#include "MinTree.h"
#include "Tree.h"

using namespace mln;
using namespace my;
using namespace std;
using value::int_u8;
using value::rgb8;

int main(int argc, char *argv[]) {
    string tsv_path = argv[1];

    unsigned long start = tsv_path.find_last_of('/');
    unsigned long end = tsv_path.find_last_of('.');
    string imgName = tsv_path.substr(start + 1, end - start - 1);

    std::stringstream ssBinary;
    ssBinary << "img/test/borders/" << imgName << ".pgm";
    image2d<int_u8> pointsImgBinary;
    io::pgm::load(pointsImgBinary, ssBinary.str());
    Tree treeBinary = MinTree(pointsImgBinary);

    image2d<value::rgb8> imaAgglomerativeClustering(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaBirch(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaKMeans(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaMeanShift(pointsImgBinary.nrows(), pointsImgBinary.ncols());
    image2d<value::rgb8> imaMiniBatchKMeans(pointsImgBinary.nrows(), pointsImgBinary.ncols());

    image2d<box2d> bbs(computeBboxImage<Bbox>(treeBinary));

    ifstream file(tsv_path, ios::in);  // on ouvre en lecture
    if (file) {  // si l'ouverture a fonctionné
        string line;
        getline(file, line);
        if (getline(file, line)) {
            istringstream iss(line);
            vector<string> tokens{istream_iterator<string>{iss}, istream_iterator<string>{}};
            unsigned long sep = tokens[0].find(',');
            unsigned int row = stoul(tokens[0].substr(1, sep - 1));
            unsigned int col = stoul(tokens[0].substr(sep + 1, tokens[0].length() - sep - 2));
            unsigned int agglomerativeClusteringCluster = stoul(tokens[1]);
            unsigned int birchCluster = stoul(tokens[2]);
            unsigned int kMeansCluster = stoul(tokens[3]);
            unsigned int meanShiftCluster = stoul(tokens[4]);
            unsigned int miniBatchKMeansCluster = stoul(tokens[5]);
            vector<image2d<value::rgb8>> imas;
            imas.push_back(imaAgglomerativeClustering);
            imas.push_back(imaBirch);
            imas.push_back(imaKMeans);
            imas.push_back(imaMeanShift);
            imas.push_back(imaMiniBatchKMeans);
            vector<unsigned int> clusters;
            clusters.push_back(agglomerativeClusteringCluster);
            clusters.push_back(birchCluster);
            clusters.push_back(kMeansCluster);
            clusters.push_back(meanShiftCluster);
            clusters.push_back(miniBatchKMeansCluster);
            for (auto const &point : treeBinary.S) {
                for (auto ima : imas) {
                    ima(point2d(point.row(), point.col())) = literal::black;
                    if (ima(point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col())) !=
                        literal::black) {
                        ima(point2d(point.row(), point.col())) = ima(
                                point2d(treeBinary.parent(point).row(), treeBinary.parent(point).col()));
                    }
                }
                if (treeBinary.parent(point) != point && bbs(point).min_row() == row && bbs(point).min_col() == col) {
                    for (unsigned int counter(0); counter < clusters.size(); ++counter) {
                        switch (clusters[counter]) {
                            case 1:
                                imas[counter](point2d(point.row(), point.col())) = literal::blue;
                                break;
                            case 2:
                                imas[counter](point2d(point.row(), point.col())) = literal::brown;
                                break;
                            case 3:
                                imas[counter](point2d(point.row(), point.col())) = literal::cyan;
                                break;
                            case 4:
                                imas[counter](point2d(point.row(), point.col())) = literal::dark_gray;
                                break;
                            case 5:
                                imas[counter](point2d(point.row(), point.col())) = literal::green;
                                break;
                            case 6:
                                imas[counter](point2d(point.row(), point.col())) = literal::light_gray;
                                break;
                            case 7:
                                imas[counter](point2d(point.row(), point.col())) = literal::lime;
                                break;
                            case 8:
                                imas[counter](point2d(point.row(), point.col())) = literal::magenta;
                                break;
                            case 9:
                                imas[counter](point2d(point.row(), point.col())) = literal::medium_gray;
                                break;
                            case 10:
                                imas[counter](point2d(point.row(), point.col())) = literal::orange;
                                break;
                            case 11:
                                imas[counter](point2d(point.row(), point.col())) = literal::pink;
                                break;
                            case 12:
                                imas[counter](point2d(point.row(), point.col())) = literal::purple;
                                break;
                            case 13:
                                imas[counter](point2d(point.row(), point.col())) = literal::red;
                                break;
                            case 14:
                                imas[counter](point2d(point.row(), point.col())) = literal::teal;
                                break;
                            case 15:
                                imas[counter](point2d(point.row(), point.col())) = literal::violet;
                                break;
                            case 16:
                                imas[counter](point2d(point.row(), point.col())) = literal::yellow;
                                break;
                            default:
                                imas[counter](point2d(point.row(), point.col())) = literal::white;
                                break;
                        }
                    }
                    if (getline(file, line)) {
                        istringstream newIss(line);
                        vector<string> newTokens{istream_iterator<string>{newIss}, istream_iterator<string>{}};
                        sep = newTokens[0].find(',');
                        row = stoul(newTokens[0].substr(1, sep - 1));
                        col = stoul(newTokens[0].substr(sep + 1, newTokens[0].length() - sep - 2));
                        for (unsigned int i(0); i < 5; ++i) {
                            clusters[i] = stoul(newTokens[i + 1]);
                        }
                    }
                }
            }

            vector<string> titles({"AgglomerativeClustering", "Birch", "KMeans", "MeanShift", "MiniBatchKMeans"});
            for (unsigned int clusterNumber(0); clusterNumber < clusters.size(); ++clusterNumber) {
                std::stringstream ss;
                ss << "img/output/" << imgName << "-" << titles[clusterNumber] << ".ppm";
                io::ppm::save(imas[clusterNumber], ss.str());
            }
        }
    } else {
        cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
    file.close();

    return 0;
}
