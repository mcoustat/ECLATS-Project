## Images ##

1. Place in the file img/input/original/ images in tif format downloaded from the website http://lig-tdcge.imag.fr/cartodialect3/carteTheme

2. Convert images from tif format to pgm format using the bash script:


    #! /bin/bash
    
    for fich in img/input/original/*
    do
        if test ${fich#*.} = "tif"
        then
            convert $fich ${fich%%.*}".pgm"
            rm -f $fich
        fi
    done

## Preprocessing ##

1. Launch the following bash script:


    #!/bin/bash
    
    g++ -DNDEBUG -O3 -I. src/Preprocessing.cpp src/ComputeAttributes.h src/GaussianBlur.h src/MinTree.h src/Tree.h -o build/Preprocessing `pkg-config --cflags --libs opencv`
    
    for fich in img/input/original/*
    do
        ./build/Preprocessing $fich
        echo $fich" OK"
    done

2. Observe the data in the tsv files in the data/raw/ folder

## Clustering ##

1. Install the virtual environment with the command lines:


    pip3 install virtualenv
    virtualenv -p python3 env

2. Install the required python modules with the command lines:


    source env/bin/activate
    pip3 install -r requirements.txt
    deactivate

3. Perform principal component analysis of datasets:


    #!/bin/bash
    
    source env/bin/activate
    
    for fich in data/raw/*
    do
        python3 src/pca.py -d $fich
        echo $fich" OK"
    done
    
    deactivate

4. Observe the data in the tsv files in the data/pca/ folder

5. Perform clustering on the principal components:


    #!/bin/bash
    
    source env/bin/activate
    
    for fich in data/pca/*
    do
        python3 src/clustering.py -d $fich -c 17
        echo $fich" OK"
    done
    
    deactivate

6. Observe the data in the tsv files in the data/clusters/ folder

## Visualization ##

1. Launch the following bash script:


    #!/bin/bash
    
    g++ -DNDEBUG -O3 -I. src/Visualization.cpp src/ComputeAttributes.h src/MinTree.h src/Tree.h -o build/Visualization
    
    for fich in data/clusters/*
    do
        ./build/Visualization $fich
        echo $fich" OK"
    done

## Postprocessing ##

Postprocessing is the part based on the visualization of clusters created from the files Borders and Other.

1. Condition of Line 100: register clusters that do not include survey point numbers.

2. Condition of Line 155: register clusters that include department names.

3. Condition of line 160: register clusters that include survey point numbers.

4. Line condition 289: enter the sizes corresponding to the blocks including the desired information.

## Whole process ##


    #!/bin/bash
    
    for fich in img/input/original/*
    do
        ./build/MathematicalMorphology $fich
        echo $fich" OK"
    done
    for fich in img/test/other/*
    do
        ./build/Preprocessing $fich
        echo $fich" OK"
    done
    source env/bin/activate
    for fich in data/raw/*
    do
        python3 src/pca.py -d $fich
        echo $fich" OK"
    done
    for fich in data/pca/*
    do
        python3 src/clustering.py -d $fich -c 17
        echo $fich" OK"
    done
    deactivate
    for fich in data/clusters/*
    do
        ./build/Visualization $fich
        mv $fich ${fich%%.*}"-other.tsv"
        echo $fich" OK"
    done
    for fich in img/test/borders/*
    do
        ./build/Preprocessing $fich
        echo $fich" OK"
    done
    source env/bin/activate
    for fich in data/raw/*
    do
        python3 src/pca.py -d $fich
        echo $fich" OK"
    done
    for fich in data/pca/*
    do
        python3 src/clustering.py -d $fich -c 17
        echo $fich" OK"
    done
    deactivate
    for fich in data/clusters/*
    do
        if [[ $file == *"other"* ]]; then
            ./build/Visualization $fich
            mv $fich ${fich%%.*}"-borders.tsv"
            echo $fich" OK"
        fi
    done
    

Then create your own postprocessing.
